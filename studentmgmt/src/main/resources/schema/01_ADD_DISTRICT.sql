INSERT INTO `district`(id, name)
VALUES (1,'Ba Đình'),
       (2,'Ba Vì'),
       (3,'Bắc Từ Liêm'),
       (4,'Cầu Giấy'),
       (5,'Chương Mỹ'),
       (6,'Đan Phượng'),
       (7,'Đông Anh'),
       (8,'Đống Đa'),
       (9,'Gia Lâm'),
       (10,'Hà Đông'),
       (11,'Hai Bà Trưng'),
       (12,'Hoài Đức'),
       (13,'Hoàn Kiếm'),
       (14,'Hoàng Mai'),
       (15,'Long Biên'),
       (16,'Mê Linh'),
       (17,'Mỹ Đức'),
       (18,'Nam Từ Liêm'),
       (19,'Phú Xuyên'),
       (20,'Phúc Thọ'),
       (21,'Quốc Oai'),
       (22,'Sóc Sơn'),
       (23,'Sơn Tây'),
       (24,'Tây Hồ'),
       (25,'Thạch Thất'),
       (26,'Thanh Oai'),
       (27,'Thanh Trì'),
       (28,'Thanh Xuân'),
       (29,'Thường Tín'),
       (30,'Ứng Hoà');