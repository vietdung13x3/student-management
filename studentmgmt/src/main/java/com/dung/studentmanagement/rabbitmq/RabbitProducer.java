package com.dung.studentmanagement.rabbitmq;

import com.dung.studentmanagement.dto.request.ImportStudentRequest;
import com.dung.studentmanagement.dto.request.UploadStudentRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Queue;

@Component
@AllArgsConstructor
public class RabbitProducer {
    private final RabbitTemplate rabbitTemplate;

    public void send(String routingKeys, Object message) {
        rabbitTemplate.convertAndSend(routingKeys, message);
    }
}
