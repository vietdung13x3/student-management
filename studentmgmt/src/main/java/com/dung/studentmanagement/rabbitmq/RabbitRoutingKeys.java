package com.dung.studentmanagement.rabbitmq;

public interface RabbitRoutingKeys {
    String UPLOAD_STUDENTS = "student-mgmt.student.uploads";
    String REMOVE_COURSE = "student-mgmt.course.remove";
}
