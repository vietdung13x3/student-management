package com.dung.studentmanagement.rabbitmq;

import com.dung.studentmanagement.dto.request.ImportStudentRequest;
import com.dung.studentmanagement.dto.request.UploadStudentRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Postcode;
import com.dung.studentmanagement.entity.Student;
import com.dung.studentmanagement.enums.StudentStatus;
import com.dung.studentmanagement.mapper.StudentMapper;
import com.dung.studentmanagement.service.CourseService;
import com.dung.studentmanagement.service.PostcodeService;
import com.dung.studentmanagement.service.StudentService;
import com.dung.studentmanagement.utils.CommonUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Slf4j
@AllArgsConstructor
public class RabbitConsumer {

    private final StudentService studentService;
    private final PostcodeService postcodeService;
    private final CourseService courseService;
    private static final Gson jsonConverter = new Gson();

    @RabbitListener(queues = {RabbitRoutingKeys.UPLOAD_STUDENTS})
    @Transactional(rollbackFor = Exception.class)
    public void receiveUploadStudents(@Payload String message) throws JsonProcessingException {
        ImportStudentRequest studentRequest = jsonConverter.fromJson(message,ImportStudentRequest.class);
        log.info("Consumed message: {}", studentRequest.getStudents().get(0).getPostcode());
        Function<UploadStudentRequest, Student> mapper = StudentMapper.INSTANCE::fromStudent;
        List<Student> entities = studentRequest.getStudents().stream().map(request -> {
            Student entity = mapper.apply(request);
            Date now = new Date();
            entity.setDateJoined(now);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(now);
            calendar.add(Calendar.YEAR, request.getNumberOfYears());
            Date expiredDate = calendar.getTime();
            entity.setStatus(StudentStatus.ACTIVE);
            entity.setDateExpired(expiredDate);
            Postcode postcode = postcodeService.findByCode(Long.valueOf(request.getPostcode()));
            postcode.getDistrict();
            entity.setDistrict(postcode.getDistrict());
            return entity;
        }).collect(Collectors.toList());
        studentService.saveAll(entities);
        log.info("Uploaded {} students with token key: {}", entities.size(), CommonUtils.makeTokenKey());
    }

    @RabbitListener(queues = {RabbitRoutingKeys.REMOVE_COURSE})
    @Transactional(rollbackFor = Exception.class)
    public void deleteCourse(@Payload String courseId) {
        Long id = Long.valueOf(courseId);
        Course course = courseService.findById(id);
        course.setEnrolledStudents(new HashSet<>());
        course.setTeachingPeriod(null);
        course.setSubTeachers(new HashSet<>());
        courseService.save(course);
        log.info("Successfully cleaned the removed course with id {} with token key: {}",id, CommonUtils.makeTokenKey());
    }
}
