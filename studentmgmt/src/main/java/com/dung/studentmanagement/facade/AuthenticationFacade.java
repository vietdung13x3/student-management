package com.dung.studentmanagement.facade;

import com.dung.studentmanagement.dto.request.TeacherLoginRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface AuthenticationFacade {
    ResponseEntity<ResponseRestDto> authenticate(TeacherLoginRequest loginRequest);
    ResponseEntity<ResponseRestDto> logOut(HttpServletRequest request, String accessToken);
}
