package com.dung.studentmanagement.facade.impl;

import com.dung.studentmanagement.dto.request.ImportStudentRequest;
import com.dung.studentmanagement.dto.request.StudentSearchRequest;
import com.dung.studentmanagement.dto.request.UpdateStatusRequest;
import com.dung.studentmanagement.dto.request.UploadStudentRequest;
import com.dung.studentmanagement.dto.response.CourseDetailResponse;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.dto.response.StudentSearchResponse;
import com.dung.studentmanagement.entity.*;
import com.dung.studentmanagement.enums.Gender;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.enums.StudentStatus;
import com.dung.studentmanagement.exception.BusinessException;
import com.dung.studentmanagement.facade.Facade;
import com.dung.studentmanagement.facade.StudentFacade;
import com.dung.studentmanagement.rabbitmq.RabbitProducer;
import com.dung.studentmanagement.rabbitmq.RabbitRoutingKeys;
import com.dung.studentmanagement.service.CourseService;
import com.dung.studentmanagement.service.DistrictService;
import com.dung.studentmanagement.service.PostcodeService;
import com.dung.studentmanagement.service.StudentService;
import com.dung.studentmanagement.utils.CommonUtils;
import com.dung.studentmanagement.utils.DateUtils;
import com.dung.studentmanagement.utils.ExcelUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Facade
@AllArgsConstructor
@Slf4j
public class StudentFacadeImpl implements StudentFacade {
    private final StudentService studentService;
    private final PostcodeService postcodeService;
    private final RabbitProducer rabbitProducer;
    private final CourseService courseService;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> saveStudentRequest(UploadStudentRequest request, Function<UploadStudentRequest, Student> mapper, String tokenKey) {
        Student student = mapper.apply(request);
        if(studentService.existsByIdentifyCode(student.getIdentifyCode())) {
            log.error("Student is already existed with token key: {}", tokenKey);
            throw new BusinessException(ResponseStatus.DUPLICATEDSTUDENT, "Student is already existed!");
        }
        Date now = new Date();
        student.setDateJoined(now);
        student.setStatus(StudentStatus.ACTIVE);
        Calendar calendar = Calendar.getInstance();
        if(request.getNumberOfYears() > 4) {
            log.error("Number of years must be less than 5 with token key: {}", tokenKey);
            throw new BusinessException(ResponseStatus.BADREQUEST, "Number of years must be less than 5");
        }
        Postcode postcode = postcodeService.findByCode(Long.valueOf(request.getPostcode()));
        if(postcode == null) {
            log.error("Can not find postcode with code {} with token key: {}", request.getPostcode(), tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Can not find postcode with code "+ request.getPostcode());
        }
        student.setDistrict(postcode.getDistrict());
        calendar.setTime(now);
        calendar.add(Calendar.YEAR, request.getNumberOfYears());
        Date expired = calendar.getTime();
        student.setDateExpired(expired);
        studentService.save(student);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully saved student!", null);
    }

    @Override
    public ResponseEntity<ResponseRestDto> searchByParams(StudentSearchRequest params, Function<Student, StudentSearchResponse> mapper, Pageable pageable, String tokenKey) {
        Page<Student> pageStudents = studentService.search(params, pageable);
        List<StudentSearchResponse> res = pageStudents.stream().map(mapper).collect(Collectors.toList());
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully searched students!", pageStudents.getNumber(), pageStudents.getTotalElements(), pageStudents.getTotalPages(), res);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> importStudents(MultipartFile file, String tokenKey) {
        if(!ExcelUtils.isXlsx(file)) {
            log.error("Error file format with token key: {}", tokenKey);
            throw new BusinessException(ResponseStatus.WRONGFILEFORMAT, "Wrong file format!");
        }
        try (XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream())) {
            XSSFSheet sheet = workbook.getSheetAt(0);
            List<UploadStudentRequest> requests = new ArrayList<>();
            int numRows = sheet.getPhysicalNumberOfRows();
            for(int i = 1; i < numRows; i ++) {
                int col = 0;
                XSSFRow row = sheet.getRow(i);
                try{
                    String firstname = ExcelUtils.getValue(row,col++);
                    String lastname = ExcelUtils.getValue(row,col++);
                    Long identifyCode = Long.valueOf(ExcelUtils.getValue(row,col++));
                    String strDateOfBirth = ExcelUtils.getValue(row, col++);
                    Date dateOfBirth = DateUtils.getDate(strDateOfBirth);
                    Integer numberOfYears = Integer.valueOf(ExcelUtils.getValue(row,col++));
                    String strPostcode = ExcelUtils.getValue(row,col++);;
                    Postcode postcode = postcodeService.findByCode(Long.valueOf(strPostcode));
                    if(postcode == null) {
                        throw new BusinessException(ResponseStatus.NOTFOUND, "Can not find postcode");
                    }
                    if(studentService.existsByIdentifyCode(identifyCode)) {
                        throw new BusinessException(ResponseStatus.DUPLICATEDSTUDENT, "Student already exist with identify code "+ identifyCode);
                    }
                    District district = postcode.getDistrict();
                    Gender gender = Gender.valueOf(ExcelUtils.getValue(row,col++));
                    UploadStudentRequest request = UploadStudentRequest.builder()
                            .firstname(firstname).lastname(lastname).identifyCode(identifyCode)
                            .dateOfBirth(strDateOfBirth).numberOfYears(numberOfYears)
                            .postcode(strPostcode).gender(gender).build();
                    requests.add(request);
                }catch (BusinessException e) {
                    log.error("Error format - {}: {}",e.getMessage(), tokenKey);
                    throw new BusinessException(ResponseStatus.WRONGFILEFORMAT,e.getMessage());
                }catch (Exception e) {
                    throw new BusinessException(ResponseStatus.WRONGFILEFORMAT,"Error content format with " + file.getOriginalFilename());
                }
            }
            rabbitProducer.send(RabbitRoutingKeys.UPLOAD_STUDENTS, CommonUtils.stringify(ImportStudentRequest.builder().students(requests).build()));
            return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully! Please wait for further information!",null);
        } catch (IOException e) {
            log.error("Catch IO error exception with token key: {}", tokenKey);
            throw new BusinessException(ResponseStatus.INTERNALERROR, "IO error!");
        }
    }

    @Override
    public ResponseEntity<ResponseRestDto> findById(Long id, Function<Student, StudentSearchResponse> mapper, String tokenKey) {
        log.info("Getting student by id with token key: {}", tokenKey);
        Student student = studentService.findById(id);
        if(student == null) {
            throw new BusinessException(ResponseStatus.NOTFOUND, "Could not find student with id " + id);
        }
        StudentSearchResponse response = mapper.apply(student);
        List<CourseDetailResponse> courseResp = student.getCourses().stream().map(course -> CourseDetailResponse.builder().name(course.getName()).code(course.getCode()).build()).collect(Collectors.toList());
        response.setCourses(courseResp);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully get student by id!", response);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> updateStudentStatus(Long id,UpdateStatusRequest status, String tokenKey) {
        Student student = studentService.findById(id);
        if(student == null) {
            throw new BusinessException(ResponseStatus.NOTFOUND, "Can not find student with id " + id);
        }
        student.setStatus(status.getStatus());
        studentService.save(student);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully update student's status!", null);
    }

    @Override
    public ResponseEntity<ResponseRestDto> findStudentNotInCourse(String code, String tokenKey) {
        Course course = courseService.findByCode(code);
        if(course == null) {
            log.error("Not found course with code {} with token key: {}", code, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND,"Can not find course with code "+ code);
        }
        List<StudentSearchResponse> students = studentService.findAll().stream()
                .filter(student -> !course.getEnrolledStudents().contains(student) && !Arrays.asList(StudentStatus.EXPIRED, StudentStatus.LOCKED).contains(student.getStatus()))
                .map(student -> {
                    StudentSearchResponse res = new StudentSearchResponse();
                    res.setId(student.getId());
                    res.setFirstname(student.getFirstname());
                    res.setLastname(student.getLastname());
                    res.setIdentifyCode(student.getIdentifyCode());
                    return res;
                })
                .collect(Collectors.toList());

        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully get student info!", students);
    }
}
