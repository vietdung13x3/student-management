package com.dung.studentmanagement.facade.impl;

import com.dung.studentmanagement.dto.request.CourseCreatedRequest;
import com.dung.studentmanagement.dto.request.CourseSearchRequest;
import com.dung.studentmanagement.dto.response.*;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Student;
import com.dung.studentmanagement.entity.Teacher;
import com.dung.studentmanagement.entity.TeachingPeriod;
import com.dung.studentmanagement.enums.CourseStatus;
import com.dung.studentmanagement.enums.Period;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.exception.BusinessException;
import com.dung.studentmanagement.facade.CourseFacade;
import com.dung.studentmanagement.facade.Facade;
import com.dung.studentmanagement.rabbitmq.RabbitProducer;
import com.dung.studentmanagement.rabbitmq.RabbitRoutingKeys;
import com.dung.studentmanagement.service.CourseService;
import com.dung.studentmanagement.service.StudentService;
import com.dung.studentmanagement.service.TeacherService;
import com.dung.studentmanagement.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Facade
@Slf4j
@AllArgsConstructor
public class CourseFacadeImpl implements CourseFacade {
    private final CourseService courseService;
    private final TeacherService teacherService;
    private final StudentService studentService;
    private final RabbitProducer rabbitProducer;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ResponseEntity<ResponseRestDto> savedCourse(CourseCreatedRequest createdRequest, Function<CourseCreatedRequest, Course> mapper, String tokenKey) {
        log.info("Starting saving course with token key: {}", tokenKey);
        Teacher convenor = teacherService.findByEmail(createdRequest.getConvenorEmail());
        if(convenor == null) {
            throw new BusinessException(ResponseStatus.TEACHERNOTFOUND,"There is no teacher with email " + createdRequest.getConvenorEmail());
        }
        if(courseService.existsByCode(createdRequest.getCode())) {
            throw new BusinessException(ResponseStatus.COURSEEXISTS, "Course already existed with code - " + createdRequest.getCode());
        }

        Course course = mapper.apply(createdRequest);
        course.setStatus(CourseStatus.AVAILABLE);
        course.setDateCreated(new Date());
        course.setConvenor(convenor);
        courseService.save(course);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully created course!",null);
    }

    @Override
    public ResponseEntity<ResponseRestDto> searchCourse(CourseSearchRequest searchRequest, Function<Course, CourseSearchResponse> mapper, Pageable pageable, String tokenKey) {
        log.info("Started search course with token key: {}", tokenKey);
        Page<Course> coursePage = courseService.search(searchRequest,pageable);
        List<CourseSearchResponse> courseSearchResponses =  coursePage.getContent().stream().map(mapper).collect(Collectors.toList());
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully get courses!", coursePage.getNumber(), coursePage.getTotalElements(), coursePage.getTotalPages(),courseSearchResponses);
    }

    @Override
    public ResponseEntity<ResponseRestDto> getCourseByCode(String code, Function<Course, CourseDetailsResponse> mapper, String tokenKey) {
        log.info("Getting course by code with token key: {}", tokenKey);
        Course course = courseService.findByCode(code);
        if(course == null) {
            throw new BusinessException(ResponseStatus.NOTFOUND, "No course with code " + code);
        }
        CourseDetailsResponse courseRes = mapper.apply(course);
        log.info("Got course by code {} with toke key: {}", courseRes.getCode(), tokenKey);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully get course details by code!", courseRes);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> updateCourseStatus(CourseStatus status, String tokenKey, String code) {
        Course courseToUpdate = courseService.findByCode(code);
        if(courseToUpdate == null) {
            log.error("Can not find course by code {} with token key: {}", code, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Not found course by code " + code);
        }
        courseToUpdate.setStatus(status);
        courseService.save(courseToUpdate);
        if(status == CourseStatus.REMOVED) {
            rabbitProducer.send(RabbitRoutingKeys.REMOVE_COURSE, courseToUpdate.getId());
        }
        log.info("Successfully update course with token key: {}", tokenKey);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully update course", null);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> assignCoursePeriod(String code, Period period, String tokenKey) {
        Course courseToUpdate = courseService.findByCode(code);
        if(courseToUpdate == null) {
            log.error("Can not find course by code {} with token key: {}", code, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Not found course by code " + code);
        }
        TeachingPeriod teachingPeriod = courseService.findTeachingPeriodByName(period);
        courseToUpdate.setTeachingPeriod(teachingPeriod);
        log.info("Successfully updated course with new teaching period from token key: {}", tokenKey);
        courseService.save(courseToUpdate);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully assigned teaching period", null);
    }

    @Override
    public ResponseEntity<ResponseRestDto> getTeacherByCourse(String code, Function<Teacher, TeacherSearchResponse> mapper,Pageable pageable ,String tokenKey) {
        Course course = courseService.findByCode(code);
        if(course == null) {
            log.error("Error get teacher by course code - course code not found with token key: {}", tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Not found course code " + code);
        }
        Page<Teacher> teacherByCourse = teacherService.findTeacherByCourse(course, pageable);
        List<TeacherSearchResponse> teacherSearchResponseList = teacherByCourse.getContent().stream().map(mapper)
                                                                                .collect(Collectors.toList());
        return CommonUtils
                .makeRes(ResponseStatus.SUCCESS, "Successfully retrieved teachers!", teacherByCourse.getNumber(),teacherByCourse.getTotalElements(), teacherByCourse.getTotalPages(), teacherSearchResponseList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> saveCourseTeacher(Collection<String> emails, String code, String tokenKey) {
        if(emails.size() == 0) {
            throw new BusinessException(ResponseStatus.EMPTY, "Email list must not be empty!");
        }
        log.info("Starting save teacher to course {} with token key: {}", code, tokenKey);
        // Find Existed Teachers
        Course byCode = courseService.findByCode(code);
        if(byCode == null) {
            throw new BusinessException(ResponseStatus.NOTFOUND, "Can not found course with code " + code);
        }
        Set<Teacher> existedTeachers = byCode.getSubTeachers();
        // Teacher to save filter by existed teachers
        Set<Teacher> teacherToSave = teacherService.findAllByEmailIn(emails)
                                            .stream()
                                            .filter(teacher -> !existedTeachers.contains(teacher))
                                            .collect(Collectors.toSet());
        byCode.getSubTeachers().addAll(teacherToSave);
        // Get all saved emails
        Set<String> savedEmails = teacherToSave.stream().map(teacher -> teacher.getEmail()).collect(Collectors.toSet());
        Set<String> errorEmails = emails.stream().filter(email -> !savedEmails.contains(email)).collect(Collectors.toSet());
        Map<String, Set> res = new HashMap<>();
        res.put("savedEmails",savedEmails);
        res.put("errorEmails", errorEmails);
        courseService.save(byCode);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully saved teacher!", res);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> deleteCourseTeacher(String code, String email, String tokenKey) {
        log.info("Starting delete teacher from course {} with token key: {}", code, tokenKey);
        Course course = courseService.findByCode(code);
        if(course == null) {
            log.error("Error delete teacher - course not found with token key", tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Can not found course with code " + code);
        }
        Teacher teacherToSave = teacherService.findByEmail(email);
        if(teacherToSave == null) {
            log.error("Error delete teacher - teacher not found with token key", tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Can not find teacher with email " + email);
        }
        if(!course.getSubTeachers().contains(teacherToSave)) {
            log.error("Error delete teacher - teacher not found with token key", tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Can not find teacher from course with email " + email);
        }
        course.getSubTeachers().remove(teacherToSave);
        courseService.save(course);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully removed teacher from course " + code, null);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> saveCourseStudent(String courseCode, List<Long> ids, String tokenKey) {
        Course course = courseService.findByCode(courseCode);
        if(course == null) {
            log.error("No course found with code {} by token key: {}", courseCode, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Could not find course with code: " + courseCode);
        }
        List<Student> students = studentService.findByIdIn(ids);
        if(ids.size() != students.size()) {
            log.error("Found non-existed id with token key: {}", tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Identify code not found or student is expired/locked in list!");
        }
        try {
            course.getEnrolledStudents().addAll(students);
        }catch (Exception e) {
            throw new BusinessException(ResponseStatus.BADREQUEST, e.getMessage());
        }
        courseService.save(course);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully add student to the course!", null);
    }

    @Override
    public ResponseEntity<ResponseRestDto> getStudentsInCourse(String code, Function<Student, StudentSearchResponse> mapper, Pageable pageable, String tokenKey) {
        Course course = courseService.findByCode(code);
        if(course == null) {
            log.error("Not found course with code {} from token key: {}", code, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "No course with code " + code);
        }
        Page<Student> students = studentService.findByCourse(course, pageable);
        List<StudentSearchResponse> searchResponses = students.stream().map(mapper).collect(Collectors.toList());
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully get students!", students.getNumber(), students.getTotalElements(), students.getTotalPages(), searchResponses);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> deleteStudentFromCourse(String courseCode, Long studentId, String tokenKey) {
        Course course = courseService.findByCode(courseCode);
        if(course == null) {
            log.error("Not found course with code {} from token key: {}", courseCode, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "No course with code " + courseCode);
        }
        Student student = studentService.findById(studentId);
        if(student == null) {
            log.error("Not found student with id {} from token key: {}",studentId, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "No student found");
        }
        course.getEnrolledStudents().remove(student);
        courseService.save(course);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully removed student from course!",null);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> assignConvenor(String code, String teacherEmail, String tokenKey) {
        Course course = courseService.findByCode(code);
        if(course == null) {
            log.error("Not found course with code {} from token key: {}", code, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "No course with code " + code);
        }
        Teacher teacher = teacherService.findByEmail(teacherEmail);
        if(teacher == null) {
            log.error("Not found teacher with email {} from token key: {}", teacherEmail, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "No teacher with email " + teacherEmail);
        }
        if(course.getSubTeachers().contains(teacher)) {
            log.error("Error to save assign sub teacher with token key: {}", tokenKey);
            throw new BusinessException(ResponseStatus.BADREQUEST, "Error request to assign sub teacher to convenor!");
        }
        course.setConvenor(teacher);
        courseService.save(course);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully assign new convenor!", null);
    }
}
