package com.dung.studentmanagement.facade.impl;

import com.dung.studentmanagement.dto.request.SavePostcodeRequest;
import com.dung.studentmanagement.dto.response.CourseDetailsResponse;
import com.dung.studentmanagement.dto.response.DistrictResponse;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.dto.response.TeachingPeriodResponse;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.District;
import com.dung.studentmanagement.entity.Postcode;
import com.dung.studentmanagement.entity.TeachingPeriod;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.exception.BusinessException;
import com.dung.studentmanagement.facade.CommonFacade;
import com.dung.studentmanagement.facade.Facade;
import com.dung.studentmanagement.redis.RedisKeyConstants;
import com.dung.studentmanagement.redis.RedisService;
import com.dung.studentmanagement.service.CourseService;
import com.dung.studentmanagement.service.DistrictService;
import com.dung.studentmanagement.service.PostcodeService;
import com.dung.studentmanagement.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Facade
@AllArgsConstructor
@Slf4j
public class CommonFacadeImpl implements CommonFacade {
    private final RedisService redisService;
    private final DistrictService districtService;
    private final PostcodeService postcodeService;
    private final CourseService courseService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> getDistricts(Function<District, DistrictResponse> mapper, String tokenKey) {
        String key = RedisKeyConstants.DISTRICT;
        try {
            List<DistrictResponse> res = (List<DistrictResponse>) redisService.getFromRedis(key);
            if(res == null) {
                log.info("No items found from redis with token key: {}", tokenKey);
                List<District> districtList = districtService.findAll();
                List<DistrictResponse> districtsResponse = districtList.stream().map(mapper).collect(Collectors.toList());
                redisService.save(key, districtsResponse);
                return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully get districts!", districtsResponse);
            }
            return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully get districts!", res);
        }catch (Exception e) {
            throw new BusinessException(ResponseStatus.INTERNALERROR, e.getMessage());
        }

    }

    @Override
    public ResponseEntity<ResponseRestDto> deleteCacheByKey(String key, String tokenKey) {
        redisService.delete(key);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully delete key from redis", null);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ResponseEntity<ResponseRestDto> savePostcode(SavePostcodeRequest savePostcodeRequest, String tokenKey) {
        District district = districtService.findById(savePostcodeRequest.getDistrictId());
        if(district == null) {
            throw new BusinessException(ResponseStatus.IDNOTFOUND, "No such district found!");
        }
        Set<Long> availablePostcodes = postcodeService.findByCodeIn(savePostcodeRequest.getPostcode()).stream().map((postcode -> postcode.getCode())).collect(Collectors.toSet());
        List<Postcode> postcodesToSave = new ArrayList<>();
        for(Long postcode : savePostcodeRequest.getPostcode()) {
            if(availablePostcodes.contains(postcode)) {
                throw new BusinessException(ResponseStatus.EXISTEDPOSTCODE, postcode.toString() + " is existed!");
            }
            postcodesToSave.add(new Postcode(postcode, district));
        }
        postcodeService.saveAll(postcodesToSave);
        log.info("Saved {} postcodes to db with token key: {}", postcodesToSave.size(), tokenKey);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully saved postcode!", null);
    }

    @Override
    public ResponseEntity<ResponseRestDto> getPostcodesByDistrict(String districtName, String tokenKey) {
        log.info("Starting searching postcode with district name {} with token key: {}", districtName, tokenKey);
        District district = districtService.findByName(districtName);
        List<Long> postcodes = district.getPostcodes().stream().map((postcode -> postcode.getCode())).collect(Collectors.toList());
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully getting postcodes!", postcodes);
    }

    @Override
    public ResponseEntity<ResponseRestDto> getStudyPeriod(Function<TeachingPeriod, TeachingPeriodResponse> mapper,String tokenKey) {
        log.info("Get all study period with token key: {}", tokenKey);
        List<TeachingPeriodResponse> responses = (List<TeachingPeriodResponse>) redisService.getFromRedis(RedisKeyConstants.PERIODS);
        if(responses == null) {
            List<TeachingPeriod> teachingPeriods = courseService.getTeachingPeriods();
            responses = teachingPeriods.stream().map(mapper).collect(Collectors.toList());
            redisService.save(RedisKeyConstants.PERIODS,responses);
        }
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully get all teaching periods!", responses);
    }
}
