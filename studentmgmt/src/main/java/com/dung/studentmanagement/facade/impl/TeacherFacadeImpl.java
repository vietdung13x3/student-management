package com.dung.studentmanagement.facade.impl;

import com.dung.studentmanagement.dto.request.*;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.dto.response.TeacherPendingResponse;
import com.dung.studentmanagement.dto.response.TeacherSearchResponse;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Role;
import com.dung.studentmanagement.entity.Teacher;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.enums.Status;
import com.dung.studentmanagement.exception.BusinessException;
import com.dung.studentmanagement.facade.TeacherFacade;
import com.dung.studentmanagement.facade.Facade;
import com.dung.studentmanagement.service.*;
import com.dung.studentmanagement.utils.CommonUtils;
import com.dung.studentmanagement.utils.DateUtils;
import com.dung.studentmanagement.utils.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Facade
@Slf4j
public class TeacherFacadeImpl implements TeacherFacade {

    private final TeacherService teacherService;
    private final JwtService jwtService;
    private final TeacherPendingService teacherPendingService;
    private final RoleService roleService;
    private final CourseService courseService;

    public TeacherFacadeImpl(TeacherService teacherService, JwtService jwtService, TeacherPendingService teacherPendingService, RoleService roleService, CourseService courseService) {
        this.teacherService = teacherService;
        this.jwtService = jwtService;
        this.teacherPendingService = teacherPendingService;
        this.roleService = roleService;
        this.courseService = courseService;
    }

    private boolean isIdContains(String existedId, Collection<String> requestedIds) {
        return requestedIds.contains(existedId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    public ResponseEntity<ResponseRestDto> approveRequestList(ApproveTeacherRequest request, Function<TeacherPendingRequest, Teacher> mapper, String tokenKey) {
        List<Long> ids = request.getIds().stream().map((id) -> {
            return Long.valueOf(id);
        }).collect(Collectors.toList());
        Role role = roleService.findByRoleCode(request.getRoleCode());
        List<TeacherPendingRequest> requests = teacherPendingService.updateRequestStatusByIds(ids, Status.APPROVE);
        log.info("Getting [{}] requests existed from accepted ids with token key: {}", requests.size(), tokenKey);
        List<String> approvedIds = new ArrayList<>();
        List<String> errorIds = new ArrayList<>();
        List<Teacher> teachersToSave = requests.stream().map((teacherRequest) -> {
            String existedId = String.valueOf(teacherRequest.getId());
            approvedIds.add(existedId);
            Teacher teacher = mapper.apply(teacherRequest);
            teacher.setStatus(Status.ACTIVE);
            teacher.setDateJoined(new Date());
            teacher.setRole(role);
            return teacher;
        }).collect(Collectors.toList());
        teacherService.saveAll(teachersToSave);
        log.info("Successfully approved {} teacher requests with token key: {}", teachersToSave.size(), tokenKey);
        for(String id : request.getIds()) {
            if(!isIdContains(id,approvedIds)) {
                errorIds.add(id);
            }
        }
        log.warn("Unable to save [{}] teacher request with token key: {}", errorIds.size(), tokenKey);
        TeacherPendingResponse resp =  TeacherPendingResponse.builder().errorIds(errorIds).savedIds(approvedIds).build();
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully approve requests",resp);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    public ResponseEntity<ResponseRestDto> rejectRequestList(RejectTeacherRequest request, String tokenKey) {
        Set<Long> ids = request.getIds().stream().collect(Collectors.toSet());
        List<TeacherPendingRequest> rejectedRequests = teacherPendingService.updateRequestStatusByIds(ids, Status.REJECT);
        Set<String> savedIds = rejectedRequests.stream().map((rejectedRequest) -> {
            return String.valueOf(rejectedRequest.getId());
        }).collect(Collectors.toSet());
        Set<String> errorIds = new HashSet<>();
        for(Long id : ids) {
            if(!savedIds.contains(String.valueOf(id))) {
                errorIds.add(String.valueOf(id));
            }
        }
        TeacherPendingResponse resp = TeacherPendingResponse.builder().savedIds(savedIds).errorIds(errorIds).build();
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully reject requests", resp);
    }

    @Override
    public ResponseEntity<ResponseRestDto> search(TeacherSearchRequest searchRequest, Function<Teacher, TeacherSearchResponse> mapper, Pageable pageable, String tokenKey) {
        Page<Teacher> pageTeacher = teacherService.search(searchRequest, pageable);
        List<TeacherSearchResponse> teacherList = pageTeacher.getContent().stream().map(mapper).collect(Collectors.toList());
        Integer currentPage = pageTeacher.getPageable().getPageNumber();
        Integer totalPage = pageTeacher.getTotalPages();
        Long totalElements = pageTeacher.getTotalElements();
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully searched teacher",currentPage, totalElements, totalPage, teacherList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    public ResponseEntity<ResponseRestDto> updateTeacher(UpdateTeacherRequest updateRequest, String tokenKey) {
        String email = updateRequest.getEmail();
        log.info("Started updating teacher details with email {} and with token key: {}", email, tokenKey);
        Teacher teacherToUpdate = teacherService.findByEmail(email);
        teacherToUpdate.setFirstname(updateRequest.getFirstname());
        teacherToUpdate.setLastname(updateRequest.getLastname());
        teacherToUpdate.setStatus(updateRequest.getStatus());
        Role role = roleService.findByRoleCode(updateRequest.getRole());
        teacherToUpdate.setRole(role);
        try {
            Date dateOfBirth = DateUtils.getDate(updateRequest.getDateOfBirth());
            teacherToUpdate.setDateOfBirth(dateOfBirth);
        } catch (Exception e) {
            log.error("Error updating teacher details of email {} and with token key: {}", email, tokenKey);
            throw new BusinessException(ResponseStatus.DATEFORMATERROR, e.getMessage());
        }
        log.info("SuccessFully updated teacher details with token key: {}", tokenKey);
        teacherService.save(teacherToUpdate);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS, "Successfully updated teacher!", null);
    }

    @Override
    public ResponseEntity<ResponseRestDto> getEmailsNotInCourse(String courseCode,String tokenKey) {
        Course course = courseService.findByCode(courseCode);
        if(course == null) {
            log.error("Failed to found course with code {} with token key: {}", courseCode, tokenKey);
            throw new BusinessException(ResponseStatus.NOTFOUND, "Not found course with code "+ courseCode);
        }
        List<String> emails = teacherService.findAll().stream()
                .filter(teacher -> teacher.getStatus() != Status.INACTIVE && teacher != course.getConvenor() && !course.getSubTeachers().contains(teacher))
                .map((teacher)-> teacher.getEmail()).collect(Collectors.toList());
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully get email!", emails);
    }

    @Override
    public void exportBySearchRequest(HttpServletResponse response, TeacherSearchRequest searchRequest, Function<Teacher, TeacherSearchResponse> mapper , String tokenKey) throws IOException {
        List<Teacher> teachers = teacherService.searchWithNoPageable(searchRequest);
        List<String> headers = Arrays.asList("Firstname", "Lastname", "Identify Code", "Date of Birth", "Email", "Role", "Status");
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            XSSFSheet sheet = ExcelUtils.createSheet(workbook, "Report");
            ExcelUtils.writeHeaderRow(workbook, sheet, headers);
            CellStyle style = ExcelUtils.getStyleDataRows(workbook,sheet);
            int rowCount = 1;
            for(Teacher teacher : teachers) {
                Row row = sheet.createRow(rowCount ++);
                int columnCount = 0;
                TeacherSearchResponse teacherResponse = mapper.apply(teacher);
                ExcelUtils.createCell(sheet, row, columnCount ++, teacherResponse.getFirstname(), style);
                ExcelUtils.createCell(sheet, row, columnCount ++, teacherResponse.getLastname(), style);
                ExcelUtils.createCell(sheet, row, columnCount ++, teacherResponse.getIdentifyCode(), style);
                ExcelUtils.createCell(sheet, row, columnCount ++, teacherResponse.getDateOfBirth(), style);
                ExcelUtils.createCell(sheet, row, columnCount ++, teacherResponse.getEmail(), style);
                ExcelUtils.createCell(sheet, row, columnCount ++, teacherResponse.getRole(), style);
                ExcelUtils.createCell(sheet, row, columnCount ++, teacherResponse.getStatus(), style);
            }
            log.info("Exported report in excel format with token key: {}", tokenKey);
            workbook.write(response.getOutputStream());
        } catch (IOException ex) {
            log.error("IO Error - [{}] with token key: {}",ex.getMessage(), tokenKey);
            throw new BusinessException(ResponseStatus.INTERNALERROR, ex.getMessage());
        } finally {
            response.getOutputStream().close();
        }
    }
}
