package com.dung.studentmanagement.facade.impl;

import com.dung.studentmanagement.dto.request.TeacherCollectionRequest;
import com.dung.studentmanagement.dto.request.TeacherPendingSearchRequest;
import com.dung.studentmanagement.dto.request.TeacherRegisterRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.dto.response.TeacherPendingSearchResponse;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.enums.Status;
import com.dung.studentmanagement.exception.BusinessException;
import com.dung.studentmanagement.facade.Facade;
import com.dung.studentmanagement.facade.RequestFacade;
import com.dung.studentmanagement.service.TeacherPendingService;
import com.dung.studentmanagement.service.TeacherService;
import com.dung.studentmanagement.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Facade
@Slf4j
public class RequestFacadeImpl implements RequestFacade {

    private final TeacherPendingService teacherPendingService;
    private final TeacherService teacherService;
    private final PasswordEncoder passwordEncoder;

    public RequestFacadeImpl(TeacherPendingService teacherPendingService, TeacherService teacherService, PasswordEncoder passwordEncoder) {
        this.teacherPendingService = teacherPendingService;
        this.teacherService = teacherService;
        this.passwordEncoder = passwordEncoder;

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> saveTeacherPendingRequest(TeacherRegisterRequest request, Function<TeacherRegisterRequest, TeacherPendingRequest> mapper, String tokenKey) {
        TeacherPendingRequest entity = mapper.apply(request);
        entity.setRequestedDate(new Date());
        String password = request.getPassword();
        entity.setPassword(passwordEncoder.encode(password));
        entity.setStatus(Status.PENDING);
        TeacherPendingRequest requestEntity = teacherPendingService.findByEmailOrIdentifyCode(entity.getEmail(), Long.valueOf(entity.getIdentifyCode()));
        if(teacherService.findByEmailOrIdentifyCode(entity.getEmail(), entity.getIdentifyCode()) != null) {
            throw new BusinessException(ResponseStatus.DUPLICATEDTEACHER, "Teacher is already approved");
        }
        if(requestEntity != null && requestEntity.getStatus() == Status.PENDING) {
            throw new BusinessException(ResponseStatus.DUPCATEDREQUEST,"Teacher Request is already existed");
        }
        teacherPendingService.save(entity);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Success", null);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> saveTeacherPendingRequest(TeacherCollectionRequest request, Function<TeacherRegisterRequest,TeacherPendingRequest> mapper, String tokenKey) {
        List<TeacherPendingRequest> entities = new ArrayList<>();
        request.getTeacherList().forEach((teacher)->{
            String password = teacher.getPassword();
            teacher.setPassword(passwordEncoder.encode(password));
            TeacherPendingRequest entity = mapper.apply(teacher);
            entity.setRequestedDate(new Date());
            entities.add(entity);
        });
        log.info("Saving {} teacher request with token key: {}", entities.size(), tokenKey);
        teacherPendingService.saveAll(entities);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully creating request!",null);
    }

    @Override
    public ResponseEntity<ResponseRestDto> searchTeacherPendingRequest(TeacherPendingSearchRequest criteria, Function<TeacherPendingRequest, TeacherPendingSearchResponse> mapper, Pageable pageable, String tokenKey) throws ParseException {
        Page<TeacherPendingRequest> search = teacherPendingService.search(criteria, pageable);
        List<TeacherPendingSearchResponse> teacherPendingSearchResponses = search.getContent().stream().map(mapper).collect(Collectors.toList());
        log.info("Getting {} responses from search request with token key: {}", teacherPendingSearchResponses.size(), tokenKey);
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"SUCCESS", search.getNumber(), search.getTotalPages(), teacherPendingSearchResponses);
    }
}
