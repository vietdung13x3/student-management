package com.dung.studentmanagement.facade.impl;

import com.dung.studentmanagement.dto.request.TeacherLoginRequest;
import com.dung.studentmanagement.dto.response.AuthResponse;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.entity.Teacher;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.facade.AuthenticationFacade;
import com.dung.studentmanagement.facade.Facade;
import com.dung.studentmanagement.service.JwtService;
import com.dung.studentmanagement.service.TeacherService;
import com.dung.studentmanagement.utils.CommonUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;


import javax.servlet.http.HttpServletRequest;

@Facade
public class AuthenticationFacadeImpl implements AuthenticationFacade {


    private final AuthenticationManager authManager;
    private final TeacherService teacherService;
    private final JwtService jwtService;

    public AuthenticationFacadeImpl(AuthenticationManager authManager, TeacherService teacherService, JwtService jwtService) {
        this.authManager = authManager;
        this.teacherService = teacherService;
        this.jwtService = jwtService;
    }

    @Override
    public ResponseEntity<ResponseRestDto> authenticate(TeacherLoginRequest loginRequest) {
        Teacher teacher = teacherService.findByEmail(loginRequest.getEmail());
        if(teacher == null) {
            throw new UsernameNotFoundException("Username not found");
        }
        authManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginRequest.getEmail(),
                loginRequest.getPassword()
        ));
        String jwtToken = jwtService.generateToken(teacher);
        AuthResponse authResponse = new AuthResponse();
        authResponse.setToken(jwtToken);
        authResponse.setRole(teacher.getRole().getRoleCode());
        authResponse.setExpiration(jwtService.extractExpiration(jwtToken));
        return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully log in!",authResponse);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResponseRestDto> logOut(HttpServletRequest request, String tokenKey) {
        try {
            String jwtToken = request.getHeader("Authorization");
            String bearerToken = jwtToken.substring("Bearer ".length());
            jwtService.saveBlackListToken(bearerToken);
            return CommonUtils.makeRes(ResponseStatus.SUCCESS,"Successfully log out!", null);
        }catch (Exception e) {
            return CommonUtils.makeRes(ResponseStatus.FORBIDDEN,"forbidden",null);
        }

    }
}
