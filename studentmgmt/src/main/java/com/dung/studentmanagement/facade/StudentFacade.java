package com.dung.studentmanagement.facade;

import com.dung.studentmanagement.dto.request.StudentSearchRequest;
import com.dung.studentmanagement.dto.request.UpdateStatusRequest;
import com.dung.studentmanagement.dto.request.UploadStudentRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.dto.response.StudentSearchResponse;
import com.dung.studentmanagement.entity.Student;
import com.dung.studentmanagement.entity.Teacher;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.function.Function;

public interface StudentFacade {
    ResponseEntity<ResponseRestDto> saveStudentRequest(UploadStudentRequest request, Function<UploadStudentRequest, Student> mapper, String tokenKey);
    ResponseEntity<ResponseRestDto> searchByParams(StudentSearchRequest params, Function<Student, StudentSearchResponse> mapper, Pageable pageable, String tokenKey);
    ResponseEntity<ResponseRestDto> importStudents(MultipartFile file, String tokenKey);

    ResponseEntity<ResponseRestDto> findById(Long id, Function<Student, StudentSearchResponse> mapper, String tokenKey);

    ResponseEntity<ResponseRestDto> updateStudentStatus(Long id,UpdateStatusRequest status, String tokenKey);

    ResponseEntity<ResponseRestDto> findStudentNotInCourse(String code, String tokenKey);
}
