package com.dung.studentmanagement.facade;

import com.dung.studentmanagement.dto.request.*;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.dto.response.TeacherSearchResponse;
import com.dung.studentmanagement.entity.Teacher;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.Function;

public interface TeacherFacade {
    ResponseEntity<ResponseRestDto> approveRequestList(ApproveTeacherRequest request, Function<TeacherPendingRequest, Teacher> mapper, String tokenKey);
    ResponseEntity<ResponseRestDto> rejectRequestList(RejectTeacherRequest request, String tokenKey);
    ResponseEntity<ResponseRestDto> search(TeacherSearchRequest searchRequest, Function<Teacher, TeacherSearchResponse> mapper, Pageable pageable, String tokenKey);
    ResponseEntity<ResponseRestDto> updateTeacher(UpdateTeacherRequest updateRequest, String tokenKey);
    ResponseEntity<ResponseRestDto> getEmailsNotInCourse(String code,String tokenKey);
    void exportBySearchRequest(HttpServletResponse response, TeacherSearchRequest searchRequest, Function<Teacher, TeacherSearchResponse> mapper , String tokenKey) throws IOException;
}
