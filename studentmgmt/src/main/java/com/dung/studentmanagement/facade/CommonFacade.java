package com.dung.studentmanagement.facade;

import com.dung.studentmanagement.dto.request.SavePostcodeRequest;
import com.dung.studentmanagement.dto.response.CourseDetailsResponse;
import com.dung.studentmanagement.dto.response.DistrictResponse;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.dto.response.TeachingPeriodResponse;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.District;
import com.dung.studentmanagement.entity.TeachingPeriod;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.function.Function;

public interface CommonFacade {
    ResponseEntity<ResponseRestDto> getDistricts(Function<District, DistrictResponse> mapper, String tokenKey);
    ResponseEntity<ResponseRestDto> deleteCacheByKey(String key, String tokenKey);
    ResponseEntity<ResponseRestDto> savePostcode(SavePostcodeRequest savePostcodeRequest, String tokenKey);
    ResponseEntity<ResponseRestDto> getPostcodesByDistrict(String districtName, String tokenKey);
    ResponseEntity<ResponseRestDto> getStudyPeriod(Function<TeachingPeriod, TeachingPeriodResponse> mapper, String tokenKey);
}
