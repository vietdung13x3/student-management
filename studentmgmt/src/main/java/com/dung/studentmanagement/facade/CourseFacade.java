package com.dung.studentmanagement.facade;

import com.dung.studentmanagement.dto.request.CourseCreatedRequest;
import com.dung.studentmanagement.dto.request.CourseSearchRequest;
import com.dung.studentmanagement.dto.response.*;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Student;
import com.dung.studentmanagement.entity.Teacher;
import com.dung.studentmanagement.enums.CourseStatus;
import com.dung.studentmanagement.enums.Period;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public interface CourseFacade {
    public ResponseEntity<ResponseRestDto> savedCourse(CourseCreatedRequest createdRequest, Function<CourseCreatedRequest, Course> mapper, String tokenKey);
    public ResponseEntity<ResponseRestDto> searchCourse(CourseSearchRequest searchRequest, Function<Course, CourseSearchResponse> mapper, Pageable pageable, String tokenKey);
    ResponseEntity<ResponseRestDto> getCourseByCode(String code, Function<Course, CourseDetailsResponse> mapper, String tokenKey);
    ResponseEntity<ResponseRestDto> updateCourseStatus(CourseStatus status, String tokenKey, String code);
    ResponseEntity<ResponseRestDto> assignCoursePeriod(String code,Period period, String tokenKey);
    ResponseEntity<ResponseRestDto> getTeacherByCourse(String code, Function<Teacher, TeacherSearchResponse> mapper, Pageable pageable ,String tokenKey);
    ResponseEntity<ResponseRestDto> saveCourseTeacher(Collection<String> emails, String code, String tokenKey);

    ResponseEntity<ResponseRestDto> deleteCourseTeacher(String code, String email, String tokenKey);

    ResponseEntity<ResponseRestDto> saveCourseStudent(String courseCode, List<Long> identifyCode, String tokenKey);

    ResponseEntity<ResponseRestDto> getStudentsInCourse(String code, Function<Student, StudentSearchResponse> mapper, Pageable pageable, String tokenKey);

    ResponseEntity<ResponseRestDto> deleteStudentFromCourse(String courseCode, Long studentId, String tokenKey);

    ResponseEntity<ResponseRestDto> assignConvenor(String code, String teacherEmail, String tokenKey);
}
