package com.dung.studentmanagement.facade;

import com.dung.studentmanagement.dto.request.TeacherCollectionRequest;
import com.dung.studentmanagement.dto.request.TeacherPendingSearchRequest;
import com.dung.studentmanagement.dto.request.TeacherRegisterRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.dto.response.TeacherPendingSearchResponse;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.function.Function;

public interface RequestFacade {
    ResponseEntity<ResponseRestDto> saveTeacherPendingRequest(TeacherRegisterRequest request, Function<TeacherRegisterRequest, TeacherPendingRequest> mapper, String tokenKey);
    ResponseEntity<ResponseRestDto> saveTeacherPendingRequest(TeacherCollectionRequest request, Function<TeacherRegisterRequest, TeacherPendingRequest> mapper, String tokenKey);
    ResponseEntity<ResponseRestDto> searchTeacherPendingRequest(TeacherPendingSearchRequest criteria, Function<TeacherPendingRequest, TeacherPendingSearchResponse> mapper, Pageable pageable, String tokenKey) throws ParseException;
}
