package com.dung.studentmanagement.exception;

import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.service.MakeResService;
import com.dung.studentmanagement.utils.CommonUtils;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.text.ParseException;
import java.time.DateTimeException;
import java.time.format.DateTimeParseException;

@RestControllerAdvice
@RequiredArgsConstructor
public class ApiExceptionHandler {

    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ResponseRestDto> handleUsernameNotFound(UsernameNotFoundException ex) {
        return CommonUtils.makeRes(com.dung.studentmanagement.enums.ResponseStatus.NOTFOUND, ex.getMessage(), null);
    }

    @ExceptionHandler({ParseException.class, DateTimeParseException.class, DateTimeException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ResponseRestDto> handleDateException(RuntimeException ex) {
        return CommonUtils.makeRes(com.dung.studentmanagement.enums.ResponseStatus.DATEFORMATERROR, ex.getMessage(), null);
    }
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ResponseEntity<ResponseRestDto> handleJwtExpired(BusinessException ex) {
        return CommonUtils.makeRes(ex.getStatus(), ex.getMessage(),null);
    }
}
