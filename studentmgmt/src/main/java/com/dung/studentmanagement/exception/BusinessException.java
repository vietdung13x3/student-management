package com.dung.studentmanagement.exception;

import com.dung.studentmanagement.enums.ResponseStatus;

public class BusinessException extends RuntimeException{
    private final ResponseStatus status;
    private static final long serialVersionUID = 2709017199190266433L;
    public BusinessException(ResponseStatus errorCode, String message) {
        super(message);
        this.status = errorCode;
    }
    public ResponseStatus getStatus() {
        return this.status;
    }
}
