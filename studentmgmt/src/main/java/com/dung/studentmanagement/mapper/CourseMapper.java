package com.dung.studentmanagement.mapper;

import com.dung.studentmanagement.dto.request.CourseCreatedRequest;
import com.dung.studentmanagement.dto.response.CourseDetailsResponse;
import com.dung.studentmanagement.dto.response.CourseSearchResponse;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Teacher;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CourseMapper {
    CourseMapper INSTANCE = Mappers.getMapper(CourseMapper.class);

    Course fromCourse(CourseCreatedRequest createdRequest);
    @Mapping(source = "dateCreated", target = "dateCreated", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "teachingPeriod.start", target = "start", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "teachingPeriod.end", target = "end", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "convenor.email", target = "convenorEmail")
    CourseSearchResponse fromSearchResponse(Course course);

    @Mapping(source = "dateCreated", target = "dateCreated", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "teachingPeriod.start", target = "startDate", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "teachingPeriod.end", target = "endDate", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "teachingPeriod.name", target = "teachingPeriod")
    CourseDetailsResponse fromDetailsResponse(Course course);

    default String mapConvenor(Teacher teacher) {
        if(teacher == null) {
            return null;
        }
        if (teacher.getFirstname() != null && teacher.getLastname() != null) {
            return teacher.getFirstname() + " " + teacher.getLastname();
        }
        return null;
    }
}
