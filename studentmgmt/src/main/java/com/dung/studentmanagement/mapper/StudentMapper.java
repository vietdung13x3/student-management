package com.dung.studentmanagement.mapper;

import com.dung.studentmanagement.dto.request.UploadStudentRequest;
import com.dung.studentmanagement.dto.response.StudentSearchResponse;
import com.dung.studentmanagement.entity.Student;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StudentMapper {
    StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

    @Mapping(source = "dateOfBirth", target = "dateOfBirth", dateFormat = "dd/MM/yyyy")
    Student fromStudent(UploadStudentRequest request);

    @Mapping(source = "dateOfBirth", target = "dateOfBirth", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "dateJoined", target = "dateJoined", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "dateExpired", target = "dateExpired", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "district.name", target = "district")
    StudentSearchResponse fromSearchResponse(Student student);
}
