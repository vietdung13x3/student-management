package com.dung.studentmanagement.mapper;

import com.dung.studentmanagement.dto.response.DistrictResponse;
import com.dung.studentmanagement.dto.response.TeachingPeriodResponse;
import com.dung.studentmanagement.entity.District;
import com.dung.studentmanagement.entity.Postcode;
import com.dung.studentmanagement.entity.TeachingPeriod;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommonMapper {
    CommonMapper INSTANCE = Mappers.getMapper(CommonMapper.class);

    @Mapping(source = "postcodes", target = "postcodes")
    DistrictResponse fromDistrictResponse(District district);

    @Mapping(source = "name.code", target = "period")
    @Mapping(source = "start", target = "start", dateFormat = "dd/MM")
    @Mapping(source = "end", target = "end", dateFormat = "dd/MM")
    TeachingPeriodResponse fromPeriodResponse(TeachingPeriod teachingPeriod);

    default List<Long> mapPostcodes(Set<Postcode> postcodes) {
        return postcodes.stream().map(Postcode::getCode).collect(Collectors.toList());
    }
}
