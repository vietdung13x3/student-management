package com.dung.studentmanagement.mapper;

import com.dung.studentmanagement.dto.request.TeacherPendingSearchRequest;
import com.dung.studentmanagement.dto.request.TeacherRegisterRequest;
import com.dung.studentmanagement.dto.request.UpdateTeacherRequest;
import com.dung.studentmanagement.dto.response.TeacherPendingSearchResponse;
import com.dung.studentmanagement.dto.response.TeacherSearchResponse;
import com.dung.studentmanagement.entity.Teacher;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.exception.BusinessException;
import com.dung.studentmanagement.utils.CommonUtils;
import com.dung.studentmanagement.utils.DateUtils;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TeacherMapper {
    TeacherMapper INSTANCE = Mappers.getMapper(TeacherMapper.class);
//    PasswordEncoder passwordEncoder =

    @Mapping(target = "id", ignore = true)
    Teacher fromTeacher(TeacherPendingRequest pendingRequest);

    @Mapping(source = "dateOfBirth", target = "dateOfBirth", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "dateJoined", target = "dateJoined", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "role.roleCode", target = "role")
    TeacherSearchResponse fromTeacherSearchResponse(Teacher teacher);

    @Mapping(source = "dateOfBirth", target = "dateOfBirth", dateFormat = "dd/MM/yyyy")
    TeacherPendingRequest fromPendingRequest(TeacherRegisterRequest request);

    @Mapping(source = "dateOfBirth", target = "dateOfBirth", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "requestedDate", target = "requestedDate", dateFormat = "dd/MM/yyyy")
    @ValueMapping(source = "status", target = "status")
    TeacherPendingSearchResponse fromSearchResponse(TeacherPendingRequest request);
}
