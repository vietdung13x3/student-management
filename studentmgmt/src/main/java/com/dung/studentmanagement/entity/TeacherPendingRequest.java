package com.dung.studentmanagement.entity;


import com.dung.studentmanagement.dto.request.TeacherRegisterRequest;
import com.dung.studentmanagement.enums.Status;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "teacher_pending_request")
@Setter
@Getter
public class TeacherPendingRequest {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "firstname",nullable = false)
    private String firstname;

    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Column(name = "date_of_birth", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfBirth;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "identifyCode", nullable = false)
    private Long identifyCode;

    @Column(name = "requested_date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date requestedDate;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
