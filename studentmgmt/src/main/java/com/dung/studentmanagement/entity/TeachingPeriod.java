package com.dung.studentmanagement.entity;

import com.dung.studentmanagement.enums.Period;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "teaching_period")
@Getter
@Setter
public class TeachingPeriod {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", unique = true)
    @Enumerated(EnumType.STRING)
    private Period name;

    @Column(name = "start")
    private Date start;

    @Column(name = "end")
    private Date end;
}
