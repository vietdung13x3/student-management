package com.dung.studentmanagement.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "postcode")
@NoArgsConstructor
public class Postcode {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private Long code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "district")
    private District district;

    public Postcode(Long code, District district) {
        this.code = code;
        this.district = district;
    }
}
