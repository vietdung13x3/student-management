package com.dung.studentmanagement.entity;

import com.dung.studentmanagement.enums.Gender;
import com.dung.studentmanagement.enums.StudentStatus;
import lombok.*;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

@Entity
@Data
@Setter
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "identify_code")
    private Long identifyCode;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private StudentStatus status;

    @Column(name = "dateOfBirth")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfBirth;

    @Column(name = "date_joined")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateJoined;

    @Column(name = "date_expired")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExpired;

    @ManyToMany(mappedBy = "enrolledStudents", fetch = FetchType.LAZY)
    private Set<Course> courses;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "district")
    private District district;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

}
