package com.dung.studentmanagement.entity;

import com.dung.studentmanagement.enums.CourseStatus;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "course")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @OneToOne
    @JoinColumn(name = "period")
    private TeachingPeriod teachingPeriod;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "dateCreated", nullable = false)
    private Date dateCreated;

    @Lob
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private CourseStatus status;

    @OneToOne
    @JoinColumn(name = "convenor")
    private Teacher convenor;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "course_management", joinColumns = @JoinColumn(name = "course_id"), inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private Set<Teacher> subTeachers;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "enrolled_students", joinColumns = @JoinColumn(name = "course_id"), inverseJoinColumns = @JoinColumn(name = "student_id"))
    private Set<Student> enrolledStudents;

}
