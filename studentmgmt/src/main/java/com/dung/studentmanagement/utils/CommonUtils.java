package com.dung.studentmanagement.utils;

import com.dung.studentmanagement.dto.request.ImportStudentRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.http.ResponseEntity;

import java.util.Date;

public interface CommonUtils {
    static String makeTokenKey() {
        return "studentmgmt_" + SequenceNumber.INSTANCE.next();
    }
    static ResponseEntity<ResponseRestDto> makeRes(ResponseStatus status, String message, Object data) {
        ResponseRestDto response = ResponseRestDto.builder().status(status.getCode()).message(message).data(data).time(new Date()).build();
        return ResponseEntity.ok(response);
    }
    static ResponseEntity<ResponseRestDto> makeRes(ResponseStatus status, String message, Integer currentPage, Integer totalPage,Object data) {
        ResponseRestDto response = ResponseRestDto.builder().status(status.getCode()).currentPage(currentPage).totalPage(totalPage).message(message).data(data).time(new Date()).build();
        return ResponseEntity.ok(response);
    }
    static ResponseEntity<ResponseRestDto> makeRes(ResponseStatus status, String message, Integer currentPage, Long totalElements,Integer totalPage,Object data) {
        ResponseRestDto response = ResponseRestDto.builder().status(status.getCode()).currentPage(currentPage).totalElements(totalElements).totalPage(totalPage).message(message).data(data).time(new Date()).build();
        return ResponseEntity.ok(response);
    }
    static ResponseRestDto makeResDto(ResponseStatus status, String message) {
        return ResponseRestDto.builder().status(status.getCode()).message(message).data(null).build();
    }
    static String makeLikeExpression(String value) {
        if(value == null) {
            value = "";
        }
        return "%" + value + "%";
    }
    static String stringify(Object o) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(o);
    }
}
