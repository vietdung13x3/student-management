package com.dung.studentmanagement.utils;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public interface DateUtils {
    String mainDatePattern = "dd/MM/yyyy";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(mainDatePattern);
    String regexDatePattern = "^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$";
    static Date getDate(String date) {
        if(StringUtils.isBlank(date)) {
            return null;
        }
        LocalDate localDate = LocalDate.from(formatter.parse(date));
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

}
