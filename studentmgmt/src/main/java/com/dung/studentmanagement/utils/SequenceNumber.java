package com.dung.studentmanagement.utils;

public class SequenceNumber {
    public static SequenceNumber INSTANCE = new SequenceNumber();

    private static Integer value = 1; // Default value

    private static final Integer DEFAULT_VALUE = 1;
    public synchronized Integer next() {
        if(value == Integer.MAX_VALUE) {
            value = DEFAULT_VALUE;
        }
        return value ++;
    }
    private SequenceNumber() {

    }
}
