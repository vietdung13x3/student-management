package com.dung.studentmanagement.utils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;

public interface ExcelUtils {

    static final String FONT_NAME = "Arial";
    static final String EXTENSION = ".xlsx";

    static XSSFSheet createSheet(XSSFWorkbook workbook, String sheetName) {
        return workbook.createSheet(sheetName);
    }
    static void writeHeaderRow(XSSFWorkbook workbook,XSSFSheet sheet, Collection<String> titles) {
        CellStyle style = workbook.createCellStyle();
        Row row = sheet.createRow(0);
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        font.setFontName(FONT_NAME);
        style.setFont(font);
        int count = 0;
        for(String title : titles) {
            createCell(sheet, row, count, title, style);
            count++;
        }
    }
    static CellStyle getStyleDataRows(XSSFWorkbook workbook, XSSFSheet sheet) {
        XSSFFont font = workbook.createFont();
        font.setFontName(FONT_NAME);
        font.setFontHeight(14);
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }
    static void createCell(XSSFSheet sheet, Row row, Integer columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        cell.setCellValue(String.valueOf(value));
        cell.setCellStyle(style);
    }
    static boolean isXlsx(MultipartFile file) {
        String originalFilename = file.getOriginalFilename();
        return originalFilename.endsWith(ExcelUtils.EXTENSION);
    }

    static String getValue(XSSFRow row, Integer col) {
        row.getCell(col).setCellType(CellType.STRING);
        return row.getCell(col).getStringCellValue();
    }
}
