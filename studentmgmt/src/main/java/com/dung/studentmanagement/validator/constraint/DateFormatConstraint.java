package com.dung.studentmanagement.validator.constraint;

import com.dung.studentmanagement.validator.FormalDateFormat;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateFormatConstraint implements ConstraintValidator<FormalDateFormat, String> {
    private boolean isRequired;
    private String message;

    @Override
    public void initialize(FormalDateFormat constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        this.isRequired = constraintAnnotation.isRequired();
        this.message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(!isRequired && StringUtils.isBlank(value)) {
            return true;
        }
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate.parse(value, formatter);
            return true;
        }catch (DateTimeParseException e) {
            return false;
        }
    }
}
