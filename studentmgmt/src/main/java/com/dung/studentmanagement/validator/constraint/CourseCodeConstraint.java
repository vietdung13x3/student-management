package com.dung.studentmanagement.validator.constraint;

import com.dung.studentmanagement.service.TeacherService;
import com.dung.studentmanagement.validator.CourseCode;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class CourseCodeConstraint implements ConstraintValidator<CourseCode, String> {
    private String message;

    @Override
    public void initialize(CourseCode constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        this.message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return StringUtils.isNotBlank(value) && StringUtils.startsWith(value, "NTY");
    }
}
