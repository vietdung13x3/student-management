package com.dung.studentmanagement.validator;

import com.dung.studentmanagement.validator.constraint.DateFormatConstraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = DateFormatConstraint.class)
public @interface FormalDateFormat {
    public String message() default "Invalid date format!";

    public boolean isRequired() default true;

    // represents group of constraints
    public Class<?>[] groups() default {};

    // represents additional information about annotation
    public Class<? extends Payload>[] payload() default {};
}
