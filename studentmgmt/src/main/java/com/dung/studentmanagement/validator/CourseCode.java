package com.dung.studentmanagement.validator;

import com.dung.studentmanagement.validator.constraint.CourseCodeConstraint;
import com.dung.studentmanagement.validator.constraint.DateFormatConstraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import java.lang.annotation.*;

@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = CourseCodeConstraint.class)
public @interface CourseCode {
    public String message() default "Course code must start with NTY";

    public boolean isRequired() default true;

    // represents group of constraints
    public Class<?>[] groups() default {};

    // represents additional information about annotation
    public Class<? extends Payload>[] payload() default {};
}
