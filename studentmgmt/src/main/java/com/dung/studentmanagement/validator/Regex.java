package com.dung.studentmanagement.validator;

import com.dung.studentmanagement.validator.constraint.CourseCodeConstraint;
import com.dung.studentmanagement.validator.constraint.RegexConstraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = RegexConstraint.class)
public @interface Regex {
    String message() default "Input did not match the required format!";
    String pattern() default "";
    public boolean isRequired() default true;

    // represents group of constraints
    public Class<?>[] groups() default {};

    // represents additional information about annotation
    public Class<? extends Payload>[] payload() default {};
}
