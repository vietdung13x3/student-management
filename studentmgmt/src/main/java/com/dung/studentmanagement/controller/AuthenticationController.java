package com.dung.studentmanagement.controller;

import com.dung.studentmanagement.dto.request.TeacherLoginRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.facade.AuthenticationFacade;
import com.dung.studentmanagement.facade.TeacherFacade;
import com.dung.studentmanagement.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1.0/auth")
@CrossOrigin
@Slf4j
public class AuthenticationController {

    private final AuthenticationFacade authFacade;

    public AuthenticationController(AuthenticationFacade authFacade) {
        this.authFacade = authFacade;
    }


    @PostMapping("/do-login")
    public ResponseEntity<ResponseRestDto> login(HttpServletRequest http, @RequestBody TeacherLoginRequest request) {
//        throw new RuntimeException("asd");\
        String ip = http.getRemoteAddr();
        String tokenKey = CommonUtils.makeTokenKey();
        log.info("Request to login from [{}] with token key: {}", ip, tokenKey);
        return authFacade.authenticate(request);
    }

    @PostMapping("/do-logout")
    public ResponseEntity<ResponseRestDto> logout(HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Request to logout from [{}] with token key: {}",ip, tokenKey);
        return authFacade.logOut(request, tokenKey);
    }
}
