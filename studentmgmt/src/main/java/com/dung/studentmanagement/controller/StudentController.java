package com.dung.studentmanagement.controller;

import com.dung.studentmanagement.dto.request.SaveStudentRequest;
import com.dung.studentmanagement.dto.request.StudentSearchRequest;
import com.dung.studentmanagement.dto.request.UpdateStatusRequest;
import com.dung.studentmanagement.dto.request.UploadStudentRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.enums.StudentStatus;
import com.dung.studentmanagement.facade.StudentFacade;
import com.dung.studentmanagement.mapper.StudentMapper;
import com.dung.studentmanagement.mapper.TeacherMapper;
import com.dung.studentmanagement.rabbitmq.RabbitProducer;
import com.dung.studentmanagement.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1.0/student")
public class StudentController {
    private final StudentFacade studentFacade;
    @PostMapping
    public ResponseEntity<ResponseRestDto> saveStudent(HttpServletRequest request,@Valid @RequestBody UploadStudentRequest saveRequest) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting save student from ip [{}] with token key: {}", ip, tokenKey);
        return studentFacade.saveStudentRequest(saveRequest, StudentMapper.INSTANCE::fromStudent, tokenKey);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseRestDto> updateStudentStatus(HttpServletRequest request, @PathVariable Long id,@RequestBody UpdateStatusRequest status){
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting update student's status from ip [{}] with token key: {}", ip, tokenKey);
        return studentFacade.updateStudentStatus(id,status, tokenKey);
    }

    @GetMapping
    public ResponseEntity<ResponseRestDto> getStudent(HttpServletRequest request, StudentSearchRequest studentSearchRequest, Pageable pageable) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting get students from ip [{}] with token key: {}", ip, tokenKey);
        return studentFacade.searchByParams(studentSearchRequest,StudentMapper.INSTANCE::fromSearchResponse ,pageable, tokenKey);
    }

    @GetMapping("/not-in-course")
    public ResponseEntity<ResponseRestDto> getStudentNotInCourse(HttpServletRequest request, String code) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting get student details from ip [{}] with token key: {}",ip, tokenKey);
        return studentFacade.findStudentNotInCourse(code, tokenKey);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseRestDto> getStudentById(HttpServletRequest request,@PathVariable Long id) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting get student by id {} with token key: {}",id, tokenKey);
        return studentFacade.findById(id, StudentMapper.INSTANCE::fromSearchResponse, tokenKey);
    }

    @PostMapping("/import")
    public ResponseEntity<ResponseRestDto> importStudents(HttpServletRequest request,@RequestBody MultipartFile file) {
        String ip = request.getRemoteAddr();
        String token = CommonUtils.makeTokenKey();
        log.info("Starting upload students from {} with token key: {}",file.getOriginalFilename(),token);
        return studentFacade.importStudents(file, token);
    }
}
