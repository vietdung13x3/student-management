package com.dung.studentmanagement.controller;

import com.dung.studentmanagement.dto.request.SavePostcodeRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.facade.CommonFacade;
import com.dung.studentmanagement.mapper.CommonMapper;
import com.dung.studentmanagement.redis.RedisService;
import com.dung.studentmanagement.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1.0/commons")
public class CommonController {
    private final CommonFacade commonFacade;
    @GetMapping("/district")
    public ResponseEntity<ResponseRestDto> getDistrict(HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting get list of district from ip [{}] with token key: {}", ip, tokenKey);
        return commonFacade.getDistricts(CommonMapper.INSTANCE::fromDistrictResponse, tokenKey);
    }

    @GetMapping("/study-period")
    public ResponseEntity<ResponseRestDto> getStudyPeriod(HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting get list of study period from ip [{}] with token key: {}",ip, tokenKey);
        return commonFacade.getStudyPeriod(CommonMapper.INSTANCE::fromPeriodResponse,tokenKey);
    }

    @GetMapping("/postcode")
    public ResponseEntity<ResponseRestDto> getPostcodesByDistrict(HttpServletRequest request, @RequestParam(name = "districtName") String districtName) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting get postcodes by district from ip [{}] with token key: {}",ip, tokenKey);
        return commonFacade.getPostcodesByDistrict(districtName, tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("/postcode")
    public ResponseEntity<ResponseRestDto> savePostcodes(HttpServletRequest request, @RequestBody SavePostcodeRequest postcodeToSave) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting saving postcode from ip [{}] with token key: {}", ip, tokenKey);
        return commonFacade.savePostcode(postcodeToSave, tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping("/cache")
    public ResponseEntity<ResponseRestDto> deleteCache(HttpServletRequest request, @RequestParam(name = "key", required = true) String key) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting deleting data from redis from ip [{}] with token key: {}", ip, tokenKey);
        return commonFacade.deleteCacheByKey(key, tokenKey);
    }

}
