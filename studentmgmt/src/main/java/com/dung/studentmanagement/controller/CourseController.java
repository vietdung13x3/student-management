package com.dung.studentmanagement.controller;

import com.dung.studentmanagement.dto.request.*;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.enums.CourseStatus;
import com.dung.studentmanagement.enums.Period;
import com.dung.studentmanagement.facade.CourseFacade;
import com.dung.studentmanagement.facade.TeacherFacade;
import com.dung.studentmanagement.mapper.CourseMapper;
import com.dung.studentmanagement.mapper.StudentMapper;
import com.dung.studentmanagement.mapper.TeacherMapper;
import com.dung.studentmanagement.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/v1.0/course")
@AllArgsConstructor
public class CourseController {
    private final CourseFacade courseFacade;
    private final TeacherFacade teacherFacade;

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping
    public ResponseEntity<ResponseRestDto> saveCourse(HttpServletRequest request,@RequestBody @Valid CourseCreatedRequest savedRequest) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip =  request.getRemoteAddr();
        log.info("Request to save course with ip [{}] and with token key: {}",ip, tokenKey);
        return courseFacade.savedCourse(savedRequest, CourseMapper.INSTANCE::fromCourse, tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','MAKER')")
    @PostMapping("/{courseCode}/student")
    public ResponseEntity<ResponseRestDto> saveEnrolledStudent(HttpServletRequest request,@PathVariable String courseCode,@Valid @RequestBody SaveStudentToCourse student) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting saving student to course from ip {} with token key: {}",ip, tokenKey);
        return courseFacade.saveCourseStudent(courseCode, student.getIds(), tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','MAKER')")
    @DeleteMapping("/{courseCode}/student/{studentId}")
    public ResponseEntity<ResponseRestDto> deleteStudentFromCourse(HttpServletRequest request, @PathVariable String courseCode, @PathVariable Long studentId) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting deleting student from course from ip {} with token key: {}",ip, tokenKey);
        return courseFacade.deleteStudentFromCourse(courseCode, studentId, tokenKey);
    }

    @GetMapping("/{code}/student")
    public ResponseEntity<ResponseRestDto> getStudentsInCourse(HttpServletRequest request, @PathVariable String code, Pageable pageable) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting getting student from course from ip {} with token key: {}", ip, tokenKey);
        return courseFacade.getStudentsInCourse(code, StudentMapper.INSTANCE::fromSearchResponse,pageable, tokenKey);
    }


    @GetMapping
    public ResponseEntity<ResponseRestDto> searchCourse(HttpServletRequest request,@Valid CourseSearchRequest searchRequest, Pageable pageable){
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        return courseFacade.searchCourse(searchRequest,CourseMapper.INSTANCE::fromSearchResponse, pageable,tokenKey);
    }

    @GetMapping("/{code}")
    public ResponseEntity<ResponseRestDto> getCourseByCode(@PathVariable("code") String code, HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        return courseFacade.getCourseByCode(code, CourseMapper.INSTANCE::fromDetailsResponse, tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PutMapping("/{code}")
    public ResponseEntity<ResponseRestDto> updateCourseStatus(@PathVariable("code") String code ,@RequestParam(required = true) CourseStatus status, HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting update course from ip [{}] with token key: {}", ip, tokenKey);
        return courseFacade.updateCourseStatus(status, tokenKey, code);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PutMapping("/{code}/teaching-period")
    public ResponseEntity<ResponseRestDto> assignTeachingPeriod(@PathVariable("code") String code,@RequestParam(required = true) Period period, HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting assign teaching period from ip [{}] with token key: {}", ip, tokenKey);
        return courseFacade.assignCoursePeriod(code, period, tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("/{code}/teachers")
    public ResponseEntity<ResponseRestDto> addCourseTeacher(@PathVariable("code") String code, @RequestBody CourseTeacherRequest courseTeacherRequest, HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting add teacher to course from ip {} with token key: {}", ip, tokenKey);
        return courseFacade.saveCourseTeacher(courseTeacherRequest.getEmails(), code, tokenKey);
    }

    @GetMapping("/{code}/teachers")
    public ResponseEntity<ResponseRestDto> getSubTeachers(@PathVariable("code") String code, HttpServletRequest request, Pageable pageable) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting get teachers from a course from ip {} with token key: {}", ip, tokenKey);
        return courseFacade.getTeacherByCourse(code, TeacherMapper.INSTANCE::fromTeacherSearchResponse, pageable,tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping("/{code}/teachers")
    public ResponseEntity<ResponseRestDto> deleteCourseTeacher(@PathVariable("code") String code, @RequestBody CourseTeacherRequest courseTeacherRequest, HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting delete teachers from ip {} with token key: {}", ip, tokenKey);
        return courseFacade.deleteCourseTeacher(code, courseTeacherRequest.getEmail(), tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PutMapping("/{code}/convenor")
    public ResponseEntity<ResponseRestDto> assignCourseConvenor(@PathVariable String code, @RequestBody CourseConvenorRequest convenorRequest, HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting assign convenor from ip {} with token key: {}", ip, tokenKey);
        return courseFacade.assignConvenor(code, convenorRequest.getTeacherEmail(), tokenKey);
    }
 }
