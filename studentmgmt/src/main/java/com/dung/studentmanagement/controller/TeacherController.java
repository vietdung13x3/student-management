package com.dung.studentmanagement.controller;

import com.dung.studentmanagement.dto.request.*;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.facade.TeacherFacade;
import com.dung.studentmanagement.mapper.TeacherMapper;
import com.dung.studentmanagement.utils.CommonUtils;
import com.dung.studentmanagement.utils.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;


@Controller
@RequestMapping("/api/v1.0/teacher")
@Slf4j
public class TeacherController {

    private final TeacherFacade teacherFacade;

    public TeacherController(TeacherFacade educationFacade) {
        this.teacherFacade = educationFacade;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','MAKER')")
    @DeleteMapping("/reject-request")
    public ResponseEntity<ResponseRestDto> rejectTeacher(HttpServletRequest request, @RequestBody @Valid RejectTeacherRequest teacherRequest) {
        String ip = request.getRemoteAddr();
        String tokenKey = CommonUtils.makeTokenKey();
        log.info("Request to reject [{}] pending requests from ip [{}] with token key: {}", teacherRequest.getIds().size(), ip, tokenKey);
        return teacherFacade.rejectRequestList(teacherRequest, tokenKey);
    }

    @PostMapping("/search/export")
    public void export(HttpServletRequest request, HttpServletResponse response,@Valid TeacherSearchRequest searchRequest) throws IOException {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String currentDate = formatter.format(new Date());
        String headerKey =  "Content-Disposition";
        String headerValue = "attachment; filename=teachers_" + currentDate + ExcelUtils.EXTENSION;
        response.setHeader(headerKey, headerValue);
        log.info("Starting export excel from ip [{}] with token key: {}", ip, tokenKey);
        teacherFacade.exportBySearchRequest(response, searchRequest,TeacherMapper.INSTANCE::fromTeacherSearchResponse,tokenKey);
    }


    @PreAuthorize("hasAnyAuthority('ADMIN','MAKER')")
    @PutMapping("/approve-request")
    public ResponseEntity<ResponseRestDto> saveTeacher(HttpServletRequest request,@RequestBody @Valid ApproveTeacherRequest teacherRequest) {
        String ip = request.getRemoteAddr();
        String tokenKey = CommonUtils.makeTokenKey();
        log.info("Request to approve pending request [{}] requests from ip [{}] with token key: {}", teacherRequest.getIds().size(), ip, tokenKey);
        return teacherFacade.approveRequestList(teacherRequest, TeacherMapper.INSTANCE::fromTeacher, tokenKey);
    }

    @GetMapping("/search")
    public ResponseEntity<ResponseRestDto> searchTeacher(HttpServletRequest request, @Valid TeacherSearchRequest searchRequest, Pageable pageable) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting search teacher from ip [{}] with token key: {}", ip, tokenKey);
        return teacherFacade.search(searchRequest,TeacherMapper.INSTANCE::fromTeacherSearchResponse, pageable,tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','MAKER')")
    @PutMapping("/update")
    public ResponseEntity<ResponseRestDto> updateTeacher(HttpServletRequest request, @RequestBody @Valid UpdateTeacherRequest updateRequest) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting update teacher from ip [{}] with token key: {}", ip, tokenKey);
        return teacherFacade.updateTeacher(updateRequest,tokenKey);
    }

    @GetMapping("/emails")
    public ResponseEntity<ResponseRestDto> getEmailsNotInCourse(@RequestParam(required = true, name = "ignoreCourseCode") String ignoreCourseCode,HttpServletRequest request) {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Starting get all emails from ip [{}] with token key: {}", ip, tokenKey);
        return teacherFacade.getEmailsNotInCourse(ignoreCourseCode,tokenKey);
    }
}
