package com.dung.studentmanagement.controller;

import com.dung.studentmanagement.dto.request.TeacherCollectionRequest;
import com.dung.studentmanagement.dto.request.TeacherPendingSearchRequest;
import com.dung.studentmanagement.dto.request.TeacherRegisterRequest;
import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import com.dung.studentmanagement.facade.RequestFacade;
import com.dung.studentmanagement.mapper.TeacherMapper;
import com.dung.studentmanagement.service.TeacherPendingService;
import com.dung.studentmanagement.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping("/api/v1.0/request")
@Slf4j
public class RequestController {
    private final RequestFacade requestFacade;

    public RequestController(RequestFacade requestFacade) {
        this.requestFacade = requestFacade;
    }

    @PostMapping("/teacher-pending/list")
    ResponseEntity<ResponseRestDto> saveTeacherRequestList(HttpServletRequest request, @RequestBody TeacherCollectionRequest teacherRequests) {
        String ip = request.getRemoteAddr();
        String tokenKey = CommonUtils.makeTokenKey();
        log.info("Start saving teacher pending request from [{}] with token key: {}", ip, tokenKey);
        return requestFacade.saveTeacherPendingRequest(teacherRequests, TeacherMapper.INSTANCE::fromPendingRequest, tokenKey);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','MAKER')")
    @PostMapping("/teacher-pending")
    ResponseEntity<ResponseRestDto> saveTeacherRequest(HttpServletRequest request,@Valid @RequestBody TeacherRegisterRequest teacherRequest) {
        String ip = request.getRemoteAddr();
        String tokenKey = CommonUtils.makeTokenKey();
        log.info("Start saving teacher pending request from [{}] with token key: {}", ip, tokenKey);
        return requestFacade.saveTeacherPendingRequest(teacherRequest, TeacherMapper.INSTANCE::fromPendingRequest, tokenKey);
    }

    @GetMapping("/teacher-pending")
    ResponseEntity<ResponseRestDto> searchTeacherRequest(HttpServletRequest request,@Valid TeacherPendingSearchRequest criteria, Pageable pageable) throws ParseException {
        String tokenKey = CommonUtils.makeTokenKey();
        String ip = request.getRemoteAddr();
        log.info("Searching teacher pending request from [{}] with token key: {}",ip, tokenKey);
        return requestFacade.searchTeacherPendingRequest(criteria, TeacherMapper.INSTANCE::fromSearchResponse, pageable, tokenKey);
    }
}
