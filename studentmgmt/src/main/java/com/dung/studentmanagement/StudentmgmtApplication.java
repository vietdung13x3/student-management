package com.dung.studentmanagement;

import com.dung.studentmanagement.entity.Role;
import com.dung.studentmanagement.entity.Teacher;
import com.dung.studentmanagement.enums.RoleCode;
import com.dung.studentmanagement.enums.Status;
import com.dung.studentmanagement.repository.RoleRepository;
import com.dung.studentmanagement.repository.TeacherRepository;
import com.dung.studentmanagement.security.config.ApplicationConfig;
import com.dung.studentmanagement.service.RoleService;
import com.dung.studentmanagement.service.TeacherService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@AllArgsConstructor
@EnableRabbit
@Slf4j
public class StudentmgmtApplication implements CommandLineRunner {

	private final TeacherService teacherService;
	private final RoleService roleService;
	private final BCryptPasswordEncoder encoder;
	public static void main(String[] args) {
		SpringApplication.run(StudentmgmtApplication.class, args);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void run(String... args) throws Exception {
		final String initEmail = "vietdung13x3";
		final String initPassword = "dung0904748106";
		final String initIdentify = "000000000001";
		Role admin = new Role();
		admin.setRoleCode(RoleCode.ADMIN);
		Role staff = new Role();
		staff.setRoleCode(RoleCode.STAFF);
		Role maker = new Role();
		maker.setRoleCode(RoleCode.MAKER);
		if(roleService.findByRoleCode(RoleCode.ADMIN) == null && roleService.findByRoleCode(RoleCode.STAFF) == null && roleService.findByRoleCode(RoleCode.MAKER) == null) {
			roleService.saveAll(Arrays.asList(admin, staff, maker));
		}
		if(teacherService.findByEmail(initEmail) == null) {
			Teacher entity = new Teacher();
			entity.setRole(roleService.findByRoleCode(RoleCode.ADMIN));
			entity.setEmail(initEmail);
			entity.setPassword(encoder.encode(initPassword));
			entity.setStatus(Status.ACTIVE);
			entity.setIdentifyCode(Long.valueOf(initIdentify));
			log.info("Initially started application!");
			teacherService.save(entity);
		}
	}
}
