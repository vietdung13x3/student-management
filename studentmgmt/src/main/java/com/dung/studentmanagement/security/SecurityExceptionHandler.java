package com.dung.studentmanagement.security;

import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.utils.CommonUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SecurityExceptionHandler implements AccessDeniedHandler, AuthenticationEntryPoint {


    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
//        if (accessDeniedException instanceof AccessDeniedException) {
        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        ResponseRestDto resp = CommonUtils.makeResDto(ResponseStatus.FORBIDDEN, "You don't have permission to perform this action!");
        response.getWriter().write(CommonUtils.stringify(resp));
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        ResponseRestDto resp = CommonUtils.makeResDto(ResponseStatus.UNAUTHORIZED,"You are not authorized");
        response.getWriter().write(CommonUtils.stringify(resp));
    }
}
