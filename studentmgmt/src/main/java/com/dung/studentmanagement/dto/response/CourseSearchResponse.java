package com.dung.studentmanagement.dto.response;

import com.dung.studentmanagement.enums.CourseStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CourseSearchResponse {
    private String code;
    private String name;
    private String convenorEmail;
    private String start;
    private String end;
    private String dateCreated;
    private String convenor;
    private CourseStatus status;

}
