package com.dung.studentmanagement.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TeacherCollectionRequest {
    private List<TeacherRegisterRequest> teacherList;
}
