package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.enums.Period;
import com.dung.studentmanagement.validator.CourseCode;
import com.dung.studentmanagement.validator.FormalDateFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Data
@Getter
@Setter
public class CourseCreatedRequest {
    @CourseCode
    private String code;

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @NotBlank
    private String convenorEmail;
}
