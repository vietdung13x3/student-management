package com.dung.studentmanagement.dto.response;

import lombok.Data;

import java.util.Collection;
import java.util.List;

@Data
public class DistrictResponse {
    private Long id;
    private String name;
    private Collection<Long> postcodes;
}
