package com.dung.studentmanagement.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TeacherPendingSearchRequest {
    @Builder.Default
    private String id = "";

    @Builder.Default
    private String firstname = "";

    @Builder.Default
    private String lastname = "";

    @Builder.Default
    private String email = "";

    @Builder.Default
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String dateOfBirth = "";

    @Builder.Default
    private String fromDate = "";

    @Builder.Default
    private String toDate = "";

    @Builder.Default
    private String identifyCode = "";
}
