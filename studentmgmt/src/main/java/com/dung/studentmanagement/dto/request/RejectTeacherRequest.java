package com.dung.studentmanagement.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Getter
@Setter
public class RejectTeacherRequest {
    @NotNull
    List<Long> ids;
}
