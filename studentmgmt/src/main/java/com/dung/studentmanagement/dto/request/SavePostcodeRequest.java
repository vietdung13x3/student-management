package com.dung.studentmanagement.dto.request;

import lombok.Data;

import java.util.Set;

@Data
public class SavePostcodeRequest {
    private Long districtId;
    private Set<Long> postcode;
}
