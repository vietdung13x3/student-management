package com.dung.studentmanagement.dto.response;

import com.dung.studentmanagement.enums.Status;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@Getter
@Setter
public class TeacherPendingSearchResponse {
    private Long id;
    private String firstname;
    private String lastname;
    private String dateOfBirth;
    private String email;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String requestedDate;

    private String identifyCode;

    private String status;
}
