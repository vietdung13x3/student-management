package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.enums.Period;
import com.dung.studentmanagement.utils.DateUtils;
import com.dung.studentmanagement.validator.Regex;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CourseSearchRequest {
    private String code;
    private String name;
//    @Regex(pattern = DateUtils.regexDatePattern, isRequired = false)
    private String from;
//    @Regex(pattern = DateUtils.regexDatePattern, isRequired = false)
    private String to;
    private String convenorEmail;
    private Period studyPeriod;
}
