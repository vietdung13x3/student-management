package com.dung.studentmanagement.dto.response;

import com.dung.studentmanagement.enums.Gender;
import com.dung.studentmanagement.enums.StudentStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentSearchResponse {
    private Long id;
    private String firstname;
    private String lastname;
    private Long identifyCode;
    private String dateOfBirth;
    private String dateExpired;
    private String dateJoined;
    private Gender gender;
    private Long postcode;
    private String district;
    private StudentStatus status;
    List<CourseDetailResponse> courses;
}
