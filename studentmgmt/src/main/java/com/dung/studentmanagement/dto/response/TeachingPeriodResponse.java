package com.dung.studentmanagement.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeachingPeriodResponse {
    private String period;
    private String start;
    private String end;
}
