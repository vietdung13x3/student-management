package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.enums.RoleCode;
import com.dung.studentmanagement.enums.Status;
import com.dung.studentmanagement.validator.FormalDateFormat;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
public class UpdateTeacherRequest {

    @NotBlank
    private String email;

    private Long identifyCode;
    @NotNull
    private Status status;
    @NotNull
    private RoleCode role;
    @NotBlank
    private String firstname;
    @NotBlank
    private String lastname;

    @NotBlank
    private String dateOfBirth;
}
