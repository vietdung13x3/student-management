package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.enums.RoleCode;
import com.dung.studentmanagement.enums.Status;
import com.dung.studentmanagement.validator.FormalDateFormat;
import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;

@Data
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TeacherSearchRequest {
    private String firstname;
    private String lastname;
    private String email;
    private String identifyCode;
    private String dateOfBirth;
    private Status status;
    private RoleCode role;

    @FormalDateFormat(isRequired = false)
    private String fromDate;

    @FormalDateFormat(isRequired = false)
    private String toDate;
}
