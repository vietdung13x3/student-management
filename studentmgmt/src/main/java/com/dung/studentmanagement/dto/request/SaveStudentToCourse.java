package com.dung.studentmanagement.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class SaveStudentToCourse {
    @NotNull
    private List<Long> ids;
}
