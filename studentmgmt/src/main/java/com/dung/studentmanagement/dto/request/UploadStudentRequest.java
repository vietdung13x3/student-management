package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.enums.Gender;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@Data
@Builder
public class UploadStudentRequest implements Serializable {
    @NotBlank
    private String firstname;
    @NotBlank
    private String lastname;
    @NotNull
    private Long identifyCode;
    @NotBlank
    private String dateOfBirth;
    @NotNull
    private Integer numberOfYears;
    @NotBlank
    private String postcode;
    @NotNull
    private Gender gender;
}
