package com.dung.studentmanagement.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchCourseRequest {
    private Long code;
    private String name;
}
