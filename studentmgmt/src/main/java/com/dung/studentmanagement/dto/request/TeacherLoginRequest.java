package com.dung.studentmanagement.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class TeacherLoginRequest {
    private String email;
    private String password;
}
