package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.entity.Student;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Data
@Builder
public class ImportStudentRequest {
    private List<UploadStudentRequest> students;
}
