package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.validator.Regex;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaveStudentRequest {
    @Regex
    private String firstname;
    private String lastname;

}
