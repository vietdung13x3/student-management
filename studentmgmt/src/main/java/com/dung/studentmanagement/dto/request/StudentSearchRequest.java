package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.enums.Gender;
import com.dung.studentmanagement.validator.FormalDateFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentSearchRequest {
    private String firstname;
    private String lastname;
    private String identifyCode;
    private Integer yearLeft;
    @FormalDateFormat(isRequired = false)
    private String joinedFrom;
    @FormalDateFormat(isRequired = false)
    private String joinedTo;
    @FormalDateFormat(isRequired = false)
    private String expiredFrom;
    @FormalDateFormat(isRequired = false)
    private String expiredTo;
    private String district;
    private Gender gender;
}
