package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.enums.RoleCode;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Getter
@Setter
public class ApproveTeacherRequest {
    @NotNull
    List<String> ids;

    @NotNull
    RoleCode roleCode;
}
