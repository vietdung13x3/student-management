package com.dung.studentmanagement.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class TeacherRegisterRequest {

    @NonNull
    @NotEmpty
    private String firstname;

    @NonNull
    @NotEmpty
    private String lastname;

    @NonNull
    @NotEmpty
    private String dateOfBirth;
    @NonNull
    @NotEmpty
    private String email;

    @NonNull
    @NotEmpty
    private String password;

    @NonNull
    private Long identifyCode;
}
