package com.dung.studentmanagement.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CourseTeacherRequest {
    private String email; // For delete operation
    private Set<String> emails;
}
