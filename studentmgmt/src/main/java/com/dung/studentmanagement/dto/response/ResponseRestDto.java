package com.dung.studentmanagement.dto.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseRestDto {
    private String status;
    private String message;
    private Date time;
    private Integer currentPage;
    private Long totalElements;
    private Integer totalPage;
    private Object data;
}
