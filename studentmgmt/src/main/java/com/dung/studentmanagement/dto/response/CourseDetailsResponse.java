package com.dung.studentmanagement.dto.response;

import com.dung.studentmanagement.entity.Teacher;
import com.dung.studentmanagement.enums.CourseStatus;
import com.dung.studentmanagement.enums.Period;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class CourseDetailsResponse {
    private String code;
    private String name;
    private String dateCreated;
    private String description;
    private CourseStatus status;
    private String convenor;
    private Period teachingPeriod;
    private String startDate;
    private String endDate;
}
