package com.dung.studentmanagement.dto.response;

import com.dung.studentmanagement.enums.RoleCode;
import lombok.*;

import java.util.Date;

@Getter
@Setter
public class AuthResponse {
    private String token;
    private RoleCode role;
    private Date expiration;
}
