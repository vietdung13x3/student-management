package com.dung.studentmanagement.dto.response;

import com.dung.studentmanagement.enums.RoleCode;
import com.dung.studentmanagement.enums.Status;
import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TeacherSearchResponse {
    private String id;
    private String firstname;
    private String lastname;
    private String dateOfBirth;
    private String identifyCode;
    private String email;
    @JsonEnumDefaultValue
    private Status status;
    private String dateJoined;
    private RoleCode role;
}
