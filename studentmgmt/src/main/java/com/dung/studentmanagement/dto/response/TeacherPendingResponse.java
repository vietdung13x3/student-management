package com.dung.studentmanagement.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;
import java.util.List;

@Data
@Builder
public class TeacherPendingResponse {
    Collection<String> savedIds;
    Collection<String> errorIds;
}
