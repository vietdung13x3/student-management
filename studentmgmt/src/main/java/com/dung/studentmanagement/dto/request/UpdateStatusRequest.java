package com.dung.studentmanagement.dto.request;

import com.dung.studentmanagement.enums.StudentStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateStatusRequest {
    private StudentStatus status;
}
