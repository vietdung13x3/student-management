package com.dung.studentmanagement.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class CourseConvenorRequest {
    private String teacherEmail;
}
