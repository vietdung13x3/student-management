package com.dung.studentmanagement.redis;

public interface RedisKeyConstants {
    static final String PREFIX = "student-mgmt";
    static final String DISTRICT = PREFIX + ".district";
    static final String PERIODS = PREFIX + ".teaching-periods";
}
