package com.dung.studentmanagement.redis;

import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class RedisService {
    private final RedisTemplate<String, Object> redisTemplate;

    private final static Duration TTL_DAYS = Duration.ofDays(1);

    public void save(String key, Object value) {
        redisTemplate.opsForValue().set(key, value, TTL_DAYS);
    }

    public void save(String key, Collection<Object> values) {
        redisTemplate.opsForList().leftPushAll(key, values);
    }

    public void delete(String key) {
        redisTemplate.delete(key);
    }

    public Object getFromRedis(String key) {
        return redisTemplate.opsForValue().get(key);
    }

}
