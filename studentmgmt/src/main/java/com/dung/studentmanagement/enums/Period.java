package com.dung.studentmanagement.enums;

public enum Period {
    SEMESTER1("SEMESTER1"),
    SEMESTER2("SEMESTER2"),
    WINTER("WINTER"),
    SUMMER("SUMMER");

    private final String name;
    Period(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.name;
    }
}
