package com.dung.studentmanagement.enums;

public enum CourseStatus {
    AVAILABLE("AVAILABLE"),
    STOPPED("STOPPED"),
    REMOVED("REMOVED");

    private final String code;
    CourseStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
