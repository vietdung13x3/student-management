package com.dung.studentmanagement.enums;

public enum RoleCode {
    ADMIN("ADMIN"),
    MAKER("MAKER"),
    STAFF("STAFF")
    ;

    private final String code;
    RoleCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
