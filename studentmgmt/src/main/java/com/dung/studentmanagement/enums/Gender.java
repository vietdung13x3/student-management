package com.dung.studentmanagement.enums;


public enum Gender {
    MALE("MALE"),
    FEMALE("FEMALE"),
    OTHER("OTHER");

    private final String code;
    Gender(String code) {
        this.code = code;
    }
    public String getCode() {
        return this.code;
    }
}
