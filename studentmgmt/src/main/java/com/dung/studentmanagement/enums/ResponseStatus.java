package com.dung.studentmanagement.enums;

public enum ResponseStatus {
    SUCCESS("00"),
    JWTEXPIRED("01"),
    DUPCATEDREQUEST("02"),
    DUPLICATEDTEACHER("03"),
    DATEFORMATERROR("04"),
    REQUESTNOTFOUND("05"),
    REDISVALUEERROR("06"),
    IDNOTFOUND("07"),
    EXISTEDPOSTCODE("08"),
    TEACHERNOTFOUND("09"),
    COURSEEXISTS("10"),
    DUPLICATEDSTUDENT("11"),
    WRONGFILEFORMAT("12"),
    INTERNALERROR("-99"),
    FORBIDDEN("403"),
    UNAUTHORIZED("99"),
    NOTFOUND("02"),
    EMPTY("20"),
    BADREQUEST("100");


    private final String code;

    ResponseStatus(String value) {
        this.code = value;
    }

    public String getCode() {
        return this.code;
    }
}
