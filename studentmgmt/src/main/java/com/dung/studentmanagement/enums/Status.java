package com.dung.studentmanagement.enums;

public enum Status {
    PENDING("PENDING"),
    APPROVE("APPROVED"),
    REJECT("REJECTED"),
    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE");


    private final String code;

    Status(String value) {
        this.code = value;
    }

    public String getCode() {
        return this.code;
    }
}
