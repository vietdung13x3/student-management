package com.dung.studentmanagement.enums;

public enum StudentStatus {
    ACTIVE("ACTIVE"),
    EXPIRED("EXPIRED"),
    LOCKED("LOCKED");


    private final String code;

    StudentStatus(String value) {
        this.code = value;
    }

    public String getCode() {
        return this.code;
    }
}
