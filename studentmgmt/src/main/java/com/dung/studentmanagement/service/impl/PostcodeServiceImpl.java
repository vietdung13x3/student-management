package com.dung.studentmanagement.service.impl;

import com.dung.studentmanagement.entity.Postcode;
import com.dung.studentmanagement.entity.QPostcode;
import com.dung.studentmanagement.repository.PostcodeRepository;
import com.dung.studentmanagement.service.PostcodeService;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PostcodeServiceImpl extends BaseServiceImpl<Long, Postcode, PostcodeRepository> implements PostcodeService {
    protected PostcodeServiceImpl(PostcodeRepository repository) {
        super(repository);
    }

    @Override
    public Set<Postcode> findByCodeIn(Collection<Long> postcodes) {
        BooleanExpression where = QPostcode.postcode.code.in(postcodes);
        return repository.findAll().stream().collect(Collectors.toSet());
    }
    @Override
    public Postcode findByCode(Long code) {
        return repository.findByCode(code);
    }
}
