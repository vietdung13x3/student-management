package com.dung.studentmanagement.service;

import com.dung.studentmanagement.entity.BlackListToken;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.Map;

public interface JwtService extends BaseService<Long, BlackListToken> {
    String extractUsername(String tokenKey);
    String generateToken(UserDetails userDetails);
    String generateToken(Map<String, Object> extractClaims, UserDetails userDetails);
    Boolean isTokenValid(String tokenKey, UserDetails userDetails);
    Boolean isBlackListToken(String jwtToken);
    Date extractExpiration(String jwtToken);
    void saveBlackListToken(String jwtToken);
}
