package com.dung.studentmanagement.service.impl;

import com.dung.studentmanagement.dto.request.CourseSearchRequest;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.QCourse;
import com.dung.studentmanagement.entity.TeachingPeriod;
import com.dung.studentmanagement.enums.Period;
import com.dung.studentmanagement.repository.CourseRepository;
import com.dung.studentmanagement.repository.TeachingPeriodRepository;
import com.dung.studentmanagement.service.CourseService;
import com.dung.studentmanagement.utils.CommonUtils;
import com.dung.studentmanagement.utils.DateUtils;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CourseServiceImpl extends BaseServiceImpl<Long, Course, CourseRepository> implements CourseService {
    private final TeachingPeriodRepository teachingPeriodRepository;
    protected CourseServiceImpl(CourseRepository repository, TeachingPeriodRepository teachingPeriodRepository) {
        super(repository);
        this.teachingPeriodRepository = teachingPeriodRepository;
    }

    @Override
    public boolean existsByCode(String code) {
        return repository.existsByCode(code).get();
    }

    @Override
    public Course findByCode(String code) {
        return repository.findByCode(code).orElse(null);
    }

    @Override
    public List<TeachingPeriod> getTeachingPeriods() {
        return teachingPeriodRepository.findAll();
    }


    @Override
    public Page<Course> search(CourseSearchRequest searchRequest, Pageable pageable) {
        Date from = DateUtils.getDate(searchRequest.getFrom());
        Date to = DateUtils.getDate(searchRequest.getTo());
        QCourse course = QCourse.course;
        String convenorEmail = CommonUtils.makeLikeExpression(searchRequest.getConvenorEmail());
        BooleanExpression where = course.code.likeIgnoreCase(CommonUtils.makeLikeExpression(searchRequest.getCode()))
                .and(course.name.likeIgnoreCase(CommonUtils.makeLikeExpression(searchRequest.getName())))
                .and(course.convenor.email.likeIgnoreCase(convenorEmail));

        if(from != null && to != null) {
            where = course.dateCreated.between(from, to);
        }
        if(searchRequest.getStudyPeriod() != null) {
            where = where.and(course.teachingPeriod.name.eq(searchRequest.getStudyPeriod()));
        }
        return repository.findAll(where, pageable);
//        QCourse course = QCourse.course;
//        BooleanExpression where = course.code.like(searchRequest.getCode())
//                .and(course.convenor.fi)
    }

    @Override
    public TeachingPeriod findTeachingPeriodByName(Period name) {
        return teachingPeriodRepository.findByName(name);
    }
}
