package com.dung.studentmanagement.service;

import com.dung.studentmanagement.entity.District;

import java.util.List;

public interface DistrictService extends BaseService<Long, District> {
    List<District> findAll();
    District findByName(String name);
}
