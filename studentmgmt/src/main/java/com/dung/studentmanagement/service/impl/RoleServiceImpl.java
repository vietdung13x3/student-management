package com.dung.studentmanagement.service.impl;

import com.dung.studentmanagement.entity.Role;
import com.dung.studentmanagement.enums.RoleCode;
import com.dung.studentmanagement.repository.RoleRepository;
import com.dung.studentmanagement.service.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends BaseServiceImpl<Long, Role, RoleRepository> implements RoleService {
    protected RoleServiceImpl(RoleRepository repository) {
        super(repository);
    }

    @Override
    public Role findByRoleCode(RoleCode code) {
        return repository.findByRoleCode(code);
    }
}
