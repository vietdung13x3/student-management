package com.dung.studentmanagement.service;

import com.dung.studentmanagement.dto.request.TeacherPendingSearchRequest;
import com.dung.studentmanagement.dto.request.TeacherRegisterRequest;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import com.dung.studentmanagement.enums.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;

public interface TeacherPendingService extends BaseService<Long, TeacherPendingRequest> {
    Page<TeacherPendingRequest> search(TeacherPendingSearchRequest criteria, Pageable pageable) throws ParseException;
    List<TeacherPendingRequest> updateRequestStatusByIds(Collection<Long> ids, Status status);
    TeacherPendingRequest findByEmail(String email);
    TeacherPendingRequest findByEmailOrIdentifyCode(String email, Long identifyCode);
}
