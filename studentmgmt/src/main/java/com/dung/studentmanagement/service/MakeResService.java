package com.dung.studentmanagement.service;

import com.dung.studentmanagement.dto.response.ResponseRestDto;
import org.springframework.http.ResponseEntity;

public interface MakeResService {
    ResponseEntity<ResponseRestDto> makeResSuccess(String message, Object data);
}
