package com.dung.studentmanagement.service;

import com.dung.studentmanagement.entity.Postcode;

import java.util.Collection;
import java.util.Set;

public interface PostcodeService extends BaseService<Long, Postcode> {
    Set<Postcode> findByCodeIn(Collection<Long> postcodes);
    Postcode findByCode(Long code);
}
