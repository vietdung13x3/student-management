package com.dung.studentmanagement.service;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;

public interface BaseService<I extends Serializable, T> {
    Page<T> findAll(Predicate predicate, Pageable pageable);
    Page<T> findAll(Pageable pageable);
    List<T> findAll();
    T findById(I id);
    void saveAll(Iterable<T> entities);
    void save(T entity);
}
