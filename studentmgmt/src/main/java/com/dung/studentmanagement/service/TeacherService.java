package com.dung.studentmanagement.service;

import com.dung.studentmanagement.dto.request.TeacherSearchRequest;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface TeacherService extends BaseService<Long, Teacher> {
    List<Teacher> findAllByEmailIn(Collection<String> emails);
    Teacher findByEmail(String email);
    List<Teacher> searchWithNoPageable(TeacherSearchRequest searchRequest);
    Page<Teacher> search(TeacherSearchRequest searchRequest, Pageable pageable);
    Teacher findByEmailOrIdentifyCode(String email, Long identifyCode);
    Page<Teacher> findTeacherByCourse(Course course, Pageable pageable);

}
