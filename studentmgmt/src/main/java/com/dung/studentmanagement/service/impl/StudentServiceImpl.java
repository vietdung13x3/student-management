package com.dung.studentmanagement.service.impl;

import com.dung.studentmanagement.dto.request.StudentSearchRequest;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Student;
import com.dung.studentmanagement.repository.StudentRepository;
import com.dung.studentmanagement.service.StudentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl extends BaseServiceImpl<Long, Student, StudentRepository> implements StudentService {
    protected StudentServiceImpl(StudentRepository repository) {
        super(repository);
    }

    @Override
    public boolean existsByIdentifyCode(Long code) {
        return repository.existsByIdentifyCode(code);
    }

    @Override
    public Student findByIdentifyCode(Long code) {
        return repository.findByIdentifyCode(code);
    }

    @Override
    public List<Student> findByIdIn(List<Long> ids) {
        return repository.findAllById(ids);
    }


    @Override
    public Student findByNotInCourse(Course course) {
        return repository.findByNotInCourse(course);
    }

    @Override
    public Page<Student> search(StudentSearchRequest params, Pageable pageable) {
        return repository.findByParams(params, pageable);
    }

    @Override
    public Page<Student> findByCourse(Course course, Pageable pageable) {
        return repository.findByCourse(course, pageable);
    }

    @Override
    public Student findById(Long id) {
        return repository.findByIdImpl(id);
    }
}
