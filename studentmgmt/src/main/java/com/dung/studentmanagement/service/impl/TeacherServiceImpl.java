package com.dung.studentmanagement.service.impl;

import com.dung.studentmanagement.dto.request.TeacherSearchRequest;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.QTeacher;
import com.dung.studentmanagement.entity.Teacher;
import com.dung.studentmanagement.repository.TeacherRepository;
import com.dung.studentmanagement.service.TeacherService;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class TeacherServiceImpl extends BaseServiceImpl<Long, Teacher, TeacherRepository> implements TeacherService {
    protected TeacherServiceImpl(TeacherRepository repository) {
        super(repository);
    }

    @Override
    public Teacher findByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public List<Teacher> findAllByEmailIn(Collection<String> emails) {
        return repository.findAllByEmailIn(emails);
    }

    @Override
    public List<Teacher> searchWithNoPageable(TeacherSearchRequest searchRequest) {
        return repository.searchWithNoPageable(searchRequest);
    }

    @Override
    public Page<Teacher> search(TeacherSearchRequest searchRequest, Pageable pageable) {
        return repository.search(searchRequest, pageable);
    }

    @Override
    public Teacher findByEmailOrIdentifyCode(String email, Long identifyCode) {
        return null;
    }

    @Override
    public Page<Teacher> findTeacherByCourse(Course course, Pageable pageable) {
        return repository.findAllByCourse(course, pageable);
    }
}
