package com.dung.studentmanagement.service.impl;

import com.dung.studentmanagement.dto.response.ResponseRestDto;
import com.dung.studentmanagement.service.MakeResService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class MakeResServiceImpl implements MakeResService {
    @Override
    public ResponseEntity<ResponseRestDto> makeResSuccess(String message, Object data) {
        ResponseRestDto response =  ResponseRestDto.builder().status("200").message(message).data(data).time(new Date()).build();
        return ResponseEntity.ok(response);
    }
}
