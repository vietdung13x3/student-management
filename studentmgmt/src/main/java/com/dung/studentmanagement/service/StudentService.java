package com.dung.studentmanagement.service;

import com.dung.studentmanagement.dto.request.StudentSearchRequest;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StudentService extends BaseService<Long, Student> {
    boolean existsByIdentifyCode(Long code);
    Student findByIdentifyCode(Long code);
    List<Student> findByIdIn(List<Long> ids);
    Student findByNotInCourse(Course course);
    Page<Student> search(StudentSearchRequest params, Pageable pageable);

    Page<Student> findByCourse(Course course, Pageable pageable);
}
