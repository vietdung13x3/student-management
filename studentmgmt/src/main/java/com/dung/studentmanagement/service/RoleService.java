package com.dung.studentmanagement.service;

import com.dung.studentmanagement.entity.Role;
import com.dung.studentmanagement.enums.RoleCode;

public interface RoleService extends BaseService<Long, Role> {
    Role findByRoleCode(RoleCode code);
}
