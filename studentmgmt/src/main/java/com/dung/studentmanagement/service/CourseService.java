package com.dung.studentmanagement.service;

import com.dung.studentmanagement.dto.request.CourseSearchRequest;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.TeachingPeriod;
import com.dung.studentmanagement.enums.Period;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CourseService extends BaseService<Long, Course> {
    boolean existsByCode(String code);

    Course findByCode(String code);

    List<TeachingPeriod> getTeachingPeriods();

    Page<Course> search(CourseSearchRequest searchRequest, Pageable pageable);
    TeachingPeriod findTeachingPeriodByName(Period name);
}
