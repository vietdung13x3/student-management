package com.dung.studentmanagement.service.impl;

import com.dung.studentmanagement.dto.request.TeacherPendingSearchRequest;
import com.dung.studentmanagement.entity.QTeacherPendingRequest;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.enums.Status;
import com.dung.studentmanagement.exception.BusinessException;
import com.dung.studentmanagement.repository.TeacherPendingRequestRepository;
import com.dung.studentmanagement.service.TeacherPendingService;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherPendingServiceImpl extends BaseServiceImpl<Long, TeacherPendingRequest, TeacherPendingRequestRepository> implements TeacherPendingService {
    protected TeacherPendingServiceImpl(TeacherPendingRequestRepository repository) {
        super(repository);
    }

//    @Override
//    public void saveAll(List<TeacherPendingRequest> teacherRegisterRequests) {
//        repository.saveAll(teacherRegisterRequests);
//    }
    @Override
    public Page<TeacherPendingRequest> search(TeacherPendingSearchRequest criteria, Pageable pageable) throws ParseException {
        return repository.search(criteria, pageable);
    }

    @Override
    public List<TeacherPendingRequest> updateRequestStatusByIds(Collection<Long> ids, Status status) {
        // Find all by id in and status is PENDING
        List<TeacherPendingRequest> entities = repository.findAllByIdInAndStatus(ids, Status.PENDING);
        for(TeacherPendingRequest entity : entities) {
            entity.setStatus(status);

        }
        repository.saveAll(entities);
//        repository.saveAll(entities);
        return entities;
    }

    @Override
    public TeacherPendingRequest findByEmail(String email) {
        QTeacherPendingRequest teacher = QTeacherPendingRequest.teacherPendingRequest;
        BooleanExpression where = teacher.email.eq(email);
        return repository.findOne(where);
    }

    @Override
    public TeacherPendingRequest findByEmailOrIdentifyCode(String email, Long identifyCode) {
        return repository.findByEmailOOrIdentifyCode(email, identifyCode);
    }
}
