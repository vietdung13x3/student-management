package com.dung.studentmanagement.service.impl;

import com.dung.studentmanagement.entity.District;
import com.dung.studentmanagement.repository.DistrictRepository;
import com.dung.studentmanagement.service.DistrictService;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistrictServiceImpl extends BaseServiceImpl<Long, District, DistrictRepository> implements DistrictService {
    protected DistrictServiceImpl(DistrictRepository repository) {
        super(repository);
    }

    @Override
    public List<District> findAll() {
        return repository.findAll();
    }

    @Override
    public District findByName(String name) {
        return repository.findByName(name);
    }
}
