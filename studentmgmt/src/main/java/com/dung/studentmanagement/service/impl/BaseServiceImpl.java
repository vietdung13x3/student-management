package com.dung.studentmanagement.service.impl;

import com.dung.studentmanagement.repository.BaseRepository;
import com.dung.studentmanagement.service.BaseService;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public abstract class BaseServiceImpl<I extends Serializable, T, R extends BaseRepository<I, T>> implements BaseService<I, T> {

    protected final R repository;

    protected BaseServiceImpl(R repository) {
        this.repository = repository;
    }

    @Override
    public Page<T> findAll(Predicate predicate, Pageable pageable) {
        return repository.findAll(predicate, pageable);
    }

    @Override
    public T findById(I id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<T> findAll(Pageable pageable){
        return repository.findAll(pageable);
    }

    @Override
    public void saveAll(Iterable<T> entities) {
        repository.saveAll(entities);
    }

    @Override
    public void save(T entity) {
        repository.save(entity);
    }
}
