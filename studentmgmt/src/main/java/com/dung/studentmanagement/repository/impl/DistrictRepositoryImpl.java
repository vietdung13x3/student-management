package com.dung.studentmanagement.repository.impl;

import com.dung.studentmanagement.entity.District;
import com.dung.studentmanagement.entity.QDistrict;
import com.dung.studentmanagement.entity.QPostcode;
import com.dung.studentmanagement.repository.DistrictRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DistrictRepositoryImpl extends BaseRepositoryImpl<Long, District> implements DistrictRepository {
    protected DistrictRepositoryImpl(EntityManager em) {
        super(District.class, em);
    }
    private static final QDistrict DISTRICT = QDistrict.district;

    @Override
    public List<District> findAll() {
//        List<District> districts = new ArrayList<>();
//        districts.add(queryFactory.selectFrom(DISTRICT).leftJoin(DISTRICT.postcodes).fetchOne()) ;
        List<District> fetch = queryFactory.selectFrom(DISTRICT).distinct()
                .leftJoin(DISTRICT.postcodes, QPostcode.postcode)
                .fetchJoin().fetch();
        return fetch;
    }

    @Override
    public District findByName(String name) {
        BooleanExpression where = QDistrict.district.name.eq(name);
        return queryFactory.selectFrom(DISTRICT).where(where).leftJoin(DISTRICT.postcodes, QPostcode.postcode).fetchJoin().fetchOne();
    }
}
