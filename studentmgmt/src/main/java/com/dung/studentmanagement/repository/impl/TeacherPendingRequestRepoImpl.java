package com.dung.studentmanagement.repository.impl;

import com.dung.studentmanagement.dto.request.TeacherPendingSearchRequest;
import com.dung.studentmanagement.dto.request.TeacherRegisterRequest;
import com.dung.studentmanagement.entity.QTeacherPendingRequest;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import com.dung.studentmanagement.enums.ResponseStatus;
import com.dung.studentmanagement.enums.Status;
import com.dung.studentmanagement.exception.BusinessException;
import com.dung.studentmanagement.repository.TeacherPendingRequestRepository;
import com.dung.studentmanagement.utils.CommonUtils;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
public class TeacherPendingRequestRepoImpl extends BaseRepositoryImpl<Long, TeacherPendingRequest> implements TeacherPendingRequestRepository {
    protected TeacherPendingRequestRepoImpl(EntityManager em) {
        super(TeacherPendingRequest.class, em);
    }

    @Override
    public Page<TeacherPendingRequest> search(TeacherPendingSearchRequest criteria, Pageable pageable) throws ParseException {
        String firstname = CommonUtils.makeLikeExpression(criteria.getFirstname());
        String lastname = CommonUtils.makeLikeExpression(criteria.getLastname());
        String email = CommonUtils.makeLikeExpression(criteria.getEmail());
        String id = CommonUtils.makeLikeExpression(criteria.getId());
        String identifyCode = CommonUtils.makeLikeExpression(criteria.getIdentifyCode());
        QTeacherPendingRequest teacherRequest = QTeacherPendingRequest.teacherPendingRequest;
        BooleanExpression where = teacherRequest.firstname.likeIgnoreCase(firstname)
                .and(teacherRequest.lastname.likeIgnoreCase(lastname))
                .and(teacherRequest.email.likeIgnoreCase(email))
                .and(teacherRequest.id.like(id))
                .and(teacherRequest.identifyCode.like(identifyCode))
                .and(teacherRequest.status.eq(Status.PENDING));
        if (criteria.getDateOfBirth() != null && criteria.getDateOfBirth().isEmpty() == false) {
            Date dateOfBirth = new SimpleDateFormat("dd/MM/yyyy").parse(criteria.getDateOfBirth());
            where = where.and(teacherRequest.dateOfBirth.eq(dateOfBirth));
        }
        if (criteria.getFromDate() != null && !criteria.getFromDate().isEmpty() && criteria.getToDate() != null && !criteria.getToDate().isEmpty()) {
            Date from = new SimpleDateFormat("dd/MM/yyyy").parse(criteria.getFromDate());
            Date to = new SimpleDateFormat("dd/MM/yyyy").parse(criteria.getToDate());
            where = where.and(teacherRequest.requestedDate.between(from, to));
        }
        return findAll(where, pageable);
    }

    @Override
    public List<TeacherPendingRequest> findAllByIdInAndStatus(Collection<Long> ids, Status status) {
        QTeacherPendingRequest teacherPendingRequest = QTeacherPendingRequest.teacherPendingRequest;
        BooleanExpression where = teacherPendingRequest.id.in(ids).and(teacherPendingRequest.status.eq(status));
        return findAll(where);
    }

    @Override
    public TeacherPendingRequest findByEmailOOrIdentifyCode(String email, Long identifyCode) {
        QTeacherPendingRequest teacherPendingRequest = QTeacherPendingRequest.teacherPendingRequest;
        BooleanExpression where = teacherPendingRequest.email.eq(email).or(teacherPendingRequest.identifyCode.eq(identifyCode));
        return findOne(where);
    }
}
