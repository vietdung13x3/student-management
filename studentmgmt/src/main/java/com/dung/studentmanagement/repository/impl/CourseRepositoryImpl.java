package com.dung.studentmanagement.repository.impl;

import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.QCourse;
import com.dung.studentmanagement.repository.CourseRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Repository
@Slf4j

public class CourseRepositoryImpl extends BaseRepositoryImpl<Long, Course> implements CourseRepository {
    protected CourseRepositoryImpl(EntityManager em) {
        super(Course.class, em);
    }

    @Override
    public Optional<Boolean> existsByCode(String code) {
        BooleanExpression where = QCourse.course.code.eq(code);
        List<Course> courses = findAll(where);
        return Optional.of(Boolean.valueOf(courses.size() != 0));
    }

    @Override
    public Optional<Course> findByCode(String code) {
        BooleanExpression where = QCourse.course.code.eq(code);
        Course course = queryFactory.selectFrom(QCourse.course).distinct().
                leftJoin(QCourse.course.subTeachers)
                .leftJoin(QCourse.course.enrolledStudents)
                .where(where).fetchJoin().fetchOne();
        return Optional.ofNullable(course);
    }
}
