package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.entity.TeachingPeriod;
import com.dung.studentmanagement.enums.Period;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeachingPeriodRepository extends JpaRepository<TeachingPeriod, Long> {
    List<TeachingPeriod> findAll();
    TeachingPeriod findByName(Period name);
}
