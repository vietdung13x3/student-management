package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.dto.request.TeacherSearchRequest;
import com.dung.studentmanagement.dto.request.UpdateTeacherRequest;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Role;
import com.dung.studentmanagement.entity.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;
import java.util.List;

@NoRepositoryBean
public interface TeacherRepository extends BaseRepository<Long, Teacher>{
    Teacher findByEmail(String email);
    Page<Teacher> findAllByCourse(Course course, Pageable pageable);
    List<Teacher> findAllByEmailIn(Collection<String> emails);
    Teacher findByIdentifyCode(Long identifyCode);
    Page<Teacher> search(TeacherSearchRequest searchRequest, Pageable pageable);
    List<Teacher> searchWithNoPageable(TeacherSearchRequest searchRequest);
    Teacher updateRoleByEmail(String email, Role role);
}
