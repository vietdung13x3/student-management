package com.dung.studentmanagement.repository.impl;

import com.dung.studentmanagement.dto.request.TeacherSearchRequest;
import com.dung.studentmanagement.dto.request.UpdateTeacherRequest;
import com.dung.studentmanagement.entity.*;
import com.dung.studentmanagement.repository.TeacherRepository;
import com.dung.studentmanagement.utils.CommonUtils;
import com.dung.studentmanagement.utils.DateUtils;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
public class TeacherRepositoryImpl extends BaseRepositoryImpl<Long, Teacher> implements TeacherRepository {

    private static final QTeacher teacher = QTeacher.teacher;

    @Autowired
    protected TeacherRepositoryImpl(EntityManager em) {
        super(Teacher.class, em);
    }

    @Override
    public Teacher findByEmail(String email) {
        BooleanExpression where = teacher.email.eq(email);
        return queryFactory.selectFrom(teacher).leftJoin(teacher.role).fetchJoin().where(where).fetchOne();
    }

    @Override
    public Page<Teacher> findAllByCourse(Course course, Pageable pageable) {
        BooleanExpression booleanExpression = teacher.courses.contains(course);
        return findAll(booleanExpression, pageable);
    }

    @Override
    public List<Teacher> findAllByEmailIn(Collection<String> emails) {
        BooleanExpression where = teacher.email.in(emails);
        return findAll(where);
    }

    @Override
    public Teacher findByIdentifyCode(Long identifyCode) {
        BooleanExpression where = teacher.identifyCode.eq(identifyCode);
        return findOne(where);
    }

    private BooleanExpression getSearchExpression(TeacherSearchRequest searchRequest) {
        String firstname = CommonUtils.makeLikeExpression(searchRequest.getFirstname());
        String lastname = CommonUtils.makeLikeExpression(searchRequest.getLastname());
        String email = CommonUtils.makeLikeExpression(searchRequest.getEmail());
        String identifyCode = CommonUtils.makeLikeExpression(searchRequest.getIdentifyCode());
        String dateOfBirth = searchRequest.getDateOfBirth();

        BooleanExpression where = teacher.firstname.likeIgnoreCase(firstname)
                .and(teacher.lastname.likeIgnoreCase(lastname)
                        .and(teacher.email.likeIgnoreCase(email)
                                .and(teacher.identifyCode.like(identifyCode))));
        if(StringUtils.isNotBlank(searchRequest.getDateOfBirth())) {
            where = where.and(teacher.dateOfBirth.eq(DateUtils.getDate(dateOfBirth)));
        }
        if(searchRequest.getStatus() != null) {
            where = where.and(teacher.status.eq(searchRequest.getStatus()));
        }
        if(StringUtils.isNotBlank(searchRequest.getFromDate()) && StringUtils.isNotBlank((searchRequest.getToDate()))){
            Date from = DateUtils.getDate(searchRequest.getFromDate());
            Date to = DateUtils.getDate(searchRequest.getToDate());
            where = where.and(teacher.dateJoined.between(from, to));
        }
        return where;
    }
    @Override
    public Page<Teacher> search(TeacherSearchRequest searchRequest, Pageable pageable) {
        BooleanExpression where = getSearchExpression(searchRequest);
        JPAQuery<Teacher> teacherJPAQuery = queryFactory.selectFrom(teacher).where(where).leftJoin(teacher.role).fetchJoin().fetchAll();
        return findAll(teacherJPAQuery, pageable);
    }

    @Override
    public List<Teacher> searchWithNoPageable(TeacherSearchRequest searchRequest) {
        BooleanExpression where = getSearchExpression(searchRequest);
        return queryFactory.selectFrom(teacher).where(where).leftJoin(teacher.role).fetchJoin().fetchAll().fetch();
    }

    @Override
    public Teacher updateRoleByEmail(String email, Role role) {
        return null;
    }
}
