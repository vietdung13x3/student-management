package com.dung.studentmanagement.repository.impl;

import com.dung.studentmanagement.entity.Postcode;
import com.dung.studentmanagement.entity.QPostcode;
import com.dung.studentmanagement.repository.PostcodeRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class PostcodeRepositoryImpl extends BaseRepositoryImpl<Long, Postcode> implements PostcodeRepository {
    protected PostcodeRepositoryImpl(EntityManager em) {
        super(Postcode.class, em);
    }

    @Override
    public Postcode findByCode(Long code) {
        BooleanExpression where = QPostcode.postcode.code.eq(code);
        return queryFactory.selectFrom(QPostcode.postcode).leftJoin(QPostcode.postcode.district).where(where).fetchJoin().fetchOne();
    }
}
