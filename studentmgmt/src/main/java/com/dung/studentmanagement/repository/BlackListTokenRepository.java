package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.entity.BlackListToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BlackListTokenRepository extends BaseRepository<Long, BlackListToken> {
    BlackListToken findByJwtToken(String jwtToken);
}
