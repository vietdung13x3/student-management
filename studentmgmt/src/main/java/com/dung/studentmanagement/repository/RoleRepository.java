package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.entity.Role;
import com.dung.studentmanagement.enums.RoleCode;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface RoleRepository extends BaseRepository<Long, Role> {
    Role findByRoleCode(RoleCode code);
}
