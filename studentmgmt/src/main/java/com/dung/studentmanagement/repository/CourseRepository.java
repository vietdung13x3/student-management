package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Teacher;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface CourseRepository extends BaseRepository<Long, Course> {
    Optional<Boolean> existsByCode(String code);
    Optional<Course> findByCode(String code);
}
