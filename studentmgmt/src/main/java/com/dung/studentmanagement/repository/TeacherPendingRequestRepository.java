package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.dto.request.TeacherPendingSearchRequest;

import com.dung.studentmanagement.dto.request.TeacherRegisterRequest;
import com.dung.studentmanagement.entity.TeacherPendingRequest;
import com.dung.studentmanagement.enums.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;

@NoRepositoryBean
public interface TeacherPendingRequestRepository extends BaseRepository<Long, TeacherPendingRequest> {
    Page<TeacherPendingRequest> search(TeacherPendingSearchRequest criteria, Pageable pageable) throws ParseException;
    List<TeacherPendingRequest> findAllByIdInAndStatus(Collection<Long> ids, Status status);
    TeacherPendingRequest findByEmailOOrIdentifyCode(String email, Long identifyCode);
}
