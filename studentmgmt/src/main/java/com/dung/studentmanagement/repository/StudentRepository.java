package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.dto.request.StudentSearchRequest;
import com.dung.studentmanagement.entity.Course;
import com.dung.studentmanagement.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface StudentRepository extends BaseRepository<Long, Student> {
    boolean existsByIdentifyCode(Long code);
    Page<Student> findByParams(StudentSearchRequest params, Pageable pageable);

    Student findByIdentifyCode(Long code);

    Page<Student> findByCourse(Course course, Pageable pageable);

    Student findByIdImpl(Long id);

    Student findByNotInCourse(Course course);

    List<Student> findByIdentifyCodeIn(List<Long> codes);
}
