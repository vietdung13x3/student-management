package com.dung.studentmanagement.repository.impl;

import com.dung.studentmanagement.entity.BlackListToken;
import com.dung.studentmanagement.entity.QBlackListToken;
import com.dung.studentmanagement.repository.BlackListTokenRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class BlackListTokenRepositoryImpl extends BaseRepositoryImpl<Long, BlackListToken> implements BlackListTokenRepository {
    protected BlackListTokenRepositoryImpl(EntityManager em) {
        super(BlackListToken.class, em);
    }

    @Override
    public BlackListToken findByJwtToken(String jwtToken) {
        BooleanExpression where = QBlackListToken.blackListToken.jwtToken.eq(jwtToken);
        return findOne(where);
    }
}
