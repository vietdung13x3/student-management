package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.entity.Postcode;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PostcodeRepository extends BaseRepository<Long, Postcode> {
    Postcode findByCode(Long code);
}
