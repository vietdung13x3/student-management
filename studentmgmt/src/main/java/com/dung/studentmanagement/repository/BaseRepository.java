package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.entity.Teacher;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@NoRepositoryBean
public interface BaseRepository<I extends Serializable, T> extends JpaRepositoryImplementation<T, I> {
    Page<T> findAll(JPAQuery<T> query, Pageable pageable);
    Page<T> findAll(Predicate predicate, Pageable pageable);
    List<T> findAll(Predicate predicate);
    T findOne(Predicate predicate);
}
