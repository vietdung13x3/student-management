package com.dung.studentmanagement.repository;

import com.dung.studentmanagement.entity.District;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface DistrictRepository extends BaseRepository<Long, District> {
    List<District> findAll();

    District findByName(String name);
}
