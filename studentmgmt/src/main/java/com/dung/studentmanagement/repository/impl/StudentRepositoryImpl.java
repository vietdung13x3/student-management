package com.dung.studentmanagement.repository.impl;

import com.dung.studentmanagement.dto.request.StudentSearchRequest;
import com.dung.studentmanagement.entity.*;
import com.dung.studentmanagement.enums.StudentStatus;
import com.dung.studentmanagement.repository.StudentRepository;
import com.dung.studentmanagement.utils.CommonUtils;
import com.dung.studentmanagement.utils.DateUtils;
import com.querydsl.core.types.ArrayConstructorExpression;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.ListExpression;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.*;

@Repository
public class StudentRepositoryImpl extends BaseRepositoryImpl<Long, Student> implements StudentRepository {
    protected StudentRepositoryImpl(EntityManager em) {
        super(Student.class, em);
    }

    @Override
    public boolean existsByIdentifyCode(Long code) {
        BooleanExpression where = QStudent.student.identifyCode.eq(code);
        return predicateExecutor.exists(where);
    }

    @Override
    public Page<Student> findByParams(StudentSearchRequest params, Pageable pageable) {
        String firstname = CommonUtils.makeLikeExpression(params.getFirstname());
        String lastname = CommonUtils.makeLikeExpression(params.getLastname());
        String identifyCode = CommonUtils.makeLikeExpression(params.getIdentifyCode());
        String district = CommonUtils.makeLikeExpression(params.getDistrict());

        QStudent student = QStudent.student;
        BooleanExpression where = student.firstname.likeIgnoreCase(firstname)
                .and(student.lastname.likeIgnoreCase(lastname))
                .and(student.identifyCode.like(identifyCode))
                .and(student.district.name.likeIgnoreCase(district));
        if(params.getGender() != null) {
            where = where.and(student.gender.eq(params.getGender()));
        }
        if(params.getJoinedFrom() != null && params.getJoinedTo() != null) {
            Date joinedFrom = DateUtils.getDate(params.getJoinedFrom());
            Date joinedTo = DateUtils.getDate(params.getJoinedTo());
            where = where.and(student.dateJoined.between(joinedFrom, joinedTo));
        }
        if(params.getExpiredFrom() != null && params.getExpiredTo() != null) {
            Date expiredFrom = DateUtils.getDate(params.getExpiredFrom());
            Date expiredTo = DateUtils.getDate(params.getExpiredTo());
            where = where.and(student.dateExpired.between(expiredFrom, expiredTo));
        }
        JPAQuery<Student> studentJPAQuery = queryFactory.selectFrom(student).distinct().orderBy(student.status.asc()).leftJoin(student.district).where(where).fetchJoin();
        return findAll(studentJPAQuery, pageable);
    }

    @Override
    public Student findByIdentifyCode(Long code) {
        QStudent student = QStudent.student;
        BooleanExpression where = student.identifyCode.eq(code);
        return queryFactory.selectFrom(student).distinct().leftJoin(student.courses).where(where).fetchJoin().fetchOne();
    }

    @Override
    public List<Student> findAllById(Iterable<Long> ids) {
        List<Long> idList = new ArrayList<>();
        for(Long id: ids) {
            idList.add(id);
        }
        QStudent student = QStudent.student;
        BooleanExpression where = student.status.notIn(StudentStatus.EXPIRED, StudentStatus.LOCKED)
                .and(student.id.in(idList));
        return findAll(where);
    }

    @Override
    public Page<Student> findByCourse(Course course, Pageable pageable) {
        QStudent student = QStudent.student;
        BooleanExpression where = student.courses.contains(course);
        return findAll(where, pageable);
    }

    @Override
    public Student findByIdImpl(Long id) {
        QStudent student = QStudent.student;
        BooleanExpression where = student.id.eq(id);
        return queryFactory.selectFrom(student).distinct().leftJoin(student.courses).where(where).fetchJoin().fetchOne();
    }

    @Override
    public Student findByNotInCourse(Course course) {
        QStudent student = QStudent.student;
//        BooleanExpression where = student.courses.contains(course);
        return null;
    }

    @Override
    public List<Student> findByIdentifyCodeIn(List<Long> codes) {
        QStudent student = QStudent.student;
        BooleanExpression where = student.identifyCode.in(codes).and(student.status.notIn(StudentStatus.EXPIRED, StudentStatus.LOCKED));
        return findAll(where);
    }
}
