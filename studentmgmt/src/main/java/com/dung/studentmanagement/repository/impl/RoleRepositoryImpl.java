package com.dung.studentmanagement.repository.impl;

import com.dung.studentmanagement.entity.QRole;
import com.dung.studentmanagement.entity.Role;
import com.dung.studentmanagement.enums.RoleCode;
import com.dung.studentmanagement.repository.RoleRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class RoleRepositoryImpl extends BaseRepositoryImpl<Long, Role> implements RoleRepository  {
    protected RoleRepositoryImpl(EntityManager em) {
        super(Role.class, em);
    }

    @Override
    public Role findByRoleCode(RoleCode code) {
        BooleanExpression where = QRole.role.roleCode.eq(code);
        return findOne(where);
    }
}
