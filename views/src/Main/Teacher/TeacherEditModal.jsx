import { Button, Col, DatePicker, Divider, Form, Input, Modal, Row, Select } from "antd";
import moment from "moment";
import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import axios from "axios";
import { useState } from "react";
dayjs.extend(customParseFormat)

const SERVER_HOST = process.env.REACT_APP_SERVER_URL;
const ROLE = {
    ADMIN: 'ADMIN',
    MAKER: 'MAKER',
    STAFF: 'STAFF'
}

export default function TeacherEditModal({ teacherDetails, isOpen, setIsOpen, notification, setIsUpdated }) {
    const [form] = Form.useForm();
    const [loading, setLoading] =  useState(false);
    const sendUpdate = async ( detail ) => {
        setLoading(true);
        await axios({
            url: `${SERVER_HOST}/api/v1.0/teacher/update`,
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                "email": detail.email,
                "firstname": detail.firstname,
                "lastname" : detail.lastname,
                "role": detail.role,
                "status" : detail.status,
                "identifyCode": detail.identifyCode,
                "dateOfBirth": moment(detail.dateOfBirth).format('DD/MM/YYYY')
            }
        }).then(res => {
            if(res.data.status === "00") {
                notification.success({
                    message: 'Success',
                    description: res.data.message
                });
                setIsUpdated(true);
                setIsOpen(false);
            }else {
                notification.error({
                    message: 'Error',
                    description: res.data.message
                });
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: er
            });
        })
        setLoading(false);
    }
    const cancel = () => {
        form.resetFields();
        setIsOpen(false);
    }
    return (
        <Modal footer={null} width={'70em'} onCancel={cancel} open={isOpen}>
            <Divider>Editing teacher's details</Divider>
            <Form onFinish={sendUpdate} form={form} layout="vertical">
                <Row gutter={20} justify='center'>
                    <Col span={10}>
                        <Form.Item label="Email" name={'email'} initialValue={teacherDetails.email}>
                            <Input disabled={true} />
                        </Form.Item>
                    </Col>
                    <Col span={10}>
                        <Form.Item label="Identify Code" name={'identifyCode'}>
                            <Input disabled />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={20} justify='center'>
                    <Col span={10}>
                        <Form.Item label="Status" initialValue={teacherDetails.status} name={'status'}>
                            <Select>
                                <Select.Option value={'ACTIVE'}>Active</Select.Option>
                                <Select.Option value={'INACTIVE'}>Inactive</Select.Option>
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={10}>

                        <Form.Item label="Role" initialValue={teacherDetails.role} name={'role'}>
                            <Select>
                                <Select.Option value={ROLE.ADMIN} >Admin</Select.Option>
                                <Select.Option value={ROLE.MAKER} >Maker</Select.Option>
                                <Select.Option value={ROLE.STAFF} >Staff</Select.Option>
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={20} justify='center'>
                    <Col span={10}>
                        <Form.Item initialValue={teacherDetails.firstname} label="Firstname" name={'firstname'}>
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={10}>
                        <Form.Item label="Lastname" name={'lastname'} initialValue={teacherDetails.lastname}>
                            <Input />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={20} justify='center'>
                    <Col span={10}>
                        <Form.Item label="Identify Code" initialValue={teacherDetails.identifyCode} name={'identifyCode'}>
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={10}>

                        <Form.Item label="Date Of Birth" initialValue={dayjs(teacherDetails.dateOfBirth)} name={'dateOfBirth'}>
                            <DatePicker className="w-full" format={'DD/MM/YYYY'} />
                        </Form.Item>
                    </Col>
                </Row>

                <Row gutter={10} justify={'center'}>
                    <Col >
                        <Form.Item>
                            <Button loading={loading} onClick={cancel}>Cancel</Button>
                        </Form.Item>
                    </Col>
                    <Col>
                        <Form.Item>
                            <Button loading={loading} type="primary" htmlType="submit">Update</Button>
                        </Form.Item>
                    </Col>
                </Row>

            </Form>
        </Modal>
    )
}