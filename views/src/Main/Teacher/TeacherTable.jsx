import { Popconfirm, Select, Table, Tag, Typography } from "antd"
import { Option } from "antd/es/mentions"
import useNotification from "antd/es/notification/useNotification"
import axios from "axios"
import { useEffect, useState } from "react"
import TeacherEditModal from "./TeacherEditModal"
import { SERVER_URL } from "../../Common/Constant"
const STATUS = {
    ACTIVE: 'ACTIVE',
    INACTIVE: 'INACTIVE'
}
const dummyData = [
    {
        key: '1',
        firstname: 'Dung',
        lastname: 'Tran',
        identifyCode: '1234555',
        dateOfBirth: '13/03/2003',
        email: 'asdasd',
        requestedDate: 'asdasd',
        role: 'ADMIN',
        dateJoined: '13/03/2003',
        status: 'INACTIVE'
    }
]
export default function TeacherTable({ searchParams, isSearched, setIsSearched }) {
    const [loading, setLoading] = useState(false);
    const [ isOpenEdit , setIsOpenEdit ] = useState(false);
    const [data, setData] = useState([]);
    const [isUpdated, setIsUpdated] = useState(false);
    const [page, setPage] = useState({ size: 10, currentPage: 0 });
    const [totalElements, setTotalElements] = useState(0);
    const [notification, contextHolder] = useNotification();
    const [chosenTeacher, setChosenTeacher] = useState({ firstname: '', lastname: '', identifyCode: '', dateOfBirth: '', email: '', dateJoined: '', status: '', role: ''});
    function onChangePage(pageNum, pageSize) {
        setPage({...page, currentPage: pageNum -1});
    }
    const fetchData = async () => {
        setData([])
        setLoading(true);
        const params = { ...searchParams ,page: page.currentPage, size: page.size};
        await axios({
            url: `${SERVER_URL}/api/v1.0/teacher/search`,
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: params
        }).then(res => {
            setTotalElements(res.data.totalElements);
            res.data.data.map((teacher) => {
                setData( data => [...data, {
                    firstname: teacher.firstname,
                    lastname: teacher.lastname,
                    identifyCode: teacher.identifyCode,
                    key: teacher.id,
                    dateOfBirth: teacher.dateOfBirth,
                    email: teacher.email,
                    dateJoined: teacher.dateJoined,
                    status: teacher.status,
                    role: teacher.role
                }])
            })
        })
        setLoading(false);
    }
    useEffect(()=> {
        if (isSearched) {
            setPage({ size: 10, currentPage: 0 })
            // fetchData();
            setIsSearched(false);
        }
        if(isUpdated === true) {
            fetchData();
            setIsUpdated(false);
        }
    }, [isSearched, isUpdated])
    useEffect(()=> {
        fetchData();
    }, [page])
    const columns = [
        {
            key: 'firstname',
            title: 'Firstname',
            dataIndex: 'firstname'
        },
        {
            key: 'lastname',
            title: 'Lastname',
            dataIndex: 'lastname'
        },
        {
            key: 'identifyCode',
            title: 'Identify Code',
            dataIndex: 'identifyCode'
        },
        {
            key: 'dateOfBirth',
            title: 'Date Of Birth',
            dataIndex: 'dateOfBirth'
        },
        {
            key: 'email',
            title: 'Email',
            dataIndex: 'email'
        },
        {
            key: 'dateJoined',
            title: 'Date Joined',
            dataIndex: 'dateJoined'
        },
        {
            key: 'role',
            title: 'Role',
            dataIndex: 'role',
            width: '40px'
        },
        {
            key: 'status',
            title: 'Status',
            dataIndex: 'status',
            render: (_, {status}) => {
                if (status == STATUS.ACTIVE) {
                    return (
                        <Tag color={'green'}>{status}</Tag>
                    )
                }
                return (
                    <Tag color='red'>{status}</Tag>
                )
            }
        },
        {
            key: 'operation',
            title: 'Operation',
            dataIndex: 'edit',
            render: (_, teacher)=> {
                return (
                    <Typography.Link onClick={() => {
                        setIsOpenEdit(true);
                        setChosenTeacher(teacher);
                    }}>Edit</Typography.Link>
                )
            }
        }
    ]
    return (
        <>
            {contextHolder}
            <TeacherEditModal setIsUpdated={setIsUpdated} notification={notification} teacherDetails={chosenTeacher} isOpen={isOpenEdit} setIsOpen={setIsOpenEdit}/>
            <Table loading={loading} pagination={{current: page.currentPage + 1, size: page.size, total: totalElements, onChange: onChangePage}} columns={columns} dataSource={data} />
        </>
    )
}