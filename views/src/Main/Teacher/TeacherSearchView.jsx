import { DownloadOutlined, SearchOutlined } from "@ant-design/icons";
import { Button, Col, DatePicker, Divider, Form, Input, Row, Select, Space, ConfigProvider } from "antd";
import useNotification from "antd/es/notification/useNotification";
import axios from "axios";
import moment from "moment";
import { useState } from "react";
import ThemeContent from "../ThemeContent";
import TeacherTable from "./TeacherTable";
import { SERVER_URL } from "../../Common/Constant";
const { RangePicker } = DatePicker;
const ROLE = {
    ADMIN: 'ADMIN',
    STAFF: 'STAFF',
    MAKER: 'MAKER'
}
const STATUS = {
    ACTIVE: 'ACTIVE',
    INACTIVE: 'INACTIVE'
}
const ROLE_OPTION = [
    {
        value: ROLE.ADMIN,
        label: 'Admin'
    },
    {
        value: ROLE.STAFF,
        label: 'Staff'
    },
    {
        value: ROLE.MAKER,
        label: 'Maker'
    }
];
export default function TeacherSearchView() {
    const [searchParams, setSearchParams] = useState({ firstname: '', lastname: '', identifyCode: '', dateOfBirth: '', email: '', status: '', role: '', fromDate: '', toDate: '' });
    const [isSubmit, setIsSubmit] = useState(false);
    const [notification, contextHolder] = useNotification();
    const onSearch = (params) => {
        setIsSubmit(true);
        setSearchParams({
            firstname: params.firstname === undefined ? '' : params.firstname,
            lastname: params.lastname === undefined ? '' : params.lastname,
            identifyCode: params.identifyCode === undefined ? '' : params.identifyCode,
            dateOfBirth: params.dateOfBirth === undefined ? '' : moment(params.dateOfBirth).format('DD/MM/YYYY'),
            email: params.email === undefined ? '' : params.email,
            status: params.status === undefined ? '' : params.status,
            role: params.role === undefined ? '' : params.role,
            fromDate: params.dateJoined === undefined ? '' : moment(params.dateJoined[0].$d).format('DD/MM/YYYY'),
            toDate: params.dateJoined === undefined ? '' : moment(params.dateJoined[1].$d).format('DD/MM/YYYY'),
        })
    }
    const exportExcel = async () => {
        await axios({
            url: `${SERVER_URL}/api/v1.0/teacher/search/export`,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: searchParams,
            responseType: 'blob'
        }).then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'report-teacher.xlsx');
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: er.message
            })
        })
    }
    return (
        <ThemeContent>
            {contextHolder}
            <Form layout="vertical" onFinish={onSearch}>
                <Row gutter={20}>
                    <Col span={8}>
                        <Form.Item label="Firstname" name={'firstname'}>
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label="Lastname" name={'lastname'}>
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label="Identify Code" name={'identifyCode'}>
                            <Input />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={20}>
                    <Col span={8}>
                        <Form.Item label="Date Of Birth" name={'dateOfBirth'}>
                            <Input type="date" />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label="Email" name={'email'}>
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label="Status" name={'status'}>
                            <Select className="text-left" dropdownStyle={{ textAlign: 'left' }}>
                                <Select.Option value={STATUS.ACTIVE}>Active</Select.Option>
                                <Select.Option value={STATUS.INACTIVE}>Inactive</Select.Option>
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={20}>
                    <Col span={8}>
                        <Form.Item label="Role" name={'role'}>
                            <Select className="text-left" options={ROLE_OPTION} dropdownStyle={{ textAlign: 'left' }} />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label="Date Joined" name={'dateJoined'}>
                            <RangePicker className="w-full" format={'DD/MM/YYYY'} />
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item>
                    <Space>
                        <Button type="primary" htmlType="submit" icon={<SearchOutlined />}>Search</Button>
                        <ConfigProvider
                            theme={{
                                token: {
                                    colorPrimary: '#00b96b',
                                },
                            }}
                        >
                            <Button onClick={exportExcel} icon={<DownloadOutlined />} type="primary">Export Excel</Button>
                        </ConfigProvider>
                    </Space>

                </Form.Item>
            </Form>
            <Divider />
                <TeacherTable isSearched={isSubmit} setIsSearched={setIsSubmit} searchParams={searchParams} />
        </ThemeContent>
    )
}