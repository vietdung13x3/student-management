import { Table, Tag, Typography } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import { redirect } from "react-router";
import { Link, NavLink, useSearchParams } from "react-router-dom";
import { DEFAULT_PAGE_SIZE, SERVER_URL } from "../../Common/Constant";
/*
    @Columns needed : code, teaching period --> range, name, dateCreated, status, convenor
*/

const columns = [
    {
        key: 'code',
        title: 'Code',
        dataIndex: 'code'
    },
    {
        key:'start',
        title: 'Start Date',
        dataIndex: 'start'
    },
    {
        key: 'end',
        title: 'End Date',
        dataIndex: 'end'
    },
    {
        key: 'name',
        title: 'Name',
        dataIndex: 'name'
    },
    {
        key: 'dateCreated',
        title: 'Date Created',
        dataIndex: 'dateCreated'
    },
        {
        key: 'convenorEmail',
        title: 'Convenor Email',
        dataIndex: 'convenorEmail'
    },
    {
        key: 'convenor',
        title: 'Convenor',
        dataIndex: 'convenor'
    },
    {
        key: 'status',
        title: 'Status',
        dataIndex: 'status',
        render: (_, {status})=> {
            if(status === 'AVAILABLE') {
                return <Tag color={'green'}>Available</Tag>
            }
            if(status === 'REMOVED') {
                return <Tag color={'red'}>Removed</Tag>
            }
            return <Tag color={'yellow'}>Stopped</Tag>
        }
    },
    {
        key: 'operation',
        title: 'Operation',
        dataIndex: 'operation',
        render: (_, {code})=> {
            redirect(`/student-mgmt/course/${code}`)
            return  <Link to={`/student-mgmt/course/${code}`}>Go to course</Link>
        }
    }
]

export default function CourseTable({isAdded, setIsAdded, isSubmit, setIsSubmit}) {
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [page, setPage] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [params, setParams] = useSearchParams();
    useEffect(() => {
        if(isAdded == true || isSubmit == true) {
            fetchData();
            setIsAdded(false);
            setIsSubmit(false);
        }
    }, [ isAdded, isSubmit])
    useEffect(()=> {
        fetchData()
    },[ ])
    async function fetchData() {
        setLoading(true);
        await axios({
            url: `${SERVER_URL}/api/v1.0/course`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: {
                code: params.get('code') == "null" ? null : params.get('code'),
                name: params.get('name') == "null" ? null : params.get('name'),
                from: null,
                to: null,
                convenorEmail: params.get('convenorEmail') == "null" ? null : params.get('convenorEmail'),
                page: page,
                size: 10
            }
        }).then(res => {
            setData([]);
            res.data.data.map((course, i) => {
                setData(data => [...data, {
                    key: i,
                    code: course.code,
                    start: course.start === undefined ? 'Empty' : course.start,
                    end: course.end === undefined ? 'Empty' : course.end,
                    name: course.name,
                    dateCreated: course.dateCreated,
                    convenor: course.convenor,
                    convenorEmail: course.convenorEmail,
                    status: course.status
                }]);
                setTotalElements(res.data.totalElements);
            })
        }).catch(er => {
            
        })
        setLoading(false)
    }
    return (
        <Table dataSource={data} loading={loading} pagination={{total: totalElements, current: page + 1, size: DEFAULT_PAGE_SIZE, onChange: (pagination)=> console.log(pagination)}} columns={columns} />
    )
}