import { Col, Divider, Form, Modal, Row, Input, Space, Button, Typography } from "antd";
import axios from "axios";
import { useState } from "react";

const SERVER_HOST = process.env.REACT_APP_SERVER_URL;

const { Text } = Typography

const {TextArea} = Input;
const commonRules = {
    required: true,
    message: "This field must not be left empty!"
}
export function UploadCourseModal({ isOpen, setIsOpen, notification, setIsAdded }) {
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const onCancel = () => {
        setIsOpen(false);
        form.resetFields();
    }
    const onCreate = (entry) => {
        saveCourse(entry);
    }
    
    const saveCourse = async (course) => {
        setLoading(true);
        await axios({
            url: `${SERVER_HOST}/api/v1.0/course`,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                code: course.code,
                name: course.name,
                convenorEmail: course.convenorEmail,
                description: course.description
            }
        }).then(res => {
            console.log(res.data.message)
            if(res.data.status == '00'){
                notification.success({
                    message: "Success",
                    description: res.data.message
                })
            }else {
                notification.error({
                    message: "Error",
                    description: res.data.message
                })
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: er
            })
        })
        setLoading(false);
        setIsOpen(false);
        setIsAdded(true);
    }

    return (
        <Modal footer={null} width={'50em'} open={isOpen} onCancel={onCancel}>
            <Divider>Create New Course</Divider>
            <Form onFinish={onCreate} form={form} className="p-8" layout="vertical">
                <Row gutter={20} className='mb-6' justify='start'>
                    <Col>
                        <Text type="danger" italic>* Warning: The action of creating course should be done or completed very carefully by the admin. The constraint of creating course will be stricted that requires skilled admin to complete it.</Text>
                    </Col>
                </Row>
                <Row gutter={20} justify='center'>
                    <Col span={12}>
                        <Form.Item rules={[
                            commonRules, {
                                pattern: /^NTY\d+$/,
                                message: 'Course code must start with NTY following with a number sequence after!   '
                            }
                        ]} required label="Code" name={'code'}>
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item rules={[
                            commonRules
                        ]} required label="Name" name={'name'}>
                            <Input />
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item rules={[
                            commonRules
                        ]} required label="Convenor Email" name={'convenorEmail'}>
                    <Input />
                </Form.Item>
                <Form.Item rules={[
                            commonRules
                        ]} required label="Description" name={'description'}>
                    <TextArea autoSize={{ minRows: 3, maxRows: 5 }} placeholder="Enter a short description of the course" allowClear={true} maxLength={500} showCount/>
                </Form.Item>
                <Form.Item className="flex justify-center mt-12 mb-0">
                    <Space>
                        <Button onClick={onCancel} htmlType="button">Cancel</Button>
                        <Button loading={loading} type="primary" htmlType="submit">Submit</Button>
                    </Space>
                </Form.Item>
            </Form>
        </Modal>
    )
}