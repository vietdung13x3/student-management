import { Button, Divider, Modal, Select, Space, Transfer } from "antd";
import useNotification from "antd/es/notification/useNotification";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { SERVER_URL } from "../../Common/Constant";

export default function AddTeacherModal({open, setOpen, notification, setIsAdded}) {
    const [newTargetKeys, setNewTargetKeys] = useState([]);
    const [dataSource, setDataSource] = useState([]);
    const {code} = useParams();
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        if(open) {
            fetchData()
        }
    }, [open])
    async function addTeachers(teachers) {
        await axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}/teachers`,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                emails: teachers
            }
        }).then(res => {
            if(res.data.status == "00") {
                notification.success({
                    message: 'Success',
                    description: res.data.message
                })
                setOpen(false);
                setIsAdded(true);
            }else {
                notification.error({
                    message: 'Error',
                    description: res.data.message
                })
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: 'Internal Server Error'
            })
        })
    }
    function onClickAdd() {
        setLoading(true);
        addTeachers(newTargetKeys);
        setNewTargetKeys([])
        setLoading(false);
    }
    async function fetchData() {
        setDataSource([]);
        await axios({
            url: `${SERVER_URL}/api/v1.0/teacher/emails`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: {
                ignoreCourseCode: code
            }
        }).then( res => {
            console.log(res.data.data.length)
            res.data.data.map((email, i) => {
                setDataSource(dataSource => [ ...dataSource, {
                    key: email,
                    title: email,
                }]);
            })
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: 'Unable to load teacher emails'
            })
        })
    }
    return (
        <Modal footer={null} width={'80em'} open={open} onCancel={()=> setOpen(false)}>
            <Divider>Add Teacher</Divider>
            <Transfer className="flex justify-center align-center mt-12" listStyle={{
                width: '30em',
            }} dataSource={dataSource} targetKeys={newTargetKeys} onChange={(newTarget)=> setNewTargetKeys(newTarget)} showSearch operations={['Add', 'Remove']} render={({title})=> title } />
            <Space className="flex justify-center align-center w-full mt-10">
                <Button loading={loading} htmlType="button" onClick={() => setOpen(false)}>Cancel</Button>
                <Button onClick={onClickAdd} loading={loading} htmlType="button" type="primary">Add To Course</Button>
            </Space>
        </Modal>
    )
}