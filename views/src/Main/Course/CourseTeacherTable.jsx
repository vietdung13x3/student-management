import { Table, Radio, Tag, Button } from "antd";
import { useCallback, useEffect, useState } from "react";
import AddTeacherModal from "./AddTeacherModal";
import { SERVER_URL } from "../../Common/Constant";
import axios from "axios";
import { useParams } from "react-router";
import useNotification from "antd/es/notification/useNotification";
export default function CourseTeacherTable({display}) {
    const [isOpenModal, setIsOpenModal] = useState(false);
    const [dataSource, setDataSource]= useState([]);
    const {code} = useParams();
    const [loadingButton, setLoadingButton] = useState(false);
    const [loadingTable, setLoadingTable] = useState(false)
    const [notification, contextHolder] = useNotification();
    const [isAdded, setIsAdded] = useState(false);
    const [pagination, setPagination] = useState({page: 0, total: 0});
    const [page, setPage] = useState(0);
    const cols = [
        {
            key: 'firstname',
            title: 'Firstname',
            dataIndex: 'firstname'
        },
        {
            key: 'lastname',
            title: 'Lastname',
            dataIndex: 'lastname'
        },
        {
            key: 'identifyCode',
            title: 'Identify Code',
            dataIndex: 'identifyCode'
        },
        {
            key: 'email',
            title: 'Email',
            dataIndex: 'email',
        },
        {
            key: 'status',
            title: 'Status',
            dataIndex: 'status',
            render: (_, { status }) => {
                return status === 'ACTIVE' ? <Tag color={'green'}>{status}</Tag> : <Tag color={'red'}>{status}</Tag>
            }
        },
        {
            key: 'operation',
            title: 'Operation',
            dataIndex: 'operation',
            render: (_, { email }) => {
                return <Button onClick={() => onRemove(email)} danger htmlType="button" type="primary">Remove</Button>
            }
        }
    ]
    async function onRemove(email) {
        setLoadingButton(true);
        await axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}/teachers`,
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                email: email
            }
        }).then(res => {
            if(res.data.status == "00") {
                setIsAdded(true);
                notification.success({
                    message: 'Success',
                    description: res.data.message
                })
            }else{
                notification.error({
                    message: 'Error',
                    description: res.data.message
                })
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: "Internal server error"
            })
        })
        setLoadingButton(false)
    }
    const fetchTeacher = useCallback(async () =>{
        setDataSource([])
        await axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}/teachers`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: {
                page: page,
                size: 10
            }

        }).then(res => {
            if(res.data.data.length == 0  && page != 0) {
                setPage(prev => prev - 1)
            }
            res.data.data.map(teacher => {
                setDataSource(dataSource => [...dataSource, {
                    key: teacher.email,
                    firstname: teacher.firstname,
                    lastname: teacher.lastname,
                    identifyCode: teacher.identifyCode,
                    email: teacher.email,
                    status: teacher.status
                }]) 
            })
            setPagination({page: res.data.currentPage, total: res.data.totalElements})
        })
    }, [isAdded, page])
    useEffect(()=> {
        setLoadingTable(true);
        fetchTeacher()
        setLoadingTable(false);
    },[page])
    useEffect(() => {
        if(isAdded) {
            setLoadingTable(true);
            fetchTeacher()
            setLoadingTable(false);
            setIsAdded(false);
        }
    }, [isAdded])
    return (
        <div className={display ? '' : 'hidden'}>
            {contextHolder}
            <AddTeacherModal fetchTeacher={fetchTeacher} setIsAdded={setIsAdded} notification={notification} open={isOpenModal} setOpen={setIsOpenModal} />
            <Button onClick={() => setIsOpenModal(true)} type="primary" className="flex justify-start mb-8" htmlType="button">Add Teacher</Button>
            <Table pagination={{current: pagination.page + 1, total: pagination.total, defaultPageSize: 10, onChange: (page)=> {
                setPage(page-1);
            }}} loading={loadingTable} dataSource={dataSource} columns={cols} />
        </div>

    )
}