import { Button, Divider, Form, Modal, Select, Space } from "antd";
import axios from "axios";
import { useState } from "react";
import { useParams } from "react-router";
import { SERVER_URL } from "../../Common/Constant";

export default function AssignPeriodModal({ open, setOpen, setIsUpdated, notification }) {
    const {code} = useParams();
    const [loading, setLoading]= useState(false);
    const assign = async (teachingPeriod) => {
        setLoading(true);
        axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}/teaching-period`,
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: {
                period: teachingPeriod
            }
        }).then(res => {
            console.log(res)
            if(res.data.status === "00") {
                notification.success({
                    message: 'Success',
                    description: res.data.message
                })
                setOpen(false);
                setIsUpdated(true);
            }else {
                notification.error({
                    message: 'Error',
                    description: res.data.message
                })
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: er.message
            })
        })
        setLoading(false);
    }
    const onFinish = (data) => {
        assign(data.teachingPeriod);
    } 
    return (
        <Modal footer={null} open={open} onCancel={() => setOpen(false)}>
            <Divider>Assign Period</Divider>
            <Form onFinish={onFinish} layout="vertical" className="mt-10">
                <Form.Item label="Choosing Teaching Period" name={"teachingPeriod"} rules={[
                    {
                        required: true,
                        message: "You must assign a teaching period before submit"
                    }
                ]}>
                    <Select placeholder="Teaching Period" className="w-full" onChange={(a) => console.log(a)}>
                        <Select.Option value="SEMESTER1" >Semester 1</Select.Option>
                        <Select.Option value="SEMESTER2" >Semester 2</Select.Option>
                        <Select.Option value="WINTER" >Winter</Select.Option>
                        <Select.Option value="SUMMER" >Summer</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item className="flex justify-center mt-12">
                    <Space>
                        <Button htmlType="button" onClick={()=> setOpen(false)}>Cancel</Button>
                        <Button type="primary" htmlType="submit" >Assign</Button>
                    </Space>

                </Form.Item>
            </Form>
        </Modal>
    )
}