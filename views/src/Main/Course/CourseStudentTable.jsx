import { Button, Divider, Form, Input, Modal, Space, Table, Tag, Transfer, Typography, notification } from "antd"
import axios from "axios";
import { useEffect, useState } from "react"
import { useParams } from "react-router";
import { DEFAULT_PAGE_SIZE, SERVER_URL } from "../../Common/Constant";


export default function CourseStudentTable({ notDisplay }) {
    const [open, setOpen] = useState(false);
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const [newTargetKeys, setNewTargetKeys] = useState([]);
    const [dataSource, setDataSource] = useState([]);
    const [loadingButton, setLoadingButton] = useState(false);
    const [page, setPage] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [loadingTable, setLoadingTable] = useState(false);
    const [data, setData] = useState([]);
    const { code } = useParams();
    const fetchStudentNotInCourse = async () => {
        setDataSource([])
        await axios({
            url: `${SERVER_URL}/api/v1.0/student/not-in-course`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: {
                code: code
            }
        }).then(res => {
            res.data.data.map((student, i) => {
                setDataSource(dataSource => [...dataSource, {
                    key: student.id,
                    firstname: student.firstname,
                    lastname: student.lastname,
                    identifyCode: student.identifyCode
                }]);
            })
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: 'Internal Server Error!'
            })
        })
    }
    const deleteStudent = async (id) => {
        setLoadingButton(true);
        await axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}/student/${id}`,
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            }
        }).then(res => {
            if (res.data.status == "00") {
                notification.success({
                    message: 'Success',
                    description: res.data.message
                })
                setLoadingTable(true);
                fetchStudents()
                setLoadingTable(false)
            } else {
                notification.error({
                    message: 'Error',
                    description: res.data.message
                })
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: "Internal Server Error"
            })
        })
        setLoadingButton(false)
    }
    const cols = [
        {
            key: 'firstname',
            title: 'Firstname',
            dataIndex: 'firstname'
        },
        {
            key: 'lastname',
            title: 'Lastname',
            dataIndex: 'lastname'
        },
        {
            key: 'identifyCode',
            title: 'Identify Code',
            dataIndex: 'identifyCode'
        },
        {
            key: 'dateOfBirth',
            title: 'Date Of Birth',
            dataIndex: 'dateOfBirth'
        },
        {
            key: 'status',
            title: 'Status',
            dataIndex: 'status',
            render: (_, { status }) => {
                return status === "ACTIVE" ? <Tag color="green">{status}</Tag> : <Tag color="red">{status}</Tag>
            }
        },
        {
            key: 'operation',
            title: 'Operation',
            dataIndex: 'operation',
            render: (_, { id }) => {
                return <Button loading={loadingButton} onClick={() => {
                    deleteStudent(id);
                }} type="primary" danger>Remove</Button>
            }
        },
    ]
    const fetchStudents = async () => {
        setData([]);
        await axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}/student`,
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: {
                page: page,
                size: DEFAULT_PAGE_SIZE
            }
        }).then(res => {
            if (res.data.status == "00") {
                if (page > 0 && res.data.data.length == 0) {
                    setPage(page => page - 1);
                }
                else {
                    res.data.data.map((student, i) => {
                        setData(data => [...data, {
                            firstname: student.firstname,
                            lastname: student.lastname,
                            identifyCode: student.identifyCode,
                            dateOfBirth: student.dateOfBirth,
                            status: student.status,
                            id: student.id,
                            key: i
                        }])
                    })
                }
                setTotalElements(res.data.totalElements);


            }
        }).catch(er => {
            notification.error({
                message: "Error",
                description: "Internal Server Error!"
            })
        })
    }
    useEffect(() => {
        setLoadingTable(true);
        fetchStudents();
        setLoadingTable(false)
    }, [page]);
    useEffect(() => {
        fetchStudentNotInCourse()
    }, [])
    const addStudents = async () => {
        setLoading(true);
        await axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}/student`,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                ids: newTargetKeys
            }
        }).then(res => {
            if (res.data.status == '00') {
                setNewTargetKeys([]);
                notification.success({
                    message: 'Success',
                    description: res.data.message
                })
                fetchStudents();
                fetchStudentNotInCourse();
            } else {
                notification.error({
                    message: 'Error',
                    description: res.data.message
                })
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: 'Internal Server Error'
            })
        })
        setLoading(false)
    }
    const onAddStudent = (e) => {
        e.preventDefault();
        addStudents();

    }
    return (
        <div className={notDisplay ? 'hidden' : ''}>
            <Modal footer={null} width={"60em"} open={open} onCancel={() => setOpen(false)}>
                <Divider>Add Student to Course {code}</Divider>
                <Transfer render={(student) => `${student.firstname} ${student.lastname} (${student.identifyCode})`} dataSource={dataSource} targetKeys={newTargetKeys} onChange={(newTarget) => setNewTargetKeys(newTarget)} showSearch className="flex justify-center align-center mt-12" listStyle={{ width: '30em' }} operations={['Add', 'Remove']} />
                <Space className="flex justify-center align-center mt-8">
                    <Button onClick={() => setOpen(false)}>Cancel</Button>
                    <Button onClick={onAddStudent} loading={loading} type="primary">Add Students</Button>
                </Space>
            </Modal>
            <Space className="flex justify-start mb-8">
                <Button onClick={(e) => setOpen(true)} type="primary">Add Student</Button>
            </Space>
            <Table pagination={{ pageSize: DEFAULT_PAGE_SIZE, total: totalElements, current: page + 1, onChange: (page, size) => setPage(page - 1) }} loading={loadingTable} dataSource={data} columns={cols} />
        </div>
    )
}