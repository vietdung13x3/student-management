import { AutoComplete, Button, Divider, Form, Modal, Space, Typography, notification } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import { SERVER_URL } from "../../Common/Constant";
import { useParams } from "react-router";
const { Text } = Typography
export default function AssignCourseConvenor({ open, setOpen, fetchCourse }) {
    const [teacherEmails, setTeacherEmails] = useState([]);
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const { code } = useParams();
    const fetchTeacherEmail = async () => {
        setTeacherEmails([])
        await axios({
            url: `${SERVER_URL}/api/v1.0/teacher/emails`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: {
                ignoreCourseCode: code
            }
        }).then(res => {
            res.data.data.map((email, i) => {
                setTeacherEmails(teacherEmails => [...teacherEmails, {
                    key: i,
                    value: email
                }])
            })
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: 'Internal Server Error!'
            })
        })
    }
    useEffect(() => {
        fetchTeacherEmail();
    }, [])
    const assignConvenor = async (teacherEmail) => {
        setLoading(true)
        await axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}/convenor`,
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                teacherEmail: teacherEmail
            }
        }).then(res => {
            if (res.data.status == "00") {
                notification.success({
                    message: 'Success',
                    description: res.data.message
                })
                setOpen(false)
                fetchCourse();
            } else {
                notification.error({
                    message: 'Error',
                    description: res.data.message
                })
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: 'Internal Server Error!'
            })
        })
        setLoading(false)
    }
    const onfinish = (form) => {
        assignConvenor(form.teacherEmail);
    }
    return (
        <Modal footer={null} width={'50em'} open={open} onCancel={() => setOpen(false)}>
            <Divider>Assign Course Convenor</Divider>
            <div className="m-8">
                <Text type="danger">* Please notice that assign a new convenor will remove the current convenor from this course!</Text>
                <Form onFinish={onfinish} form={form} layout="vertical" className="mt-8">
                    <Form.Item label="Select teacher to assign as convenor" name={'teacherEmail'} rules={[
                        { required: true, message: "You must choose an existed convenor's email" }
                    ]}>
                        <AutoComplete options={teacherEmails} filterOption={(input, option) => {
                            return option.value.toUpperCase().indexOf(input.toUpperCase()) !== -1
                        }} />
                    </Form.Item>
                    <Form.Item className="flex justify-center align-center">
                        <Space>
                            <Button>Cancel</Button>
                            <Button type="primary" htmlType="submit">Assign Convenor</Button>
                        </Space>
                    </Form.Item>
                </Form>
            </div>

        </Modal>
    )
}