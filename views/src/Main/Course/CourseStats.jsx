import { SearchOutlined, UploadOutlined } from "@ant-design/icons";
import { Button, Col, DatePicker, Divider, Form, Input, Row, Select, Space, Tag } from "antd";
import useNotification from "antd/es/notification/useNotification";
import moment from "moment";
import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import ThemeContent from "../ThemeContent";
import CourseTable from "./CourseTable";
import { UploadCourseModal } from "./UploadCourseModal";
const { RangePicker } = DatePicker;
export default function CourseStats() {
    const [isUploadOpen, setIsUploadOpen] = useState(false);
    const [notification, contextHolder] = useNotification();
    const [isAdded, setIsAdded] = useState(false);
    const [params, setParams] = useSearchParams();
    const [isSubmit, setIsSubmit] = useState(false);
    useEffect(()=> {
        setParams({}) //  Clear all the params
    },[])
    const onFinish = (data) => {
        setParams({
            code: data.code === undefined ? null : data.code,
            name: data.name === undefined ? null : data.name,
            from: data.dateCreated === undefined ? null : moment(data.dateCreated[0].$d).format('DD/MM/YYYY'),
            to: data.dateCreated === undefined ? null : moment(data.dateCreated[1].$d).format('DD/MM/YYYY'),
            convenorEmail: data.convenorEmail === undefined ? null : data.convenorEmail,
            teachingPeriod: data.teachingPeriod === undefined ? null : data.teachingPeriod
        })
        setIsSubmit(true);
    }
    return (
        <ThemeContent>
            {contextHolder}
                <UploadCourseModal setIsAdded={setIsAdded} notification={notification} isOpen={isUploadOpen} setIsOpen={setIsUploadOpen} />
                <Form onFinish={onFinish} layout="vertical">
                    <Row gutter={20}>
                        <Col span={8}>
                            <Form.Item label="Code" name={'code'}>
                                <Input type="text" />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Name" name={"name"}>
                                <Input type="text" />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Created Date" name={'dateCreated'}>
                                <RangePicker format={"DD/MM/YYYY"} className="w-full" />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Convenor Email" name={'convenorEmail'}>
                                <Input type="text" />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label="Teaching Period" name={'teachingPeriod'}>
                                <Select placeholder="None" className="w-full text-left">
                                    <Select.Option value="">None</Select.Option>
                                    <Select.Option value="SEMESTER1">Semester 1</Select.Option>
                                    <Select.Option value="SEMESTER2">Semester 2</Select.Option>
                                    <Select.Option value="SUMMER">Summer</Select.Option>
                                    <Select.Option value="WINTER">Winter</Select.Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        
                    </Row>
                    <Form.Item>
                        <Space>
                            <Button icon={<SearchOutlined />} type="primary" htmlType="submit">Search</Button>
                            <Button icon={<UploadOutlined />} onClick={() => setIsUploadOpen(true)} type="default" htmlType="button">Create Course</Button>
                        </Space>
                    </Form.Item>
                </Form>
                <Divider />
                <CourseTable isSubmit={isSubmit} setIsSubmit={setIsSubmit} setIsAdded={setIsAdded} isAdded={isAdded} />
        </ThemeContent>
    )
}