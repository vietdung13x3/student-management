import axios from "axios";
import { useNavigate, useParams } from "react-router"
import ThemeContent from "../ThemeContent";
import { SERVER_URL } from "../../Common/Constant";
import { useCallback, useEffect, useState } from "react";
import useNotification from "antd/es/notification/useNotification";
import { Descriptions, Spin, Tag, Typography, Button, Divider, Badge, Space, Radio } from "antd";
import { DeleteOutlined, StopOutlined, PlayCircleOutlined } from "@ant-design/icons";
import Link from "antd/es/typography/Link";
import AssignPeriodModal from "./AssignPeriodModal";
import CourseTeacherTable from "./CourseTeacherTable";
import CourseStudentTable from "./CourseStudentTable";
import AssignCourseConvenor from "./AssignCourseConvenor";
const { Title, Paragraph } = Typography;

export default function CourseDetailView() {
    const { code } = useParams();
    const navigate = useNavigate();
    
    const [loading, setLoading] = useState(false);
    const [notification, contextHolder] = useNotification();
    const [openAssignConvenor, setOpenAssignConvenor] = useState(false);
    const [color, setColor] = useState("green");
    const [courseDetail, setCourseDetail] = useState({});
    const [action, setAction] = useState();
    const [isOpenModal, setIsOpenModal] = useState(false);
    const [status, setStatus] = useState("");   
    const [updatedPeriod, setUpdatedPeriod] = useState(false);
    const [isDisplayTeacher, setIsDisplayTeacher] = useState(true);
    const updateCourseStatus = useCallback(async (status, code) => {
        setLoading(true);
        await axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}`,
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: {
                status: status
            }
        }).then(res => {
            if(res.data.status === "00") {
                notification.success({
                    message:"Success",
                    description: res.data.message
                })
            }else {
                notification.error({
                    message: 'Error',
                    description: res.data.message
                })
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: er.message
            })
        })
        setLoading(false);
    }, [status])
    useEffect(()=> {
        if (status !== '') {
            updateCourseStatus(status, code);
            setStatus("");
        }
    },[status])
    useEffect(() => {
        fetchData();
    }, [updateCourseStatus])
    useEffect(() => {
        if(updatedPeriod === true) {
            fetchData();
            setUpdatedPeriod(false);
        }
    }, [updatedPeriod])
    useEffect(() => {
        if (courseDetail.status === "AVAILABLE") {
            setColor("green");
            setAction(
                <Space>
                    <Button onClick={()=> setStatus('STOPPED')} icon={<StopOutlined />} danger>Stop Course</Button>
                    <Button onClick={()=> setStatus('REMOVED')} icon={<DeleteOutlined />} type="primary" danger>Remove Course</Button>
                </Space>
            )
        } else if (courseDetail.status === "REMOVED") {
            setColor("#f50")
            setAction(
                <Space>
                    <Tag color={'red'}>This course is removed</Tag>
                </Space>
            )
        } else if (courseDetail.status === "STOPPED") {
            setColor("warning")
            setAction(
                <Space>
                    <Button onClick={()=> setStatus('AVAILABLE')} icon={<PlayCircleOutlined />} type='primary'>Reopen Course</Button>
                    <Button onClick={()=> {
                        setStatus('REMOVED');
                        navigate("/student-mgmt/course/stats")
                        }} icon={<DeleteOutlined />} type="primary" danger>Remove Course</Button>
                </Space>
            )
        }
    }, [courseDetail])
    const fetchData = async () => {
        setLoading(true);
        await axios({
            url: `${SERVER_URL}/api/v1.0/course/${code}`,
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            }
        }).then(res => {
            if(res.data.status =="00") {
                setCourseDetail(res.data.data)
            }else if (res.data.status === "02"){
                navigate("/not-found", {replace: true})
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: er
            })
        })
        setLoading(false);
    }
    return (
        <ThemeContent>
            {contextHolder}
            <AssignCourseConvenor fetchCourse={fetchData} open={openAssignConvenor} setOpen={setOpenAssignConvenor}/>
            <AssignPeriodModal notification={notification} setIsUpdated={setUpdatedPeriod} open={isOpenModal} setOpen={setIsOpenModal}/>
            <Descriptions extra={action} title={<Title level={3} className="text-left">Course Details</Title>} className={loading ? "hidden" : ""} >
                <Descriptions.Item label="Code">
                    <Paragraph italic >{courseDetail.code}</Paragraph>
                </Descriptions.Item>
                <Descriptions.Item label="Name">
                    <Paragraph italic >{courseDetail.name}</Paragraph>
                </Descriptions.Item>
                <Descriptions.Item label="Date Created">
                    <Paragraph italic >{courseDetail.dateCreated}</Paragraph>
                </Descriptions.Item>
                <Descriptions.Item label="Course Convenor">
                    <Paragraph italic editable={{editing: false, onStart: ()=> setOpenAssignConvenor(true)}}>{courseDetail.convenor}</Paragraph>
                </Descriptions.Item>
                <Descriptions.Item label="Teaching Period">
                    <Paragraph italic >
                        <Link onClick={()=> setIsOpenModal(true)}>
                            {courseDetail.teachingPeriod ? courseDetail.teachingPeriod : "Unassigned"}
                            ({courseDetail.startDate ? courseDetail.startDate : ""} - {courseDetail.endDate ? courseDetail.endDate : ""})
                        </Link>
                    </Paragraph>
                </Descriptions.Item>
                <Descriptions.Item label="Status">
                    <Tag color={color}>{courseDetail.status}</Tag>
                </Descriptions.Item>
                <Descriptions.Item label="Description">
                    <Badge>
                        <Paragraph italic >{courseDetail.description}</Paragraph>
                    </Badge>

                </Descriptions.Item>

            </Descriptions>
            <Spin spinning={loading} />
            <Divider />
            <Radio.Group defaultValue={"1"} onChange={(e)=> {
                if(e.target.value === '1') {
                    setIsDisplayTeacher(true);
                }else {
                    setIsDisplayTeacher(false)
                }
            }}>
                <Radio defaultChecked value={'1'}>Teaching Team</Radio>
                <Radio value={"2"}>Enrolled Students</Radio>
            </Radio.Group>
            <CourseTeacherTable display={isDisplayTeacher}/>
            <CourseStudentTable notDisplay={isDisplayTeacher}/>
        </ThemeContent>
    )
}