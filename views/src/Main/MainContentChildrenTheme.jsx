import React, { useContext } from "react";
const ThemeStyleContext = React.createContext();

export function useThemeStyle() {
    return useContext(ThemeStyleContext);
}

export function MainContentChildrenTheme({table}){
    const style = {
        padding: 24,
        textAlign: 'center',
        background: 'white',
    };
    return (
        <ThemeStyleContext.Provider value={style}>
            {table}
        </ThemeStyleContext.Provider>
    )
}