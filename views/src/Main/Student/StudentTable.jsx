import { Anchor, Table, Tag } from "antd"
import Link from "antd/es/typography/Link"
import axios from "axios"
import moment from "moment"
import { useCallback, useEffect, useState } from "react"
import { Outlet, useLocation, useNavigate, useSearchParams } from "react-router-dom"
import { DEFAULT_PAGE_SIZE, SERVER_URL } from "../../Common/Constant"
import UpdateStudentModal from "./UpdateStudentModal"

export default function StudentTable({isSearched, setIsSearched, params}) {
    const [dataSource, setDataSource]= useState([]);
    const {pathname} = useLocation();
    const [ page , setPage] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [loading, setLoading] = useState(false);
    const fetchData = useCallback(async () => {
        setLoading(true);
        setDataSource([]);
        await axios({
            url: `${SERVER_URL}/api/v1.0/student`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: {
                page: page,
                size: 10,
                firstname: params.firstname ? params.firstname : null,
                lastname: params.lastname ? params.lastname : null,
                identifyCode: params.identifyCode ? params.identifyCode : null,
                dateOfBirth: params.dateOfBirth ? params.dateOfBirth : null,
                expiredFrom: params.dateExpired ? moment(params.dateExpired[0].$d).format('DD/MM/YYYY') : null,
                expiredTo : params.dateExpired ? moment(params.dateExpired[1].$d).format('DD/MM/YYYY') : null,
                joinedFrom: params.dateJoined ? moment(params.dateJoined[0].$d).format('DD/MM/YYYY') : null,
                joinedTo: params.dateJoined ? moment(params.dateJoined[1].$d).format('DD/MM/YYYY') : null,
                gender: params.gender ? params.gender : null,
                district: params.district ? params.district : null 
            }

        }).then(res => {
            res.data.data.map((student, i) => {
                setTotalElements(res.data.totalElements)
                setDataSource(dataSource => [...dataSource, {
                    firstname: student.firstname ,
                    lastname: student.lastname,
                    identifyCode: student.identifyCode,
                    dateOfBirth: student.dateOfBirth,
                    dateExpired: student.dateExpired,
                    dateJoined: student.dateJoined,
                    district: student.district,
                    status: student.status,
                    gender: student.gender,
                    key: i,
                    id: student.id
                }])
            })
        })
        setLoading(false);
    }, [page, isSearched, pathname])
    useEffect(() => {
        fetchData();
    },[page, pathname])
    useEffect(()=> {
        if(isSearched) {
            fetchData();
            setIsSearched(false);
        }
    }, [isSearched])
    const cols = [
        {
            key: 'firstname',
            title: 'Firstname',
            dataIndex: 'firstname'
        },
        {
            key: 'lastname',
            title: 'Lastname',
            dataIndex: 'lastname'
        },
        {
            key: 'identifyCode',
            title: 'Identify Code',
            dataIndex: 'identifyCode'
        },
        {
            key: 'dateOfBirth',
            title: 'Date Of Birth',
            dataIndex: 'dateOfBirth'
        },
        {
            key: 'dateExpired',
            title: 'Expired Date',
            dataIndex: 'dateExpired'
        },
        {
            key: 'dateJoined',
            title: 'Joined Date',
            dataIndex: 'dateJoined'
        },
        {
            key: 'gender',
            title: 'Gender',
            dataIndex: 'gender'
        },
        {
            key: 'district',
            title: 'District',
            dataIndex: 'district'
        },
        {
            key: 'status',
            title: 'Status',
            dataIndex: 'Status',
            render: (_, {status}) => {
                if(status === "ACTIVE") {
                    return <Tag color={'green'}>Active</Tag>
                }else if (status  === 'EXPIRED') {
                    return <Tag color={'yellow'}>Expired</Tag>
                }
                return <Tag color={'red'}>Locked</Tag>
            }
        },
        {
            key: 'operation',
            title: 'Operation',
            dataIndex: 'operation',
            render: (_,{id, status}) => {
                let disable = true;
                if(status === "ACTIVE" || status === "LOCKED"){
                    disable = false;
                }
                return <Link disabled={disable} onClick={()=> navigate(`/student-mgmt/student/${id}`)}>Edit</Link>
            }
        }
    ]
    const navigate = useNavigate();
    return (
        <>
            <Outlet/>
            <Table columns={cols} dataSource={dataSource} pagination={{defaultPageSize: DEFAULT_PAGE_SIZE, current: page + 1, total: totalElements, onChange: (page, pageSize)=> setPage(page - 1)}}/>
        </>
    )
}