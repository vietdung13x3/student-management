import { Button, Col, DatePicker, Divider, Form, Input, Modal, Row, Select, Space } from "antd";
import { useForm } from "antd/es/form/Form";
import axios from "axios";
import moment from "moment";
import { useState } from "react";
import { SERVER_URL } from "../../Common/Constant";

export default function AddStudentModal({ open, setOpen, notfication, setIsSearched }) {
    const [loading, setLoading] = useState(false);
    const [form] = useForm();
    
    async function onFinish(entry) {
        setLoading(true);
        await axios({
            url: `${SERVER_URL}/api/v1.0/student`,
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                firstname: entry.firstname,
                lastname: entry.lastname,
                identifyCode: entry.identifyCode,
                dateOfBirth: moment(entry.dateOfBirth.$d).format('DD/MM/YYYY'),
                numberOfYears: entry.numberOfYears,
                postcode: entry.postcode,
                gender: entry.gender
            }
        }).then(res => {
            if(res.data.status == "00") {
                notfication.success({
                    message: 'Success',
                    description: res.data.message
                })
                setLoading(false)
                setOpen(false)
                setIsSearched(true);
                form.resetFields();
            }else{
                notfication.error({
                    message: 'Error',
                    description: res.data.message
                })
            }
        }).catch(er=> {
            notfication.error({
                message: 'Error',
                description: 'Internal Server Error!'
            })
        })
        setLoading(false)
        
    }
    return (
        <Modal open={open} width={'60em'} footer={null} onCancel={()=> setOpen(false)}>
            <Divider>Add Student</Divider>
            <Form form={form} className="m-12 pb-0" onFinish={onFinish} layout="vertical">
                <Row gutter={20}>
                    <Col span={12}>
                        <Form.Item id="firstname" label="Firstname" name={'firstname'} rules={[
                            {
                                required: true,
                                message: 'Firstname must be filled!'
                            }, {
                                pattern: '^[A-Za-z ]*$',
                                message: 'Firstname must only contain alphabetical characters'
                            }
                        ]}>
                            <Input />
                        </Form.Item>
                        <Form.Item label="Identify Code" name={'identifyCode'} rules={[
                            {
                                required: true,
                                message: 'Identify Code must be filled!'
                            }, {
                                pattern: '^[0-9]{8}$',
                                message: 'Identify Code must contain 8 digits'
                            }
                        ]}>
                            <Input />
                        </Form.Item>
                        <Form.Item label="Gender" name={'gender'} rules={[
                            {
                                required: true,
                                message: 'Gender is required'
                            }
                        ]}>
                            <Select placeholder="Select gender...">
                                <Select.Option value={"MALE"}>Male</Select.Option>
                                <Select.Option value={"FEMALE"}>Female</Select.Option>
                                <Select.Option value={"OTHER"}>Other</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="Postcode" name={'postcode'} rules={[
                            {
                                required: true,
                                message: 'Postcode is required'
                            }
                        ]}>
                            <Input type="number"/>
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label="Lastname" name={'lastname'} rules={[
                            {
                                required: true,
                                message: 'Lastname must be filled!'
                            }, {
                                pattern: '^[A-Za-z ]*$',
                                message: 'Lastname must only contain alphabetical characters'
                            }
                        ]}>
                            <Input />
                        </Form.Item>
                        <Form.Item label="Date Of Birth" name={'dateOfBirth'} rules={[
                            {
                                required: true,
                                message: 'Date Of Birth must not be null!'
                            }
                        ]}>
                            <DatePicker className="w-full" format={'DD/MM/YYYY'}/>
                        </Form.Item>
                        <Form.Item name={'numberOfYears'} label="Number Of Study Years" rules={[
                            {
                                required: true,
                                message: 'Number Of Study Years is required'
                            }
                        ]}>
                            <Select placeholder="Select number of study year...">
                                <Select.Option value={1}>1 Years</Select.Option>
                                <Select.Option value={2}>2 Years</Select.Option>
                                <Select.Option value={3}>3 Years</Select.Option>
                                <Select.Option value={4}>4 Years</Select.Option>
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item className="flex justify-center align-center mt-0">
                    <Space>
                        <Button onClick={()=> setOpen(false)}>Cancel</Button>
                        <Button loading={loading} htmlType="submit" type="primary">Submit</Button>
                    </Space>
                </Form.Item>
            </Form>
        </Modal>
    )
}