import { SearchOutlined, UploadOutlined } from "@ant-design/icons";
import { Button, Col, DatePicker, Divider, Form, Input, Row, Select, Space, ConfigProvider, AutoComplete } from "antd";
import useNotification from "antd/es/notification/useNotification";
import axios from "axios";
import moment from "moment";
import { useCallback, useEffect, useState } from "react";
import { Outlet, useLocation, useSearchParams } from "react-router-dom";
import { SERVER_URL } from "../../Common/Constant";
import ThemeContent from "../ThemeContent";
import AddStudentModal from "./AddStudentModal";
import StudentTable from "./StudentTable";
import UploadExcelStudents from "./UploadExcelStudents";

const { RangePicker } = DatePicker
export default function StudentView() {
    const [districts , setDistricts] = useState([]);
    const {pathname} = useLocation();
    const [notification, contextHolder] = useNotification();
    const [isOpen, setIsOpen] = useState(false);
    const [isOpenExcel, setIsOpenExcel] = useState(false);
    const [isSearched, setIsSearched] = useState(false);
    const [searchParams, setSearchParams] = useState({
        firstname: null,
        lastname:null,
        identifyCode: null,
        dateOfBirth: null,
        expiredFrom: null,
        expiredTo :null,
        joinedFrom: null,
        joinedTo: null,
        gender: null,
        districts: null 
    });

    const fetchDistrict = useCallback(async () => {
        setDistricts([])
        await axios({
            url: `${SERVER_URL}/api/v1.0/commons/district`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem("jwt")}`
            }
        }).then(res => {
            res.data.data.map(district => {
                setDistricts(districts => [...districts, {value: district.name, id: district.id}]);
            })
        })
    }, [])
    useEffect(() => {
        fetchDistrict();
    },[])
    const onfinish = (params) => {
        setSearchParams(params)
        setIsSearched(true)
    }
    return (
        <>
            
            <AddStudentModal setIsSearched={setIsSearched} notfication={notification} open={isOpen} setOpen={setIsOpen}/>
            <UploadExcelStudents notification={notification} setOpen={setIsOpenExcel} open={isOpenExcel}/>
            {contextHolder}
            <ThemeContent>
                <Form layout="vertical" onFinish={onfinish}>
                    <Row gutter={20}>
                        <Col span={8}>
                            <Form.Item label='Firstname' name={'firstname'}>
                                <Input type="text" />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label='Lastname' name={'lastname'}>
                                <Input type="text" />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label='Identify Code' rules={[
                                {
                                    pattern: "^[0-9]+$",
                                    message: "Identify Code only contains number!"
                                }
                            ]} name={'identifyCode'}>
                                <Input type="text" />
                            </Form.Item>
                        </Col>

                    </Row>
                    <Row gutter={20}>
                        <Col span={8}>
                            <Form.Item label='Date Joined' name={'dateJoined'}>
                                <RangePicker className="w-full" format={"DD/MM/YYYY"}/>
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label='Date Expired' name={'dateExpired'}>
                                <RangePicker className="w-full" format={"DD/MM/YYYY"}/>
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label='Gender' name={'gender'}>
                                <Select className="w-full text-left" >
                                    <Select.Option value="MALE">Male</Select.Option>
                                    <Select.Option value="FEMALE">Female</Select.Option>
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={20}>
                        <Col span={8}>
                            <Form.Item label='District' name={'district'}>
                                <Select className="w-full text-left">
                                    {
                                        districts.map((district,i) => <Select.Option key={i} value={district.value}>{district.value}</Select.Option>)
                                    }
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Form.Item>
                        <Space>

                            <Button icon={<SearchOutlined />} type="primary" htmlType="submit">Search</Button>
                            <ConfigProvider
                                theme={{
                                    token: {
                                        colorPrimary: '#00b96b',
                                    },
                                }}
                            >
                                <Button onClick={()=> setIsOpenExcel(true)} type="primary" icon={<UploadOutlined />} htmlType="button">Import Students</Button>
                            </ConfigProvider>
                            <Button onClick={()=> setIsOpen(true)} icon={<UploadOutlined />} htmlType="button">Add Student</Button>
                        </Space>
                    </Form.Item>
                </Form>
                <Divider/>
                <StudentTable params={searchParams} isSearched={isSearched} setIsSearched={setIsSearched}/>
            </ThemeContent>
        </>
    )
}