import { Badge, Button, Col, DatePicker, Descriptions, Divider, Form, Input, List, Modal, notification, Row, Select, Space, Table, Tag } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import { SERVER_URL } from "../../Common/Constant";
import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import Typography from "antd/es/typography/Typography";
dayjs.extend(customParseFormat)

const {Text} = Typography;
const cols = [
    {
        key: 'name',
        title: "Name",
        dataIndex:'name'
    },
    {
        key: 'code',
        title: 'Code',
        dataIndex: 'code'
    }
]
export default function UpdateStudentModal() {
    const navigate = useNavigate();
    const { id } = useParams();
    const [loading, setLoading] = useState(false);
    const [loadButton, setLoadButton] = useState(false);
    const [student, setStudent] = useState({});
    const [isShowDistrict , setIsShowDistrict] = useState(true)
    const oncancel = (e) => {
        navigate("/student-mgmt/student")
    }
    const fetchData = async () => {
        await axios.get(`${SERVER_URL}/api/v1.0/student/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            }
        }).then(res => {
            if (res.data.status == "00") {
                setStudent(res.data.data);
            } else {
                navigate('/not-found')
            }
        }).catch(er => {
            notification.error({
                message: 'Network Error',
                description: 'Internal Server Trouble!'
            })
        })
    }
    const updateStatus = async (status) => {
        await axios({
            url: `${SERVER_URL}/api/v1.0/student/${id}`,
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                status: status
            }
        }).then(res => {
            if(res.data.status == 0) {
                notification.success({
                    message: "Success",
                    description: res.data.message
                })
                navigate("/student-mgmt/student")
            }else{
                notification.error({
                    message: "Error",
                    description: res.data.message
                })
            }
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: "Internal server error!"
            })
        })
    }
    useEffect(() => {
        setLoading(false)
        fetchData()
        setLoading(true)
    }, [])
    const lock = (e) => {
        setLoadButton(true);
        updateStatus("LOCKED");
        setLoadButton(false);
    }
    const activate = (e) => {
        setLoadButton(true);
        updateStatus("ACTIVE");
        setLoadButton(false);
    }
    console.log(student)
    return (
        <>
            <Modal open={loading} footer={null} width={"60em"} onCancel={oncancel}>
                <Divider>Update Student Status</Divider>
                <Descriptions layout="horizontal" className="m-8">
                    <Descriptions.Item label="Firstname">{student.firstname}</Descriptions.Item>
                    <Descriptions.Item label="Lastname">{student.lastname}</Descriptions.Item>
                    <Descriptions.Item label="Identify Code">{student.identifyCode}</Descriptions.Item>
                    <Descriptions.Item label="Gender">{student.gender}</Descriptions.Item>
                    <Descriptions.Item label="Date Of Birth">{student.dateOfBirth}</Descriptions.Item>
                    <Descriptions.Item label="Date Joined">{student.dateJoined}</Descriptions.Item>
                    <Descriptions.Item label="Date Expired">{student.dateExpired}</Descriptions.Item>
                    <Descriptions.Item label="Status">{
                        student.status == "EXPIRED" || student.status == "LOCKED" ? <Tag color={'red'}>{student.status}</Tag> : <Tag color={'green'}>{student.status}</Tag>
                    }</Descriptions.Item>
                    <Descriptions.Item label="District">{student.district}</Descriptions.Item>
                    <Descriptions.Item label="Course">
                        <div className="flex flex-col">
                        {
                            student.courses ? student.courses.map((course, i) => <Typography key={i}>{i+1}. {course.code} - {course.name}</Typography>) : <Typography>No Enrolled Courses</Typography>
                        }
                        </div>

                    </Descriptions.Item>
                </Descriptions>
                <Space className="flex justify-center align-center">
                    <Button onClick={oncancel}>Cancel</Button>
                    {
                        student.status === "ACTIVE" ? <Button onClick={lock} danger type="primary">Lock Student</Button> : <Button onClick={activate} type="primary">Reactivate Student</Button>
                    }
                </Space>

            </Modal>
        </>
    )
}