import { InboxOutlined } from "@ant-design/icons";
import { Button, Form, Modal, Space, Upload, notification } from "antd";
import { useForm } from "antd/es/form/Form";
import axios from "axios";
import { useState } from "react";
import { SERVER_URL } from "../../Common/Constant";
const { Dragger } = Upload
export default function UploadExcelStudents({ open, setOpen }) {
    const [form] = useForm();
    const [ loading, setLoading ] = useState(false);
    async function upload(file) {
        setLoading(true);
        const form = new FormData();
        form.append('file',file);
        await axios({
            url: `${SERVER_URL}/api/v1.0/student/import`,
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${localStorage.getItem('jwt')}`,
            
            },
            data: form
        }).then(res => {
            if(res.data.status === "00") {
                notification.success({
                    message: 'Success',
                    description: res.data.message
                })
                cancel(); 
                
            }else{
                notification.error({
                    message: "Error",
                    description: res.data.message
                })
            }
            
        }).catch(er => {
            notification.error({
                message: 'Error',
                description: 'Internal Server Error!'
            })
        })
        setLoading(false)
    };

    const cancel = () => {
        setOpen(false)
        form.resetFields();
    }

    const onSubmit = (entry) => {
        const {status, originFileObj} = entry.file.file;
        if(status === 'done') {
            upload(originFileObj);
        }
    }
    
    return (
        <Modal width={'60em'} open={open} onCancel={() => setOpen(false)} footer={null}>
            <Form form={form} onFinish={onSubmit} className="p-4">
                <Form.Item rules={[{
                    required: true,
                    message: 'Must included excel'
                }]} name={'file'}>
                    <Dragger action={'https://www.mocky.io/v2/5cc8019d300000980a055e76'} headers={{
                        authorization: "authorization-text"
                    }} multiple={false} maxCount={1}>
                        <p className="ant-upload-drag-icon">
                            <InboxOutlined />
                        </p>
                        <p className="ant-upload-text">Click or drag file to this area to upload</p>
                        <p className="ant-upload-hint">
                            Support for a multiple students upload within single .xlsx file
                        </p>
                    </Dragger>
                </Form.Item>
                <Form.Item className="flex justify-center align-center">
                    <Space>
                        <Button onClick={() => cancel()} >Cancel</Button>
                        <Button loading={loading} htmlType="submit" type="primary">Submit</Button>
                    </Space>
                </Form.Item>
            </Form>

        </Modal>
    )
}