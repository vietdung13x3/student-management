import { LogoutOutlined, TeamOutlined, ApartmentOutlined } from "@ant-design/icons"
import Sider from "antd/es/layout/Sider"
import { Menu } from "antd"
import { useLocation, useNavigate } from "react-router"
import axios from "axios"
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserGraduate, faUserTie, faFolderTree } from "@fortawesome/free-solid-svg-icons"
import { SERVER_URL } from "../Common/Constant"

library.add(faUserGraduate);
library.add(faUserTie);
library.add(faFolderTree)
const items = [
    {
        label: "Teacher Management", key:"asd", icon: <FontAwesomeIcon icon={faUserTie}/>, 
        children: [
            { label: "Pending Request", key: "/student-mgmt/teacher-pending" },
            { label: "Teacher", key: '/student-mgmt/teacher' }]
    },
    { label: "Course", icon: <FontAwesomeIcon icon={faFolderTree}/>, key: "/student-mgmt/course/stats"},
    { label: "Student", icon: <FontAwesomeIcon icon={faUserGraduate} />, key: "/student-mgmt/student"},
    { label: "Logout", icon: <LogoutOutlined />, key: "signout" },
]

export default function SideBar() {
    const location = useLocation();
    const navigate = useNavigate();
    const doLogout = () => {
        axios({
            method: "POST",
            url: `${SERVER_URL}/api/v1.0/auth/do-logout`,
    
            headers: {
                'Authorization':'Bearer ' + localStorage.getItem('jwt'),
            }
        }).then(res => {
            localStorage.removeItem("jwt");
            navigate('/login')                                                  
        }).catch(er => {
            // console.log(`Error: ${er}`);
            localStorage.removeItem("jwt");
            navigate('/login')
        })
    }
    const style={
        height: '100vh', position: 'fixed', overflow: 'hidden', marginTop: '4.5em'
    }
    return (
        <Sider theme='light ' className="absolute overflow-hidden mt-16" >
            <div>
            <Menu
                selectedKeys={location.pathname}
                mode="inline"
                style={{ fontSize: 16 }}
                onClick={({ key }) => {
                    if (key === "signout") {
                        doLogout(navigate);
                    } else {
                        navigate(key)
                        console.log(key);
                    }
                }}
                items={items}
            />
            </div>

        </Sider>
    )
}