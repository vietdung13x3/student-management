import { Button, Modal, Typography } from "antd";
import axios from "axios";
import { useState } from "react";
import { SERVER_URL } from "../../Common/Constant";

const {Paragraph } = Typography;
export default function RejectTeacherModal({isOpen, setIsOpen, ids, setModalDone, notification}) {
    const [loading, setLoading] = useState(false);
    async function onApprove(e) {
        e.preventDefault();
        setLoading(true);
        await axios({
            url: `${SERVER_URL}/api/v1.0/teacher/reject-request`,
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                "ids": ids
            }
        }).then(res => {
            if(res.data.code === "00") {
                notification.success({
                    message: res.data.message,
                    description: <>Rejected Ids: {res.data.data.savedIds.length} records found<br/>
                                    Error Ids: {res.data.data.errorIds.length} records found
                                    </>,
                })
                setModalDone(true);
                setIsOpen(false);
            }else{
                notification.error({
                    message: "Error",
                    description: res.data.message,
                })
            }

        }).catch(er => {
            notification.error({
                message: 'Unexpected Error',
                description: <>Internal server error is found</>
            })
        }) 
        setLoading(false)
    }
    return (
        <Modal title="Reject teacher pending request" open={isOpen} footer={[
            <Button key={'cancel'} onClick={()=> setIsOpen(false)}>Cancel</Button>,
            <Button danger type="primary" key={'ok'} onClick={onApprove}>Reject</Button>
        ]}>
            <Paragraph>
                Are you sure to reject <span style={{fontWeight: 'bold'}}>{ids.length}</span> requests ?
            </Paragraph>
        </Modal>
    )
}