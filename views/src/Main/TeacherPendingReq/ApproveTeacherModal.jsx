import { Button, Col, Form, Input, Modal, Row, Select, Typography } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import { SERVER_URL } from "../../Common/Constant";
const { Paragraph } = Typography
const ROLE = {
    ADMIN : 'ADMIN',
    STAFF: 'STAFF',
    MAKER: 'MAKER'
}
const OPTIONS = [
    {
        value: ROLE.ADMIN,
        label: 'Admin'
    },
    {
        value: ROLE.STAFF,
        label: 'Staff'
    },
    {
        value: ROLE.MAKER,
        label: 'Maker'
    }
];

export default function ApproveTeacherModal({isOpen, setIsOpen ,ids, notification, setModalDone}) {
    const [roleCode, setRoleCode] = useState('');
    const [isDisabled, setIsDisabled] = useState(false);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        if(roleCode === '') {
            setIsDisabled(true);
        }else {
            setIsDisabled(false);
        }
    }, [roleCode])
    async function onApprove(role) {
        // setRoleCode(role);
        setLoading(true);
        await axios({
            url: `${SERVER_URL}/api/v1.0/teacher/approve-request`,
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                "ids": ids,
                "roleCode": roleCode
            }
        }).then(res => {
            if(res.data.code ==="00"){
                notification.success({
                    message: res.data.message,
                    description: <>Saved Ids: {res.data.data.savedIds.length} records found<br/>
                                    Error Ids: {res.data.data.errorIds.length} records found
                                    </>,
                })
                setModalDone(true);
                setIsOpen(false);
            }
            notification.error({
                message: "Error",
                description: res.data.message
            })  
        }).catch(er => {
            notification.error({
                message: "Error",
                description: er.message
            })
        })
        setLoading(false);
    }
    return (
        <Modal onCancel={()=> setIsOpen(false)} okText={'Approve'}  centered={true} title = {'Please select approval role!'} open={isOpen} 
            footer={[
                <Button key={'cancel'}  onClick={()=> setIsOpen(false)}>Cancel</Button>,
                <Button type="primary" disabled={isDisabled} onClick={onApprove} key={'ok'}>Approve</Button>,
            ]}>
            <br />
            <Paragraph>
                You must select a role in order to approve request to save teacher
            </Paragraph>
            <Row>
                <Col span={24}>
                    <Select onChange={(role)=> setRoleCode(role)} style={{width: '100%'}} options={OPTIONS}  placeholder="Select teacher's role"/>
                </Col>
            </Row>
        </Modal>
    )
}