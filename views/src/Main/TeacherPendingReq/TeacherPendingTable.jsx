import React from 'react';
import { Button, Space, Table, Tag } from 'antd';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { Link } from 'react-router-dom';
import ApproveTeacherModal from './ApproveTeacherModal';
import useNotification from 'antd/es/notification/useNotification';
import RejectTeacherModal from './RejectTeacherModal';
import { SERVER_URL } from '../../Common/Constant';
/**
 * @Todo Remake the table view
 */
const cols = [
    {
        key: '1',
        title: 'Firstname',
        dataIndex: 'firstname'
    },
    {
        key: '2',
        title: 'Lastname',
        dataIndex: 'lastname'
    },
    {
        key: '3',
        title: 'Identify Code',
        dataIndex: 'identifyCode'
    },
    {
        key: '3',
        title: 'Date Of Birth',
        dataIndex: 'dateOfBirth'
    },
    {
        key: '4',
        title: 'Email',
        dataIndex: 'email'
    },
    {
        key: '5',
        title: 'Requested Date',
        dataIndex: 'requestedDate'
    },
    {
        key: '6',
        title: 'Status',
        dataIndex: 'status',
        render: (_, { status }) => (
            <>
                <Tag color={'geekblue'} key='status'>
                    {status}
                </Tag>
            </>
        )
    }
]

export default function TeacherPendingTable({ isSubmit, setIsSubmit, isUploaded, setIsUploaded, criteria }) {
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const [data, setData] = useState([]);
    const [page, setPage] = useState({ size: 10, currentPage: 0, totalPage: 0 });
    const [loading, setLoading] = useState(false);
    const [isOpenApprove, setIsOpenApprove] = useState(false);
    const [isOpenReject, setIsOpenReject] = useState(false);
    const [isDisabled, setIsDisabled] = useState(true);
    const [notification, contextHolder] = useNotification();
    const [modalDone, setModalDone] = useState(false);
    async function onChangeRowKey(newSelectedRowKey) {
        setSelectedRowKeys(newSelectedRowKey);
    }
    function onChangePage(pageNum, pageSize) {
        setPage({...page, currentPage: pageNum -1});
    }
    useEffect(() => {
        fetchData();
    }, [page])
    useEffect(() => {
        if (isUploaded === true) {
            fetchData();
            setIsUploaded(false);
        }
        if(modalDone === true) {
            fetchData();
            setModalDone(false);
            setSelectedRowKeys([]);
        }
        if (isSubmit === true) {
            setPage({ size: 10, currentPage: 0, totalPage: 0 })
            // fetchData();
            setIsSubmit(false);
        }
        if(selectedRowKeys.length === 0) {
            setIsDisabled(true);
        }else {
            setIsDisabled(false)
        }
    }, [isUploaded, isSubmit, selectedRowKeys, modalDone])
    const rowSelection = {
        selectedRowKeys,
        onChange: onChangeRowKey,
    }
    async function fetchData() {
        setData([]);
        setLoading(true);
        const finalParams = { ...criteria, size: page.size, page: page.currentPage };
        await axios({
            method: 'GET',
            url: `${SERVER_URL}/api/v1.0/request/teacher-pending`,
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`
            },
            params: finalParams
        }).then(res => {
            res.data.data.map((request, idx) => {
                setData(data => [...data, {
                    firstname: request.firstname,
                    lastname: request.lastname,
                    dateOfBirth: request.dateOfBirth,
                    email: request.email,
                    requestedDate: request.requestedDate,
                    status: request.status,
                    key: request.id,
                    identifyCode: request.identifyCode
                }])
            })
        }).catch(er => {
            notification.error({
                message:'Error',
                description: er.message
            })
        })
        setLoading(false);
    }
    return (
        <div>
            {contextHolder}
            <ApproveTeacherModal setModalDone={setModalDone} notification={notification} ids={selectedRowKeys} isOpen={isOpenApprove} setIsOpen={setIsOpenApprove}/>
            <RejectTeacherModal setIsOpen={setIsOpenReject} ids={selectedRowKeys} setModalDone={setModalDone}  isOpen={isOpenReject} notification={notification}/>
            <div style={{ textAlign: 'left', marginBottom: '2em' }}>
                <Space size={'middle'}>
                    <Button disabled={isDisabled} type='primary' onClick={()=> setIsOpenApprove(true)}>
                        Approve
                    </Button>
                    <Button disabled={isDisabled} type='primary' danger onClick={()=> setIsOpenReject(true)}>
                        Reject
                    </Button>
                </Space>
            </div>
            <Table loading={loading} columns={cols} rowSelection={rowSelection}  pagination={{ size: page.size, current: page.currentPage + 1, total: page.size * page.totalPage, onChange: onChangePage }} dataSource={data} />
        </div>

    )
}

