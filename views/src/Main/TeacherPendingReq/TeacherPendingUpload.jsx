import { Button, Col, Divider, Form, Input, Row } from "antd";
import Password from "antd/es/input/Password";
import Modal from "antd/es/modal/Modal";
import useNotification from "antd/es/notification/useNotification";
import axios from "axios";
import moment from "moment";
import { useState } from "react";
import { SERVER_URL } from "../../Common/Constant";


export default function TeacherPendingUpload({ isModalOpen, setIsModalOpen, isUploaded, setIsUploaded }) {
    const [form] = Form.useForm();
    const [api, contextHolder] = useNotification();
    const [loading, setLoading] = useState(false);
    const emailPattern = '^(?=.{1,64}@)[A-Za-z0-9_-]+(\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,})$'
    const handleCancel = () => {
        setIsModalOpen(false);
        form.resetFields();
    }
    async function onSubmit(values) {
        setLoading(true);
        await axios({
            method: "POST",
            url: `${SERVER_URL}/api/v1.0/request/teacher-pending`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`
            },
            data: {
                'firstname': values.firstname,
                'lastname': values.lastname,
                'email': values.email,
                'password': values.password,
                'identifyCode':values.identifyCode,
                'dateOfBirth': moment(values.dateOfBirth).format('DD/MM/YYYY')
            }
        }).then(res => {
            if(res.data.status === "00") {
                api.success({
                    message: res.data.message,
                    description: 'Successfully created request!'
                })
                setIsModalOpen(false);
                setIsUploaded(true);
                form.resetFields();
            }else {
                api.error({
                    message: "Error",
                    description: res.data.message
                })
            }
        }).catch(er=> {
            api.error({
                description: er.message,
                message: "Error"
            })
        })
        setLoading(false);
    }
    const [body, setBody] = useState({ email: '', password: '', firstname: '', lastname: '', dateOfBirth: ''})
    return (
        <Modal footer={null} okButtonProps={{}} width={'70em'} open={isModalOpen} onCancel={handleCancel} okText='Submit' >
            {contextHolder}
            <Divider style={{ marginBottom: 30 }}>Upload Teacher Request</Divider>
            <Form form={form} layout="vertical" onFinish={onSubmit} >
                <Row justify={"center"} gutter={20} >
                    <Col span={10} >

                        <Form.Item
                            label="Email"
                            name={'email'}
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your username!',
                                },
                                {
                                    pattern: emailPattern,
                                    message: 'Please enter valid email format'
                                }
                            ]}>
                            <Input type="text"/>
                        </Form.Item>
                    </Col>
                    <Col span={10}>

                        <Form.Item
                            label="Password"
                            name={'password'}
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}>
                            <Password/>
                        </Form.Item>

                    </Col>
                </Row>
                <Row justify={"center"} gutter={20} >
                    <Col span={10} >

                        <Form.Item
                            label="Firstname"
                            name={'firstname'}
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your firstname!',
                                },
                            ]}>
                            <Input/>
                        </Form.Item>

                    </Col>
                    <Col span={10}>

                        <Form.Item
                            label="Lastname"
                            name={'lastname'}
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your lastname!',
                                },
                            ]}>
                            <Input/>
                        </Form.Item>

                    </Col>
                </Row>
                <Row justify={"center"} gutter={20} >
                <Col span={10} >
                        <Form.Item
                            label="Identify Code"
                            name={'identifyCode'}
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your date of birth!',
                                },
                                {
                                    pattern: '^[0-9]{12}$',
                                    message: 'Identify Code must contains 12 numeric characters!'
                                }
                            ]}>
                            <Input type="text"/>
                        </Form.Item>
                    </Col>
                    <Col span={10} >
                        <Form.Item
                            label="Date Of Birth"
                            name={'dateOfBirth'}
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your date of birth!',
                                },
                            ]}>
                            <Input type="date"/>
                        </Form.Item>
                    </Col>
                </Row>
                <br />
                <Row justify={"center"} gutter={20} >
                    <Col  >
                        <Form.Item>
                            <Button onClick={handleCancel}>Cancel</Button>

                        </Form.Item>
                    </Col>
                    <Col>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">Submit</Button>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Modal>
    )
}