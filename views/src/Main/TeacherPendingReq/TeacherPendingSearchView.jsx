import { Col, Form, Input, Row, Button, Divider, Space, DatePicker } from "antd";
import { SearchOutlined, UploadOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import ThemeContent from "../ThemeContent";
import TeacherPendingTable from "./TeacherPendingTable";
import TeacherPendingUpload from "./TeacherPendingUpload";
import moment from "moment";
import { Outlet } from "react-router";
const { RangePicker } = DatePicker;
const CRITERIA = { firstname: '', lastname: '', email: '', id: '', identifyCode: '', dateOfBirth: '', fromDate: null, toDate: null }
export default function TeacherPendingSearchView() {
    const [criteria, setCriteria] = useState(CRITERIA);
    const [isSubmit, setIsSubmit] = useState(false);
    const [validateStatus, setValidateStatus] = useState("");
    const [isUploadOpen, setIsUploadOpen] = useState(false);
    const [isUploaded, setIsUploaded] = useState(false);
    const onFinish = (body) => {
        setIsSubmit(true);
        setCriteria({
            firstname: body.firstname === undefined ? '' : body.firstname,
            lastname: body.lastname === undefined ? '' : body.lastname,
            email: body.email === undefined ? '' : body.email,
            id: body.id === undefined ? '' : body.id,
            identifyCode: body.identifyCode === undefined ? '' : body.identifyCode,
            dateOfBirth: body.dateOfBirth === undefined ? '' : body.dateOfBirth,
            fromDate: moment(body.requestedDate[0].$d).format('DD/MM/YYYY'),
            toDate: moment(body.requestedDate[1].$d).format('DD/MM/YYYY'),
        });
    }
    return (
        <ThemeContent>

            <Form layout="vertical" onFinish={onFinish}>
                <Row gutter={20} justify='center'>
                    <Col span={8}>
                        <Form.Item label="Firstname"
                            name="firstname">
                            <Input/>
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label="Lastname"
                            name="lastname">
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label="Email"
                            name="email">
                            <Input/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={20}>
                    <Col span={8}>
                        <Form.Item label="ID"
                            name="id">
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                    <Form.Item label="Identify Code"
                            name="identifyCode">
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label="Date Of Birth"
                            name="dateOfBirth">
                            <Input type="date"/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={20}>
                    <Col span={8}>
                        <Form.Item rules={[{
                            required: true,
                            message: "Required requested date",
                        }]}  label="Requested Date" name="requestedDate">
                            <RangePicker className="w-full" format={"DD/MM/YYYY"}/>
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item>
                    <Space>
                        <Button size='middle' type="primary" htmlType="submit" icon={<SearchOutlined />} loading={isSubmit}>
                            Search
                        </Button>
                        <Button size='middle' htmlType="button" onClick={()=> {setIsUploadOpen(true)}} icon={<UploadOutlined />}>
                            Create Pending Request
                        </Button>
                    </Space>
                </Form.Item>
            </Form>
            <Divider />
                <TeacherPendingTable setIsUploaded={setIsUploaded} isUploaded={isUploaded} criteria={criteria} isSubmit={isSubmit} setIsSubmit={setIsSubmit} />
                <TeacherPendingUpload isUploaded={isUploaded} setIsUploaded={setIsUploaded} isModalOpen={isUploadOpen} setIsModalOpen={setIsUploadOpen}></TeacherPendingUpload>
        </ThemeContent> 
    )
}