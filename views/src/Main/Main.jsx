import { Layout } from 'antd';
import React, { useEffect } from 'react';
import { useNavigate, Outlet, useLocation } from 'react-router';
import SideBar from './SideBar';
import HeaderBar from './HeaderBar';
import MainContent from './MainContent';
import jwtDecode from 'jwt-decode';
const { Content } = Layout;
const Main = () => {
    const navigate = useNavigate();
    const location = useLocation();
    useEffect(() => {
        if(location.pathname === "/student-mgmt") {
            navigate('/student-mgmt/teacher-pending')
        }
        if (!localStorage.getItem('jwt')) {
            navigate('/login')
        }else {
            const token = localStorage.getItem('jwt')
            const decodedToken = jwtDecode(token);
            const currentTime = Date.now()/1000;
            if(currentTime > decodedToken.exp) {
                localStorage.removeItem('jwt')
                navigate('/login')
            }
        }
    }, []);
    return (
        <Layout>
            <HeaderBar/>
            <Layout>
                <SideBar/>
                <MainContent/>
            </Layout>
        </Layout>
    );
};
export default Main;