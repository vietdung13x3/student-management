
export default function ThemeContent({children}) {
    // const style = {
    //     padding: 24,
    //     textAlign: 'center',
    //     background: 'white',
    // }
    return (
        <div className="p-10 text-center bg-white">
            {children}
        </div>
    )
}