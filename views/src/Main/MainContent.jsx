import React from "react"
import { Content } from "antd/es/layout/layout"
import ThemeContent from "./ThemeContent"
import { Outlet } from "react-router";
export default function MainContent(){
    // useEffect(()
    return (
        // <h1>Hello</h1>
        <Content id="mainContent" className="mt-28 ml-20 mr-12 mb-12" style={{minHeight: '100vh'}}>

                <Outlet/>

        </Content>
    )
}
