import { Header } from "antd/es/layout/layout";
import Typography from "antd/es/typography/Typography";

const {Title} = Typography;
export default function HeaderBar() {
    return (
        <Header className="fixed top-0 left-0 z-10 w-full overflow-hidden flex justify-center items-center h-96" >
            <Title level={1} style={{color:'white'}}>Student Management</Title>
        </Header>
    )
}