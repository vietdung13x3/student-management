import './App.css';
import {  useLocation, useNavigate, useRoutes } from 'react-router-dom';
import Login from './Auth/Login';
import NotFound from './Common/NotFound';
import Main from './Main/Main';
import useNotification from 'antd/es/notification/useNotification';
import TeacherPendingSearchView from './Main/TeacherPendingReq/TeacherPendingSearchView';
import { useEffect } from 'react';
import TeacherSearchView from './Main/Teacher/TeacherSearchView';
import CourseView from './Main/Course/CourseView';
import CourseDetailView from './Main/Course/CourseDetailView';
import CourseStats from './Main/Course/CourseStats';
import UpdateStudentModal from './Main/Student/UpdateStudentModal';
import StudentView from './Main/Student/StudentView'

function App() {
  const [notification, contextHolder]= useNotification();
  const {pathname} = useLocation()
  const navigate = useNavigate();
  useEffect(()=> {
    if(pathname == "/") {
      navigate('/student-mgmt')
    }
  }, [])
  const element = useRoutes([
    {
      path:"*",
      element: <NotFound/>
    },
    {
      path:'/student-mgmt',
      element: <Main notification={notification}/>,
      children: [
        {
          path:'teacher-pending',
          element: <TeacherPendingSearchView/>,
        },
        {
          path: 'teacher',
          element: <TeacherSearchView/>
        },
        {
          path: 'course',
          element: <CourseView/>,
          children: [{
            path: ':code',
            element: <CourseDetailView/>
          },
          {
            path: 'stats',
            element: <CourseStats/>
          }
        ]
        },
        {
          path: 'student',
          element: <StudentView/>,
          children: [
            {
              path: ':id',
              element: <UpdateStudentModal/>
            }
          ]
        }
      ]
    },
    {
      path: "/login",
      element: <Login notification={notification}/>,
    }
  ])
  return (
    <div>
      {contextHolder}
      {element}
    </div>

  );
}



export default App;
