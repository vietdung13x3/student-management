import { Row, Col, Typography } from "antd"
import { QuestionOutlined, QuestionCircleFilled } from "@ant-design/icons"
const { Title } = Typography
export default function NotFound() {
    return (
        <div>
            <Row >
                <Col span={12} offset={6}>
                    <Title level={1} style={{ color: 'white', textAlign: "center" }}><QuestionCircleFilled style={{fontSize: '2em', color: 'white'}}/></Title>
                </Col>
            </Row>
            <Row>
                <Col span={12} offset={6}>
                    <Title level={1} style={{ color: 'white', textAlign: "center" }}>404 - Not Found</Title>
                </Col>
            </Row>
        </div>
    )
}
