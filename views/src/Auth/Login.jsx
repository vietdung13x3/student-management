import { Button, Checkbox, Form, Input, Typography, Divider, message } from 'antd';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router';
import useNotification from 'antd/es/notification/useNotification';
const { Title } = Typography;
const SERVER_HOST = process.env.REACT_APP_SERVER_URL;
export default function Login({notification}) {
    const navigate = useNavigate();

    useEffect(() => {
        if (localStorage.getItem('jwt') != null) {
            navigate('/student-mgmt')
        }
    })
    const [userDetails, setUserDetails] = useState({ email: '', password: '' });
    const [loading, setLoading] = useState(false);
    async function onSubmit(e) {
        e.preventDefault();
        setLoading(true);
        await axios({
            method: "POST",
            url: `${SERVER_HOST}/api/v1.0/auth/do-login`,
            data: {
                "email": userDetails.email,
                "password": userDetails.password
            }
        }).then(res => {
            if (res.data.status === "00") {
                localStorage.setItem("jwt", res.data.data.token)
                localStorage.setItem("role", res.data.role);
                notification.success({
                    message: res.data.message,
                    description: 'Successfully Login'
                })
                navigate('/student-mgmt')
            } else {
                notification.error({
                    message: "Error",
                    description: res.data.message
                })
                navigate('/login')
            }
        }).catch(er => {
            console.log(er)
            notification.error({
                message: "Error",
                description: er.message
            })
        })
        setLoading(false);
    }
    return (
        <div>
            <fieldset className='justify-center w-1/3 m-auto mt-32 bg-white p-16 rounded-lg'>
                <Title level={1} style={{
                    textAlign: 'center',
                }}>Student Management Tools</Title>
                <Divider>Sign In</Divider>
                <Form
                    name="login"
                    className='w-full'
                    
                    initialValues={{
                        remember: true,
                    }}
                    layout='vertical'
                    autoComplete="off"
                >
                    <Form.Item
                        label="Email"
                        name="email"
                        className='w-full'
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input onChange={e => setUserDetails({ ...userDetails, email: e.target.value })} />
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password onChange={e => setUserDetails({ ...userDetails, password: e.target.value })} />
                    </Form.Item>
                    <Form.Item
                        name="remember"
                        valuePropName="checked"
                    >
                        <Checkbox>Remember me!</Checkbox>
                    </Form.Item>

                    <Form.Item>
                        <Button size='middle' type="primary" htmlType="submit" onClick={onSubmit} loading={loading}>
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </fieldset>
        </div>
    )
}